/**
 * @file wifi_event.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-06-29
 *
 * @copyright Copyright (c) 2023
 *
*/
#ifndef WIFI_EVENT_H
#define WIFI_EVENT_H
#include "chip_include.h"

typedef void (*wifi_callback_t)(void);

void wifi_callback_register(wifi_callback_t wifi_callback);
int my_wifi_start_firmware_task(void);
void wifi_event_handler(uint32_t code);
uint8_t my_wifi_connect(char* ssid, char* passwd);
#endif