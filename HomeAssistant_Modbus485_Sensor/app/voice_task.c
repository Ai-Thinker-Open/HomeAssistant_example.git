#define __VOICE_TASK_C_
#include "voice_task.h"
#include "qyq_frame_modbus_master.h"
#include "qyq_frame_modbus_slave.h"

xQueueHandle button_queue = NULL;
xQueueHandle mqtt_queue = NULL;
TaskHandle_t voice_task_xhandle;
TaskHandle_t modbus_tick_task_xhandle;
TaskHandle_t modbus_recv_task_xhandle;
volatile struct bflb_device_s *uartx;
volatile struct bflb_device_s *gpio;

volatile static qyq_frame_modbus_master_type_t qyq_frame_modbus_master;
volatile static qyq_frame_modbus_master_config_t qyq_frame_modbus_master_config;
uint8_t qyq_frame_modbus_master_writebuf[100];        // xmodem 写buf
uint32_t qyq_frame_modbus_master_writebuf_size = 100; // xmodem 写buf大小
uint8_t qyq_frame_modbus_master_readbuf[100];         // xmodem 读buf
uint32_t qyq_frame_modbus_master_readbuf_size = 100;  // xmodem 读buf大小

typedef struct
{
    uint32_t illu_value;    // 光照值    设备地址1
    uint16_t smoggy_status; //烟雾状态   设备地址2
    uint16_t pm1_0_value;   // 设备地址
    uint16_t pm2_5_value;
    uint16_t pm10_value;
    uint16_t temp_value;     //温度
    uint16_t humidity_value; //湿度
    uint16_t co2_value;      //CO2值
} disp_data_t;

uint8_t qyq_frame_modbus_master_config_write(uint8_t *buf, uint16_t length)
{
    uint16_t i = 0;
    bflb_gpio_set(gpio, GPIO_PIN_16);
    for (i = 0; i < length; i++) {
        bflb_uart_putchar(uartx, buf[i]);
    }
    vTaskDelay(pdMS_TO_TICKS(10));
    bflb_gpio_reset(gpio, GPIO_PIN_16);
    return 0;
}

void uart1_send(uint8_t *buf, uint16_t length)
{
    uint16_t i = 0;
    for (i = 0; i < length; i++) {
        bflb_uart_putchar(uartx, buf[i]);
    }
}

void modbus_tick_task(void *msg)
{
    while (1) {
        qyq_frame_modbus_master.qyq_frame_modbus_master_tick(&qyq_frame_modbus_master);
        vTaskDelay(pdMS_TO_TICKS(1));
    }
}

void modbus_recv_task(void *msg)
{
    int ch;
    gpio = bflb_device_get_by_name("gpio");

    bflb_gpio_init(gpio, GPIO_PIN_16, GPIO_OUTPUT | GPIO_PULLUP | GPIO_SMT_EN | GPIO_DRV_0);
    bflb_gpio_uart_init(gpio, GPIO_PIN_1, GPIO_UART_FUNC_UART1_TX);
    bflb_gpio_uart_init(gpio, GPIO_PIN_0, GPIO_UART_FUNC_UART1_RX);

    uartx = bflb_device_get_by_name("uart1");

    struct bflb_uart_config_s cfg;

    cfg.baudrate = 9600;
    cfg.data_bits = UART_DATA_BITS_8;
    cfg.stop_bits = UART_STOP_BITS_1;
    cfg.parity = UART_PARITY_NONE;
    cfg.flow_ctrl = 0;
    cfg.tx_fifo_threshold = 7;
    cfg.rx_fifo_threshold = 7;
    bflb_uart_init(uartx, &cfg);

    while (1) {
        ch = bflb_uart_getchar(uartx);
        if (ch != -1) {
            // printf("0x%02x ", ch);
            qyq_frame_modbus_master.qyq_frame_modbus_master_recv(&qyq_frame_modbus_master, ch);
        }
        vTaskDelay(pdMS_TO_TICKS(1));
    }
}

void voice_task_process(void *msg)
{
    int8_t ret = 0;
    uint8_t read_buf[10];
    uint32_t length;
    uint32_t value = 0;
    uint32_t send_value = 0;
    disp_data_t disp_data;

    qyq_frame_modbus_master_config.qyq_frame_modbus_master_writebuf = qyq_frame_modbus_master_writebuf;
    qyq_frame_modbus_master_config.qyq_frame_modbus_master_readbuf = qyq_frame_modbus_master_readbuf;
    qyq_frame_modbus_master_config.qyq_frame_modbus_master_writebuf_size = qyq_frame_modbus_master_writebuf_size;
    qyq_frame_modbus_master_config.qyq_frame_modbus_master_readbuf_size = qyq_frame_modbus_master_readbuf_size;
    qyq_frame_modbus_master_config.qyq_frame_modbus_master_config_write = qyq_frame_modbus_master_config_write;

    if (qyq_frame_modbus_master_create(&qyq_frame_modbus_master, &qyq_frame_modbus_master_config) != 0) {
        printf("qyq_frame_modbus_master_create fail!\r\n");
    }

    xTaskCreate(modbus_tick_task, (char *)"modbus_tick_task", 1024, NULL, 26, &modbus_tick_task_xhandle);
    xTaskCreate(modbus_recv_task, (char *)"modbus_recv_task", 1024, NULL, 25, &modbus_recv_task_xhandle);

    disp_data.illu_value = 100;
    disp_data.smoggy_status = 1;
    disp_data.pm1_0_value = 100;
    disp_data.pm2_5_value = 100;
    disp_data.pm10_value = 100;
    disp_data.temp_value = 100;
    disp_data.humidity_value = 100;
    disp_data.co2_value = 100;

    while (1) {
        // 设备读取1
        ret = qyq_frame_modbus_master.qyq_frame_modbus_master_read_holding_registers(&qyq_frame_modbus_master, 0x01, 0x0002, 2, read_buf, &length);
        printf("device addr:0x01 ret:%d length:%d\r\n", ret, length);
        if (ret == 0) {
            value = 0;
            value = read_buf[0];
            value <<= 8;
            value |= read_buf[1];
            value <<= 8;
            value |= read_buf[2];
            value <<= 8;
            value |= read_buf[3];

            disp_data.illu_value = value / 1000;
        }
        // 设备读取2
        ret = qyq_frame_modbus_master.qyq_frame_modbus_master_read_holding_registers(&qyq_frame_modbus_master, 0x02, 0x0001, 1, read_buf, &length);
        printf("device addr:0x02 ret:%d length:%d\r\n", ret, length);
        if (ret == 0) {
            value = 0;
            value = read_buf[0];
            value <<= 8;
            value |= read_buf[1];

            disp_data.smoggy_status = value;
        }

        // 设备读取3
        ret = qyq_frame_modbus_master.qyq_frame_modbus_master_read_holding_registers(&qyq_frame_modbus_master, 0x03, 0x0007, 3, read_buf, &length);
        printf("device addr:0x03 ret:%d length:%d\r\n", ret, length);
        if (ret == 0) {
            value = 0;
            value = read_buf[0];
            value <<= 8;
            value |= read_buf[1];
            disp_data.pm1_0_value = value;
            value = 0;
            value = read_buf[2];
            value <<= 8;
            value |= read_buf[3];
            disp_data.pm2_5_value = value;
            value = 0;
            value = read_buf[4];
            value <<= 8;
            value |= read_buf[5];
            disp_data.pm10_value = value;
        }
        
        // // 设备读取4
        // ret = qyq_frame_modbus_master.qyq_frame_modbus_master_read_holding_registers(&qyq_frame_modbus_master, 0x04, 0x0000, 3, read_buf, &length);
        // printf("device addr:0x04 ret:%d length:%d\r\n", ret, length);
        // if (ret == 0) {
        //     value = 0;
        //     value = read_buf[0];
        //     value <<= 8;
        //     value |= read_buf[1];
        //     disp_data.humidity_value = value;
        //     value = 0;
        //     value = read_buf[2];
        //     value <<= 8;
        //     value |= read_buf[3];
        //     disp_data.temp_value = value;
        //     value = 0;
        //     value = read_buf[4];
        //     value <<= 8;
        //     value |= read_buf[5];
        //     disp_data.co2_value = value;
        // }

        // 设备读取4
        ret = qyq_frame_modbus_master.qyq_frame_modbus_master_read_holding_registers(&qyq_frame_modbus_master, 0x04, 0x0002, 1, read_buf, &length);
        printf("device addr:0x04 ret:%d length:%d\r\n", ret, length);
        if (ret == 0) {
            value = 0;
            value = read_buf[0];
            value <<= 8;
            value |= read_buf[1];
            disp_data.co2_value = value;
        }
        xQueueSend(button_queue, &disp_data, 0);
        xQueueSend(mqtt_queue, &disp_data, 0);
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}
