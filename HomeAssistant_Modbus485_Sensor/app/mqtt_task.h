#ifndef __MQTT_TASK_H_
#define __MQTT_TASK_H_
#include "chip_include.h"
#ifdef __MQTT_TASK_C_
#define MQTT_TASK_EXT
#else
#define MQTT_TASK_EXT   extern
#endif

MQTT_TASK_EXT TaskHandle_t mqtt_task_xhandle;

MQTT_TASK_EXT void mqtt_task_process(void *msg);

#endif
