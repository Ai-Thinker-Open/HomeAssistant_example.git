#ifndef __CHIP_INCLUDE_H_
#define __CHIP_INCLUDE_H_ 
#include <stdio.h>
#include <string.h>
#include <FreeRTOS.h>
#include <task.h>
#include "queue.h"
#include <stdio.h>

#include "bl616_glb.h"

#include "bflb_adc.h"
#include "bflb_uart.h"
#include "bflb_mtimer.h"

#include "wifi_event.h"



#ifndef null
#define null (void *)0
#endif

#ifndef false
#define false (1 == 0)
#endif

#ifndef true
#define true (1 == 1)
#endif

#endif
