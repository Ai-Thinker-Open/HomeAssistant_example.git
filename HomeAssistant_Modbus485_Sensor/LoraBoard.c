#include "bflb_mtimer.h"
#include "bflb_i2c.h"
#include "bflb_cam.h"
#include "bl616_glb.h"
#include "bflb_l1c.h"
#include "bflb_dbi.h"
#include "bflb_gpio.h"
#include "bflb_uart.h"

#include <FreeRTOS.h>
#include <task.h>
#include <event_groups.h>
#include <queue.h>

#include "board.h"
#include "image_sensor.h"
#include "lcd.h"
#include "ring_buffer.h"

typedef struct
{
    uint32_t illu_value;    // 光照值    设备地址1
    uint16_t smoggy_status; //烟雾状态   设备地址2
    uint16_t pm1_0_value;   // 设备地址3
    uint16_t pm2_5_value;
    uint16_t pm10_value;
    uint16_t temp_value;    //温度
    uint16_t humidity_value;    //温度
    uint16_t co2_value;    //温度
} disp_data_t;

#if (1)
#define LORA_BOARD_DBG(a, ...) printf("[LORA_BOARD dbg]:" a, ##__VA_ARGS__)
#else
#define LORA_BOARD_DBG(a, ...)
#endif

#if (1)
#define LORA_BOARD_INFO(a, ...) printf("[LORA_BOARD info]:" a, ##__VA_ARGS__)
#else
#define LORA_BOARD_INFO(a, ...)
#endif

#define LORA_BOARD_ERR(a, ...) printf("[LORA_BOARD err]:" a, ##__VA_ARGS__)

#define UART_ISR_NOTIFY_INDEX  0

static Ring_Buffer_Type uart_rx_rb;
static uint8_t uart_rx_buffer[256];

static struct bflb_device_s *uart1;

static TaskHandle_t LoraBoard_task_hd;

void intToString(int num, char *str, int size)
{
    // 使用 sprintf 函数进行转换
    snprintf(str, size, "%d", num);
}

extern xQueueHandle button_queue;
static void LoraBoard_task(void *pvParameters)
{
    static uint16_t cnt = 0;
    disp_data_t button_value;
    char str1[100] = "illumination intensity:";
    char str[10];
    char str2[10] = " Lux";
    char ppm_str[] = " ppm";
    char pm_str[] = " ug/m3";

    lcd_draw_str_ascii16(10, 50, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), "Please wait for the data to read...", 100);

    while (1) {
        if (xQueueReceive(button_queue, &button_value, portMAX_DELAY)) {
            memset(str1, 32, sizeof(str1));
            lcd_draw_str_ascii16(10, 50, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);
            strcpy(str1, "illumination intensity:");
            intToString(button_value.illu_value, str, sizeof(str));
            strcat(str1, str);
            strcat(str1, str2);
            // lcd_clear(LCD_COLOR_RGB(0, 0, 0));
            lcd_draw_str_ascii16(10, 50, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);

            memset(str1, 32, sizeof(str1));
            lcd_draw_str_ascii16(10, 70, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);
            strcpy(str1, "smoggy_status:");
            if(button_value.smoggy_status == 0)
            {
                strcpy(str, "on");
            }
            else
            {
                strcpy(str, "off");
            }
            strcat(str1, str);
            lcd_draw_str_ascii16(10, 70, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);

            memset(str1, 32, sizeof(str1));
            lcd_draw_str_ascii16(10, 90, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);
            strcpy(str1, "pm1_0_value:");
            intToString(button_value.pm1_0_value, str, sizeof(str));
            strcat(str1, str);
            strcat(str1, pm_str);
            lcd_draw_str_ascii16(10, 90, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);

            memset(str1, 32, sizeof(str1));
            lcd_draw_str_ascii16(10, 110, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);
            strcpy(str1, "pm2_5_value:");
            intToString(button_value.pm2_5_value, str, sizeof(str));
            strcat(str1, str);
            strcat(str1, pm_str);
            lcd_draw_str_ascii16(10, 110, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);

            memset(str1, 32, sizeof(str1));
            lcd_draw_str_ascii16(10, 130, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);
            strcpy(str1, "pm10_value:");
            intToString(button_value.pm10_value, str, sizeof(str));
            strcat(str1, str);
            strcat(str1, pm_str);
            lcd_draw_str_ascii16(10, 130, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);

            // memset(str1, 32, sizeof(str1));
            // lcd_draw_str_ascii16(10, 150, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);
            // strcpy(str1, "temp_value:");
            // intToString(button_value.temp_value, str, sizeof(str));
            // strcat(str1, str);
            // lcd_draw_str_ascii16(10, 150, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);

            // memset(str1, 32, sizeof(str1));
            // lcd_draw_str_ascii16(10, 170, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);
            // strcpy(str1, "humidity_value:");
            // intToString(button_value.humidity_value, str, sizeof(str));
            // strcat(str1, str);
            // lcd_draw_str_ascii16(10, 170, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);

            // memset(str1, 32, sizeof(str1));
            // lcd_draw_str_ascii16(10, 190, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);
            // strcpy(str1, "co2_value:");
            // intToString(button_value.co2_value, str, sizeof(str));
            // strcat(str1, str);
            // lcd_draw_str_ascii16(10, 190, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);


            memset(str1, 32, sizeof(str1));
            lcd_draw_str_ascii16(10, 150, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);
            strcpy(str1, "co2_value:");
            intToString(button_value.co2_value, str, sizeof(str));
            strcat(str1, str);
            strcat(str1, ppm_str);
            lcd_draw_str_ascii16(10, 150, LCD_COLOR_RGB(0x80, 0x80, 0xff), LCD_COLOR_RGB(0, 0, 0), str1, 100);
        }
    }
}

int LoraBoard_task_init(void)
{
    Ring_Buffer_Init(&uart_rx_rb, uart_rx_buffer, sizeof(uart_rx_buffer), NULL, NULL);

    /* lcd init */
    lcd_init();
    lcd_set_dir(1, 0);
    lcd_clear(LCD_COLOR_RGB(0, 0, 0));
    printf("lcd init\r\n");

    xTaskCreate(LoraBoard_task, (char *)"LoraBoard_task", 1024 * 3, NULL, 20, &LoraBoard_task_hd);

    return 0;
}
