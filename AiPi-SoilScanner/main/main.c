/**
 * @file main.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-08-22
 *
 * @copyright Copyright (c) 2023
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <FreeRTOS.h>
#include <task.h>
#include "board.h"
#include "log.h"
#include "timers.h"
#include "lv_user_config.h"
#include "gui_guider.h"
#include "custom.h"
#include "events_init.h"
#include "lv_port_fs.h"
#include "soil_adc.h"
//gpio
#include "bflb_gpio.h"
#define DBG_TAG "MAIN"

lv_ui guider_ui;


static TimerHandle_t modbus_time;
struct bflb_device_s* uartx;
struct bflb_device_s* gpio;

char** taxs_lable[] = { "土壤宝宝喊渴啦！赶紧给它来个‘水分SPA’，让它恢复活力满满！",
                        "土壤湿度刚刚好，植物宝宝们正享受着完美的‘水床’呢!",
                        "土壤湿度过高啦，植物们可能正在‘游泳’呢。快来帮它们把水排一排，保持舒适环境！",
};

void uart1_send(uint8_t* buf, uint16_t length)
{
    uint16_t i = 0;
    for (i = 0; i < length; i++) {
        bflb_uart_putchar(uartx, buf[i]);
    }
}

void wifi_event_handler()
{

}

void voice_task_process(void* msg);

static void soil_adcc_callback(uint32_t soil_adc)
{
    uint8_t soil_canner = 0;

    soil_canner = -0.0501*soil_adc+124.75;
    LOG_I("soil_adc =%d mv soil_canner=%d", soil_adc, soil_canner);
    if (soil_canner>=0&& soil_canner<=100)
        lv_label_set_text_fmt(guider_ui.screen_label_2, "%d", soil_canner);
    /* 湿度过低时提示*/
    if (soil_canner<30)
        lv_label_set_text(guider_ui.screen_label_4, taxs_lable[0]);
    /* 湿度适当时提示*/
    if (soil_canner>30&&soil_canner<70)
        lv_label_set_text(guider_ui.screen_label_4, taxs_lable[1]);
    /* 湿度过高时提示*/
    if (soil_canner>70)
        lv_label_set_text(guider_ui.screen_label_4, taxs_lable[2]);
}

int main(void)
{
    board_init();
    // if (0 != rfparam_init(0, NULL, 0)) {
    //     LOG_E("PHY RF init failed!\r\n");
    //     return 0;
    // }
    struct bflb_device_s* gpio;
    gpio = bflb_device_get_by_name("gpio");
    bflb_gpio_init(gpio, GPIO_PIN_19, GPIO_OUTPUT | GPIO_PULLUP | GPIO_SMT_EN | GPIO_DRV_0);
    bflb_mtd_init();
    lv_init();
    lv_port_disp_init();
    lv_port_indev_init();
    setup_ui(&guider_ui);
    bflb_gpio_set(gpio, GPIO_PIN_19);
    xTaskCreate(lvgl_tick_task, (char*)"lvgl", 1024, NULL, 2, NULL);
    soil_adc_init(&soil_adcc_callback);
    vTaskStartScheduler();
}

