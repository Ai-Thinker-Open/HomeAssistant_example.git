/**
 * @file soil_adc.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-10-17
 *
 * @copyright Copyright (c) 2024
 *
*/
#ifndef SOIL_ADC_H
#define SOIL_ADC_H

typedef void(*soil_adc_callback_t)(uint32_t);

void soil_adc_init(soil_adc_callback_t soil_adc_callback);
#endif