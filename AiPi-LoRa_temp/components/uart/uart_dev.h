/**
 * @file uart_dev.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-09-10
 *
 * @copyright Copyright (c) 2024
 *
*/
#ifndef UART_DEV_H
#define UART_DEV_H

typedef struct ra08_data
{
    char dev_uid[16];
    double temperature;
    float temperature_F;
    int humidity;
}ra08_data_t;
extern ra08_data_t __ra08_data;
void uart1_init();
#endif