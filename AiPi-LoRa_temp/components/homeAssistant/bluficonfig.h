/**
 * @file bluficonfig.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-01-23
 *
 * @copyright Copyright (c) 2024
 *
*/
#ifndef BLUFICONFIG_H
#define BLUFICONFIG_H
#include "blufi.h"
#define BLUFI_NAME "AiPi-R286"
void homeassistant_blufi_init(char* blufi_name);
void homeassistant_blufi_start(void);
void update_homeassistant_blufi_event(int blufi_event);
char* homeassistant_get_blufi_name(void);
int homeassistant_get_blufi_state(void);
#endif