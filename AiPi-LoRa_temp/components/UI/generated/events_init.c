/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/

#include "events_init.h"
#include <stdio.h>
#include "lvgl.h"
#include "device_ctrl.h"
#if LV_USE_GUIDER_SIMULATOR && LV_USE_FREEMASTER
#include "freemaster_client.h"
#endif

extern bool config_scr_del;
extern bool main_scr_del;
static void main_event_handler(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    switch (code) {
        case LV_EVENT_GESTURE:
        {
            lv_dir_t dir = lv_indev_get_gesture_dir(lv_indev_get_act());
            switch (dir) {
                case LV_DIR_RIGHT:
                {
                    main_scr_del = true;
                    config_scr_del = false;
                    lv_indev_wait_release(lv_indev_get_act());
                    ui_load_scr_animation(&guider_ui, &guider_ui.config, guider_ui.config_del, &guider_ui.main_del, setup_scr_config, LV_SCR_LOAD_ANIM_OVER_RIGHT, 150, 150, false, true);
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

void events_init_main(lv_ui* ui)
{
    lv_obj_add_event_cb(ui->main, main_event_handler, LV_EVENT_ALL, ui);
}

static void config_event_handler(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    switch (code) {
        case LV_EVENT_GESTURE:
        {
            lv_dir_t dir = lv_indev_get_gesture_dir(lv_indev_get_act());
            switch (dir) {
                case LV_DIR_LEFT:
                {
                    lv_indev_wait_release(lv_indev_get_act());
                    ui_load_scr_animation(&guider_ui, &guider_ui.main, guider_ui.main_del, &guider_ui.config_del, setup_scr_main, LV_SCR_LOAD_ANIM_OVER_LEFT, 150, 150, false, true);
                    main_scr_del = false;
                    config_scr_del = true;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}
static void config_btn_1_event(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_ui* ui = lv_event_get_user_data(e);
    switch (code) {
        case LV_EVENT_CLICKED:
        {
            device_send_state_notify(DEVICE_STATE_BLUFI_CONFIG_START, 0);
            lv_label_set_text(ui->config_btn_1_label, "WiFi Config ...");
        }
        default:
            break;
    }
}
void events_init_config(lv_ui* ui)
{
    lv_obj_add_event_cb(ui->config, config_event_handler, LV_EVENT_ALL, ui);
    lv_obj_add_event_cb(ui->config_btn_1, config_btn_1_event, LV_EVENT_CLICKED, ui);
}


void events_init(lv_ui* ui)
{

}
