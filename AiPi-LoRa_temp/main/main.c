/**
 * @file main.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-08-22
 *
 * @copyright Copyright (c) 2023
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <FreeRTOS.h>
#include <task.h>
#include "board.h"
#include "log.h"
#include "timers.h"
#include "lv_user_config.h"
#include "gui_guider.h"
#include "custom.h"
#include "events_init.h"
#include "lv_port_fs.h"
#include "wifi_event.h"
#include "homeAssistantMQTT.h"
#include "uart_dev.h"
#include "device_ctrl.h"

#define DBG_TAG "MAIN"

lv_ui guider_ui;
static homeAssisatnt_device_t ha_device;

static TimerHandle_t modbus_time;

struct bflb_device_s* uartx;
struct bflb_device_s* gpio;


void uart1_send(uint8_t* buf, uint16_t length)
{
    uint16_t i = 0;
    for (i = 0; i < length; i++) {
        bflb_uart_putchar(uartx, buf[i]);
    }
}

static void ha_event_cb(ha_event_t event, homeAssisatnt_device_t* ha_dev)
{
    switch (event)
    {
        case HA_EVENT_MQTT_CONNECED:
            LOG_I("<<<<<<<<<<  HA_EVENT_MQTT_CONNECED");
            static ha_sensor_entity_t _temp = {
                .name = "温度",
                .device_class = Class_temperature,
                .unique_id = "temp_lora",
                .unit_of_measurement = "℃"
            };
            static ha_sensor_entity_t _humi = {
                .name = "湿度",
                .device_class = Class_humidity,
                .unique_id = "humi_lora",
                .unit_of_measurement = "%",

            };
            homeAssistant_device_add_entity(CONFIG_HA_ENTITY_SENSOR, &_temp);
            homeAssistant_device_add_entity(CONFIG_HA_ENTITY_SENSOR, &_humi);
            homeAssistant_device_send_status(HOMEASSISTANT_STATUS_ONLINE);

            break;
        case HA_EVENT_MQTT_DISCONNECT:
            LOG_I("<<<<<<<<<<  HA_EVENT_MQTT_DISCONNECT");
            device_send_state_notify(DEVICE_STATE_WIFI_DISCONNECT, false);
            break;

        default:
            break;
    }
}

void voice_task_process(void* msg);

int main(void)
{
    board_init();
    if (0 != rfparam_init(0, NULL, 0)) {
        LOG_E("PHY RF init failed!\r\n");
        return 0;
    }
    esay_flash_init();
    uart1_init();
    staWiFiInit();
    uint8_t STA_MAC[6] = { 0 };
    // homeassistant_blufi_init(NULL)
    homeassistant_blufi_init("AiPi-LoRa");
    aiio_wifi_sta_mac_get(STA_MAC);
    ha_device.mqtt_info.mqtt_username = pvPortMalloc(128);
    memset(ha_device.mqtt_info.mqtt_username, 0, 128);
    sprintf(ha_device.mqtt_info.mqtt_username, "AiPi-UNO-ET485_sensor", STA_MAC[4], STA_MAC[5]);

    ha_device.mqtt_info.mqtt_host = "192.168.32.2";
    ha_device.mqtt_info.port = 1883;
    ha_device.mqtt_info.mqtt_keeplive = 60;
    ha_device.model = "AiPi-UNO-ET485";
    ha_device.mqtt_info.mqtt_clientID = ha_device.model;
    ha_device.name = "LoRa_Borad";
    ha_device.hw_version = "v1.0";

    homeAssistant_device_init(&ha_device, ha_event_cb);
     lv_init();
    lv_port_disp_init();
    lv_port_indev_init();
    setup_ui(&guider_ui);
    xTaskCreate(lvgl_tick_task, (char*)"lvgl", 1024, NULL, 2, NULL);
    xTaskCreate(device_state_task, "deivce", 1024*2, &ha_device, 3, (TaskHandle_t*)&device_task);

   
    vTaskStartScheduler();
}

