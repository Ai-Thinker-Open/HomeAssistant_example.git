/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/

#include "lvgl.h"
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "widgets_init.h"
#include "custom.h"
#include "encoder.h"
void setup_scr_main(lv_ui* ui)
{
	//Write codes main
	ui->main = lv_obj_create(NULL);
	lv_obj_set_size(ui->main, 240, 240);
	lv_obj_set_scrollbar_mode(ui->main, LV_SCROLLBAR_MODE_OFF);

	//Write style for main, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->main, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->main, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_carousel_1
	ui->main_carousel_1 = lv_carousel_create(ui->main);
	lv_carousel_set_element_width(ui->main_carousel_1, 200);
	lv_obj_set_size(ui->main_carousel_1, 240, 240);
	ui->main_carousel_1_element_1 = lv_carousel_add_element(ui->main_carousel_1, 0);
	ui->main_carousel_1_element_2 = lv_carousel_add_element(ui->main_carousel_1, 1);
	ui->main_carousel_1_element_3 = lv_carousel_add_element(ui->main_carousel_1, 2);
	lv_obj_set_element_id(ui->main_carousel_1, element_id_old, LV_ANIM_ON);

	lv_obj_set_pos(ui->main_carousel_1, 0, 0);
	lv_obj_set_size(ui->main_carousel_1, 240, 240);
	lv_obj_set_scrollbar_mode(ui->main_carousel_1, LV_SCROLLBAR_MODE_ON);

	//Write style for main_carousel_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->main_carousel_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_carousel_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_carousel_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style for main_carousel_1, Part: LV_PART_SCROLLBAR, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->main_carousel_1, 0, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_carousel_1, 0, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_DEFAULT for &style_main_carousel_1_extra_element_items_default
	static lv_style_t style_main_carousel_1_extra_element_items_default;
	ui_init_style(&style_main_carousel_1_extra_element_items_default);

	lv_style_set_bg_opa(&style_main_carousel_1_extra_element_items_default, 0);
	lv_style_set_outline_width(&style_main_carousel_1_extra_element_items_default, 0);
	lv_style_set_border_width(&style_main_carousel_1_extra_element_items_default, 0);
	lv_style_set_radius(&style_main_carousel_1_extra_element_items_default, 5);
	lv_style_set_shadow_width(&style_main_carousel_1_extra_element_items_default, 0);

	lv_obj_add_style(ui->main_carousel_1_element_3, &style_main_carousel_1_extra_element_items_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_style(ui->main_carousel_1_element_2, &style_main_carousel_1_extra_element_items_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_style(ui->main_carousel_1_element_1, &style_main_carousel_1_extra_element_items_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_FOCUSED for &style_main_carousel_1_extra_element_items_focused
	static lv_style_t style_main_carousel_1_extra_element_items_focused;
	ui_init_style(&style_main_carousel_1_extra_element_items_focused);

	lv_style_set_bg_opa(&style_main_carousel_1_extra_element_items_focused, 0);
	lv_style_set_outline_width(&style_main_carousel_1_extra_element_items_focused, 0);
	lv_style_set_border_width(&style_main_carousel_1_extra_element_items_focused, 0);
	lv_style_set_radius(&style_main_carousel_1_extra_element_items_focused, 5);
	lv_style_set_shadow_width(&style_main_carousel_1_extra_element_items_focused, 0);

	lv_obj_add_style(ui->main_carousel_1_element_3, &style_main_carousel_1_extra_element_items_focused, LV_PART_MAIN|LV_STATE_FOCUSED);
	lv_obj_add_style(ui->main_carousel_1_element_2, &style_main_carousel_1_extra_element_items_focused, LV_PART_MAIN|LV_STATE_FOCUSED);
	lv_obj_add_style(ui->main_carousel_1_element_1, &style_main_carousel_1_extra_element_items_focused, LV_PART_MAIN|LV_STATE_FOCUSED);




	//Write codes main_cont_1
	ui->main_cont_1 = lv_obj_create(ui->main_carousel_1_element_1);
	lv_obj_set_pos(ui->main_cont_1, 23, 43);
	lv_obj_set_size(ui->main_cont_1, 151, 163);
	lv_obj_set_scrollbar_mode(ui->main_cont_1, LV_SCROLLBAR_MODE_OFF);

	//Write style for main_cont_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_cont_1, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->main_cont_1, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->main_cont_1, lv_color_hex(0x646668), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->main_cont_1, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_cont_1, 50, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_cont_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_cont_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_cont_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_cont_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_cont_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_cont_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_img_1
	ui->main_img_1 = lv_img_create(ui->main_cont_1);
	lv_obj_add_flag(ui->main_img_1, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->main_img_1, &_color_s_alpha_100x100);
	lv_img_set_pivot(ui->main_img_1, 50, 50);
	lv_img_set_angle(ui->main_img_1, 0);
	lv_obj_set_pos(ui->main_img_1, 23, 13);
	lv_obj_set_size(ui->main_img_1, 100, 100);

	//Write style for main_img_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_recolor_opa(ui->main_img_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->main_img_1, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_img_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_clip_corner(ui->main_img_1, true, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_1
	ui->main_label_1 = lv_label_create(ui->main_cont_1);
	lv_label_set_text(ui->main_label_1, "Set all lights colors");
	lv_label_set_long_mode(ui->main_label_1, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_1, 16, 114);
	lv_obj_set_size(ui->main_label_1, 130, 33);

	//Write style for main_label_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_1, lv_color_hex(0xe7e7e7), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_1, &lv_font_arial_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_opa(ui->main_label_1, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_1, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_1, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);



	//Write codes main_cont_2
	ui->main_cont_2 = lv_obj_create(ui->main_carousel_1_element_2);
	lv_obj_set_pos(ui->main_cont_2, 23, 43);
	lv_obj_set_size(ui->main_cont_2, 151, 163);
	lv_obj_set_scrollbar_mode(ui->main_cont_2, LV_SCROLLBAR_MODE_OFF);

	//Write style for main_cont_2, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_cont_2, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->main_cont_2, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->main_cont_2, lv_color_hex(0x646668), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->main_cont_2, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_cont_2, 50, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_cont_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_cont_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_cont_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_cont_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_cont_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_cont_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_img_2
	ui->main_img_2 = lv_img_create(ui->main_cont_2);
	lv_obj_add_flag(ui->main_img_2, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->main_img_2, &_numble_ctrl_alpha_100x100);
	lv_img_set_pivot(ui->main_img_2, 50, 50);
	lv_img_set_angle(ui->main_img_2, 0);
	lv_obj_set_pos(ui->main_img_2, 23, 13);
	lv_obj_set_size(ui->main_img_2, 100, 100);

	//Write style for main_img_2, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_recolor_opa(ui->main_img_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->main_img_2, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_img_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_clip_corner(ui->main_img_2, true, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_2
	ui->main_label_2 = lv_label_create(ui->main_cont_2);
	lv_label_set_text(ui->main_label_2, "Control one by one");
	lv_label_set_long_mode(ui->main_label_2, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_2, 12, 122);
	lv_obj_set_size(ui->main_label_2, 130, 33);

	//Write style for main_label_2, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_2, lv_color_hex(0xe7e7e7), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_2, &lv_font_arial_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_opa(ui->main_label_2, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_2, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_2, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);



	//Write codes main_cont_3
	ui->main_cont_3 = lv_obj_create(ui->main_carousel_1_element_3);
	lv_obj_set_pos(ui->main_cont_3, 22, 43);
	lv_obj_set_size(ui->main_cont_3, 151, 163);
	lv_obj_set_scrollbar_mode(ui->main_cont_3, LV_SCROLLBAR_MODE_OFF);

	//Write style for main_cont_3, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_cont_3, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->main_cont_3, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->main_cont_3, lv_color_hex(0x646668), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->main_cont_3, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_cont_3, 50, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_cont_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_cont_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_cont_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_cont_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_cont_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_cont_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_img_3
	ui->main_img_3 = lv_img_create(ui->main_cont_3);
	lv_obj_add_flag(ui->main_img_3, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->main_img_3, &_mode1_alpha_45x45);
	lv_img_set_pivot(ui->main_img_3, 50, 50);
	lv_img_set_angle(ui->main_img_3, 0);
	lv_obj_set_pos(ui->main_img_3, 51, 10);
	lv_obj_set_size(ui->main_img_3, 45, 45);

	//Write style for main_img_3, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_recolor_opa(ui->main_img_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->main_img_3, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_img_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_clip_corner(ui->main_img_3, true, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_3
	ui->main_label_3 = lv_label_create(ui->main_cont_3);
	lv_label_set_text(ui->main_label_3, "Mode control");
	lv_label_set_long_mode(ui->main_label_3, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_3, 12, 122);
	lv_obj_set_size(ui->main_label_3, 130, 33);

	//Write style for main_label_3, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_3, lv_color_hex(0xe7e7e7), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_3, &lv_font_arial_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_opa(ui->main_label_3, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_3, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_3, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_img_4
	ui->main_img_4 = lv_img_create(ui->main_cont_3);
	lv_obj_add_flag(ui->main_img_4, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->main_img_4, &_mode2_alpha_45x45);
	lv_img_set_pivot(ui->main_img_4, 50, 50);
	lv_img_set_angle(ui->main_img_4, 0);
	lv_obj_set_pos(ui->main_img_4, 17, 65);
	lv_obj_set_size(ui->main_img_4, 45, 45);

	//Write style for main_img_4, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_recolor_opa(ui->main_img_4, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->main_img_4, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_img_4, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_clip_corner(ui->main_img_4, true, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_img_5
	ui->main_img_5 = lv_img_create(ui->main_cont_3);
	lv_obj_add_flag(ui->main_img_5, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->main_img_5, &_mode3_alpha_45x45);
	lv_img_set_pivot(ui->main_img_5, 50, 50);
	lv_img_set_angle(ui->main_img_5, 0);
	lv_obj_set_pos(ui->main_img_5, 88, 65);
	lv_obj_set_size(ui->main_img_5, 45, 45);

	//Write style for main_img_5, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_recolor_opa(ui->main_img_5, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->main_img_5, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_img_5, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_clip_corner(ui->main_img_5, true, LV_PART_MAIN|LV_STATE_DEFAULT);

	//The custom code of main.
	// ui_add_obj_to_encoder_group(ui->main);
	// ui_add_obj_to_encoder_group(ui->main_carousel_1);
	// ui_add_obj_to_encoder_group(ui->main_carousel_1_element_1);
	// ui_add_obj_to_encoder_group(ui->main_carousel_1_element_2);
	// ui_add_obj_to_encoder_group(ui->main_carousel_1_element_3);
	// ui_add_obj_to_encoder_group(ui->main_cont_1);
	// ui_add_obj_to_encoder_group(ui->main_cont_2);
	// ui_add_obj_to_encoder_group(ui->main_cont_3);
	//Update current screen layout.
	lv_obj_update_layout(ui->main);
	events_init_main(ui);
}
