/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/

#include "events_init.h"
#include <stdio.h>
#include "lvgl.h"
#include "gui_guider.h"
#include "qyq_triled_drive.h"
#include "encoder.h"
#include "homeAssistantPort.h"
static void mode_event_handler(lv_event_t* e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code) {
		case LV_EVENT_LONG_PRESSED:
		{
			ui_load_scr_animation(&guider_ui, &guider_ui.main, guider_ui.main_del, &guider_ui.mode_del, setup_scr_main, LV_SCR_LOAD_ANIM_NONE, 200, 200, false, true);
			ha_lh_entity_t* light_rgb = homeAssistant_fine_entity(CONFIG_HA_ENTITY_LIGHT, "triled-all_01");
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, light_rgb, 0);
			guider_ui.screen = 0;
			break;
		}
		default:
			break;
	}
}

static void mode_carousel_1_event_handler(lv_event_t* e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code) {
		case LV_EVENT_LONG_PRESSED:
		{
			ui_load_scr_animation(&guider_ui, &guider_ui.main, guider_ui.main_del, &guider_ui.mode_del, setup_scr_main, LV_SCR_LOAD_ANIM_NONE, 200, 200, false, true);
			guider_ui.screen = 0;
			ha_lh_entity_t* light_rgb = homeAssistant_fine_entity(CONFIG_HA_ENTITY_LIGHT, "triled-all_01");
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, light_rgb, 0);
			break;
		}
		default:
			break;
	}
}

static void mode_carousel_1_mode1_event_handler(lv_event_t* e)
{
	lv_event_code_t code = lv_event_get_code(e);
	ha_sw_entity_t* mode1_sw = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "triled-mode1_01");
	ha_sw_entity_t* mode2_sw = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "triled-mode2_01");
	ha_sw_entity_t* mode3_sw = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "triled-mode3_01");
	ha_lh_entity_t* light_rgb = homeAssistant_fine_entity(CONFIG_HA_ENTITY_LIGHT, "triled-all_01");
	homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, light_rgb, 1);

	switch (code) {
		case LV_EVENT_CLICKED:
		{
			printf("mode_carousel_1_mode1 LV_EVENT_CLICKED\r\n");
			triled_drive_set_mode(0);
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode1_sw, 1);
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode2_sw, 0);
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode3_sw, 0);
			break;
		}
		default:
			break;
	}
}


static void mode_carousel_1_mode2_event_handler(lv_event_t* e)
{
	lv_event_code_t code = lv_event_get_code(e);
	ha_sw_entity_t* mode1_sw = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "triled-mode1_01");
	ha_sw_entity_t* mode2_sw = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "triled-mode2_01");
	ha_sw_entity_t* mode3_sw = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "triled-mode3_01");
	ha_lh_entity_t* light_rgb = homeAssistant_fine_entity(CONFIG_HA_ENTITY_LIGHT, "triled-all_01");
	homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, light_rgb, 1);
	switch (code) {
		case LV_EVENT_CLICKED:
		{
			printf("mode_carousel_1_mode2 LV_EVENT_CLICKED\r\n");
			triled_drive_set_mode(1);
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode1_sw, 0);
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode2_sw, 1);
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode3_sw, 0);
			break;
		}
		default:
			break;
	}
}

static void mode_carousel_1_mode3_event_handler(lv_event_t* e)
{
	lv_event_code_t code = lv_event_get_code(e);
	ha_sw_entity_t* mode1_sw = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "triled-mode1_01");
	ha_sw_entity_t* mode2_sw = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "triled-mode2_01");
	ha_sw_entity_t* mode3_sw = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "triled-mode3_01");
	ha_lh_entity_t* light_rgb = homeAssistant_fine_entity(CONFIG_HA_ENTITY_LIGHT, "triled-all_01");
	switch (code) {
		case LV_EVENT_CLICKED:
		{
			printf("mode_carousel_1_mode3 LV_EVENT_CLICKED\r\n");
			triled_drive_set_mode(2);
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, light_rgb, 1);
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode1_sw, 0);
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode2_sw, 0);
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode3_sw, 1);
			break;
		}
		default:
			break;
	}
}
void events_init_mode(lv_ui* ui)
{
	lv_obj_add_event_cb(ui->mode, mode_event_handler, LV_EVENT_ALL, ui);
	lv_obj_add_event_cb(ui->mode_carousel_1, mode_carousel_1_event_handler, LV_EVENT_ALL, ui);
	lv_obj_add_event_cb(ui->mode_carousel_1_mode1, mode_carousel_1_mode1_event_handler, LV_EVENT_ALL, ui);
	lv_obj_add_event_cb(ui->mode_carousel_1_mode2, mode_carousel_1_mode2_event_handler, LV_EVENT_ALL, ui);
	lv_obj_add_event_cb(ui->mode_carousel_1_mode3, mode_carousel_1_mode3_event_handler, LV_EVENT_ALL, ui);
	Create_triled_drive_set_mode_task();
}

static void main_cont_1_event_handler(lv_event_t* e)
{
	lv_event_code_t code = lv_event_get_code(e);

	switch (code) {
		case LV_EVENT_CLICKED:
		{
			ui_load_scr_animation(&guider_ui, &guider_ui.RGB, guider_ui.RGB_del, &guider_ui.main_del, setup_scr_RGB, LV_SCR_LOAD_ANIM_NONE, 200, 200, false, true);
			guider_ui.screen = 1;
			qyq_triled_drive_turn_onall();
			ha_lh_entity_t* light_rgb = homeAssistant_fine_entity(CONFIG_HA_ENTITY_LIGHT, "triled-all_01");
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, light_rgb, 1);
			break;
		}
		default:
			break;
	}
}

static void main_cont_2_event_handler(lv_event_t* e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code) {
		case LV_EVENT_CLICKED:
		{
			ui_load_scr_animation(&guider_ui, &guider_ui.Quantity, guider_ui.Quantity_del, &guider_ui.main_del, setup_scr_Quantity, LV_SCR_LOAD_ANIM_NONE, 200, 200, false, true);
			guider_ui.screen = 2;
			qyq_triled_drive_turn_onall();
			qyq_triled_drive_set_allcolor(0, 0, 0);
			ha_lh_entity_t* light_rgb = homeAssistant_fine_entity(CONFIG_HA_ENTITY_LIGHT, "triled-all_01");
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, light_rgb, 1);
			break;
		}
		default:
			break;
	}
}

static void main_cont_3_event_handler(lv_event_t* e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code) {
		case LV_EVENT_CLICKED:
		{
			ui_load_scr_animation(&guider_ui, &guider_ui.mode, guider_ui.mode_del, &guider_ui.main_del, setup_scr_mode, LV_SCR_LOAD_ANIM_NONE, 200, 200, false, true);
			guider_ui.screen = 3;
			qyq_triled_drive_turn_onall();
			break;
		}
		default:
			break;
	}
}

void events_init_main(lv_ui* ui)
{
	lv_obj_add_event_cb(ui->main_cont_1, main_cont_1_event_handler, LV_EVENT_ALL, ui);
	lv_obj_add_event_cb(ui->main_cont_2, main_cont_2_event_handler, LV_EVENT_ALL, ui);
	lv_obj_add_event_cb(ui->main_cont_3, main_cont_3_event_handler, LV_EVENT_ALL, ui);
}

static void Quantity_event_handler(lv_event_t* e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code) {
		case LV_EVENT_LONG_PRESSED:
		{
			ui_load_scr_animation(&guider_ui, &guider_ui.main, guider_ui.main_del, &guider_ui.Quantity_del, setup_scr_main, LV_SCR_LOAD_ANIM_NONE, 200, 200, false, true);
			guider_ui.screen = 0;
			ha_lh_entity_t* light_rgb = homeAssistant_fine_entity(CONFIG_HA_ENTITY_LIGHT, "triled-all_01");
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, light_rgb, 0);
			break;
		}
		default:
			break;
	}
}

void events_init_Quantity(lv_ui* ui)
{
	lv_obj_add_event_cb(ui->Quantity, Quantity_event_handler, LV_EVENT_ALL, ui);
}

static void RGB_event_handler(lv_event_t* e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code) {
		case LV_EVENT_LONG_PRESSED:
		{
			ui_load_scr_animation(&guider_ui, &guider_ui.main, guider_ui.main_del, &guider_ui.RGB_del, setup_scr_main, LV_SCR_LOAD_ANIM_NONE, 200, 200, false, true);
			guider_ui.screen = 0;
			ha_lh_entity_t* light_rgb = homeAssistant_fine_entity(CONFIG_HA_ENTITY_LIGHT, "triled-all_01");
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, light_rgb, 0);
			break;
		}
		default:
			break;
	}
}

static void rgb_change_value(lv_event_t* e)
{
	lv_event_code_t code = lv_event_get_code(e);
	lv_ui* ui = lv_event_get_user_data(e);
	switch (code) {
		case LV_EVENT_VALUE_CHANGED:
		{
			printf("Color CHANED\r\n");
			lv_color_hsv_t c_hsv = lv_colorwheel_get_hsv(ui->RGB_cpicker_1);
			lv_color_t c = lv_color_hsv_to_rgb(c_hsv.h, c_hsv.s, c_hsv.v);
			qyq_triled_drive_set_allcolor(LV_COLOR_GET_R(c), LV_COLOR_GET_G(c), LV_COLOR_GET_B(c));
			ha_lh_entity_t* light_rgb = homeAssistant_fine_entity(CONFIG_HA_ENTITY_LIGHT, "triled-all_01");
			light_rgb->rgb.red = LV_COLOR_GET_R(c);
			light_rgb->rgb.green = LV_COLOR_GET_G(c);
			light_rgb->rgb.blue = LV_COLOR_GET_B(c);
			homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, light_rgb, 1);
			// printf("RGB: R=%d G=%d B=%d\r\n", LV_COLOR_GET_R(c), LV_COLOR_GET_G(c), LV_COLOR_GET_B(c));
		}
		break;
		default:
			break;
	}
}

void events_init_RGB(lv_ui* ui)
{
	lv_obj_add_event_cb(ui->RGB, RGB_event_handler, LV_EVENT_ALL, NULL);
	lv_obj_add_event_cb(ui->RGB_cpicker_1, rgb_change_value, LV_EVENT_VALUE_CHANGED, ui);
}

void events_init(lv_ui* ui)
{

}
