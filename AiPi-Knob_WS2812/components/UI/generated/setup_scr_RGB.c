/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/

#include "lvgl.h"
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "widgets_init.h"
#include "custom.h"
#include "encoder.h"

void setup_scr_RGB(lv_ui* ui)
{
	//Write codes RGB
	ui->RGB = lv_obj_create(NULL);
	lv_obj_set_size(ui->RGB, 240, 240);

	//Write style for RGB, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->RGB, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->RGB, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes RGB_cpicker_1
	ui->RGB_cpicker_1 = lv_colorwheel_create(ui->RGB, true);
	lv_obj_set_pos(ui->RGB_cpicker_1, 20, 20);
	lv_obj_set_size(ui->RGB_cpicker_1, 200, 200);
	lv_color_hsv_t c_hsv = lv_color_rgb_to_hsv(red, green, blue);
	c_hsv.v = 30;
	lv_colorwheel_set_hsv(ui->RGB_cpicker_1, c_hsv);
	lv_color_t c = lv_color_hsv_to_rgb(c_hsv.h, c_hsv.s, c_hsv.v);
	qyq_triled_drive_set_allcolor(LV_COLOR_GET_R(c), LV_COLOR_GET_G(c), LV_COLOR_GET_B(c));
	//Write style for RGB_cpicker_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_arc_width(ui->RGB_cpicker_1, 10, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Update current screen layout.
	lv_obj_update_layout(ui->RGB);
	// ui_add_obj_to_encoder_group(ui->RGB);

	//Init events for screen.
	events_init_RGB(ui);
}
