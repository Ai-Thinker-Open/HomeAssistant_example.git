/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/

#include "lvgl.h"
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "widgets_init.h"
#include "custom.h"
#include "encoder.h"

void setup_scr_mode(lv_ui* ui)
{
	//Write codes mode
	ui->mode = lv_obj_create(NULL);
	lv_obj_set_size(ui->mode, 240, 240);
	lv_obj_set_scrollbar_mode(ui->mode, LV_SCROLLBAR_MODE_OFF);

	//Write style for mode, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->mode, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->mode, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->mode, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes mode_carousel_1
	ui->mode_carousel_1 = lv_carousel_create(ui->mode);
	lv_carousel_set_element_width(ui->mode_carousel_1, 200);
	lv_obj_set_size(ui->mode_carousel_1, 240, 240);
	ui->mode_carousel_1_mode1 = lv_carousel_add_element(ui->mode_carousel_1, 0);
	ui->mode_carousel_1_mode2 = lv_carousel_add_element(ui->mode_carousel_1, 1);
	ui->mode_carousel_1_mode3 = lv_carousel_add_element(ui->mode_carousel_1, 2);
	lv_obj_set_element_id(ui->mode_carousel_1, mode_element_id_old, LV_ANIM_ON);

	lv_obj_set_pos(ui->mode_carousel_1, 0, 0);
	lv_obj_set_size(ui->mode_carousel_1, 240, 240);
	lv_obj_set_scrollbar_mode(ui->mode_carousel_1, LV_SCROLLBAR_MODE_ON);

	//Write style for mode_carousel_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->mode_carousel_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->mode_carousel_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->mode_carousel_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style for mode_carousel_1, Part: LV_PART_SCROLLBAR, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->mode_carousel_1, 0, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->mode_carousel_1, 0, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_DEFAULT for &style_mode_carousel_1_extra_element_items_default
	static lv_style_t style_mode_carousel_1_extra_element_items_default;
	ui_init_style(&style_mode_carousel_1_extra_element_items_default);

	lv_style_set_bg_opa(&style_mode_carousel_1_extra_element_items_default, 0);
	lv_style_set_outline_width(&style_mode_carousel_1_extra_element_items_default, 0);
	lv_style_set_border_width(&style_mode_carousel_1_extra_element_items_default, 0);
	lv_style_set_radius(&style_mode_carousel_1_extra_element_items_default, 5);
	lv_style_set_shadow_width(&style_mode_carousel_1_extra_element_items_default, 0);
	lv_obj_add_style(ui->mode_carousel_1_mode3, &style_mode_carousel_1_extra_element_items_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_style(ui->mode_carousel_1_mode2, &style_mode_carousel_1_extra_element_items_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_style(ui->mode_carousel_1_mode1, &style_mode_carousel_1_extra_element_items_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_FOCUSED for &style_mode_carousel_1_extra_element_items_focused
	static lv_style_t style_mode_carousel_1_extra_element_items_focused;
	ui_init_style(&style_mode_carousel_1_extra_element_items_focused);

	lv_style_set_bg_opa(&style_mode_carousel_1_extra_element_items_focused, 0);
	lv_style_set_outline_width(&style_mode_carousel_1_extra_element_items_focused, 0);
	lv_style_set_border_width(&style_mode_carousel_1_extra_element_items_focused, 0);
	lv_style_set_radius(&style_mode_carousel_1_extra_element_items_focused, 5);
	lv_style_set_shadow_width(&style_mode_carousel_1_extra_element_items_focused, 0);
	lv_obj_add_style(ui->mode_carousel_1_mode3, &style_mode_carousel_1_extra_element_items_focused, LV_PART_MAIN|LV_STATE_FOCUSED);
	lv_obj_add_style(ui->mode_carousel_1_mode2, &style_mode_carousel_1_extra_element_items_focused, LV_PART_MAIN|LV_STATE_FOCUSED);
	lv_obj_add_style(ui->mode_carousel_1_mode1, &style_mode_carousel_1_extra_element_items_focused, LV_PART_MAIN|LV_STATE_FOCUSED);



	//Write codes mode_img_1
	ui->mode_img_1 = lv_img_create(ui->mode_carousel_1_mode1);
	lv_obj_add_flag(ui->mode_img_1, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->mode_img_1, &_mode1_alpha_100x100);
	lv_img_set_pivot(ui->mode_img_1, 50, 50);
	lv_img_set_angle(ui->mode_img_1, 0);
	lv_obj_set_pos(ui->mode_img_1, 56, 70);
	lv_obj_set_size(ui->mode_img_1, 100, 100);

	//Write style for mode_img_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_recolor_opa(ui->mode_img_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->mode_img_1, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->mode_img_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_clip_corner(ui->mode_img_1, true, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes mode_label_1
	ui->mode_label_1 = lv_label_create(ui->mode_carousel_1_mode1);
	lv_label_set_text(ui->mode_label_1, "mode1");
	lv_label_set_long_mode(ui->mode_label_1, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->mode_label_1, 55, 177);
	lv_obj_set_size(ui->mode_label_1, 100, 16);

	//Write style for mode_label_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->mode_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->mode_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->mode_label_1, lv_color_hex(0xe2dfdf), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->mode_label_1, &lv_font_FontAwesome5_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_opa(ui->mode_label_1, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->mode_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->mode_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->mode_label_1, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->mode_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->mode_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->mode_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->mode_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->mode_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->mode_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);



	//Write codes mode_img_2
	ui->mode_img_2 = lv_img_create(ui->mode_carousel_1_mode2);
	lv_obj_add_flag(ui->mode_img_2, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->mode_img_2, &_mode2_alpha_100x100);
	lv_img_set_pivot(ui->mode_img_2, 50, 50);
	lv_img_set_angle(ui->mode_img_2, 0);
	lv_obj_set_pos(ui->mode_img_2, 56, 70);
	lv_obj_set_size(ui->mode_img_2, 100, 100);

	//Write style for mode_img_2, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_recolor_opa(ui->mode_img_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->mode_img_2, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->mode_img_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_clip_corner(ui->mode_img_2, true, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes mode_label_2
	ui->mode_label_2 = lv_label_create(ui->mode_carousel_1_mode2);
	lv_label_set_text(ui->mode_label_2, "mode2\n");
	lv_label_set_long_mode(ui->mode_label_2, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->mode_label_2, 55, 177);
	lv_obj_set_size(ui->mode_label_2, 100, 16);

	//Write style for mode_label_2, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->mode_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->mode_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->mode_label_2, lv_color_hex(0xe2dfdf), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->mode_label_2, &lv_font_FontAwesome5_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_opa(ui->mode_label_2, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->mode_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->mode_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->mode_label_2, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->mode_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->mode_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->mode_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->mode_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->mode_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->mode_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);



	//Write codes mode_img_3
	ui->mode_img_3 = lv_img_create(ui->mode_carousel_1_mode3);
	lv_obj_add_flag(ui->mode_img_3, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->mode_img_3, &_mode3_alpha_100x100);
	lv_img_set_pivot(ui->mode_img_3, 50, 50);
	lv_img_set_angle(ui->mode_img_3, 0);
	lv_obj_set_pos(ui->mode_img_3, 56, 70);
	lv_obj_set_size(ui->mode_img_3, 100, 100);

	//Write style for mode_img_3, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_recolor_opa(ui->mode_img_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->mode_img_3, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->mode_img_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_clip_corner(ui->mode_img_3, true, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes mode_label_3
	ui->mode_label_3 = lv_label_create(ui->mode_carousel_1_mode3);
	lv_label_set_text(ui->mode_label_3, "mode3");
	lv_label_set_long_mode(ui->mode_label_3, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->mode_label_3, 55, 177);
	lv_obj_set_size(ui->mode_label_3, 100, 16);

	//Write style for mode_label_3, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->mode_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->mode_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->mode_label_3, lv_color_hex(0xe2dfdf), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->mode_label_3, &lv_font_FontAwesome5_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_opa(ui->mode_label_3, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->mode_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->mode_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->mode_label_3, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->mode_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->mode_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->mode_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->mode_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->mode_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->mode_label_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//The custom code of mode.
	// ui_add_obj_to_encoder_group(ui->mode);
	// ui_add_obj_to_encoder_group(ui->mode_img_1);
	// ui_add_obj_to_encoder_group(ui->mode_img_2);
	// ui_add_obj_to_encoder_group(ui->mode_img_3);
	//Update current screen layout.
	lv_obj_update_layout(ui->mode);
	events_init_mode(ui);
}
