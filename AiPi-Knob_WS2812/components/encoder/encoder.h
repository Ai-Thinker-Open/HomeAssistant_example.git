/**
 * @file encode.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-08-29
 *
 * @copyright Copyright (c) 2023
 *
*/
#ifndef ENCODER_H
#define ENCODER_H

#include "bflb_core.h"
#include "touch.h"

extern void* encoder_task;
extern uint8_t element_id_old;
extern uint8_t mode_element_id_old;
// extern lv_ui guider_ui;
void encoder_process_task(void* param);
void Create_triled_drive_set_mode_task(void);
int8_t triled_drive_set_mode(uint8_t mode);
extern  uint8_t red;
extern uint8_t green;
extern uint8_t blue;
extern int32_t end;
#endif
