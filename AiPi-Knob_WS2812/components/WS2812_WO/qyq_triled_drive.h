#ifndef __QYQ_TRILED_DRIVE_H_
#define __QYQ_TRILED_DRIVE_H_
#include "stdint.h"
#include "bflb_wo.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "mem.h"
#include "bflb_irq.h"
#include "bl616_glb.h"
#include "bflb_mtimer.h"
#include "board.h"
#include "bflb_dma.h"
#ifdef __QYQ_TRILED_DRIVE_C_
#define QYQ_TRILED_DRIVE_EXT
#else
#define QYQ_TRILED_DRIVE_EXT extern
#endif

// TRILED 控制块
typedef struct
{
    uint8_t status; /**< TRILED状态：1表示打开，0表示关闭 */
    uint8_t red;    /**< 红色分量：范围为0-255 */
    uint8_t green;  /**< 绿色分量：范围为0-255 */
    uint8_t blue;   /**< 蓝色分量：范围为0-255 */
} qyq_triled_control_block_t;

extern uint8_t triled_drive[][8];
/**
 * @brief 初始化TRILED驱动
 * @param triledlist 指向TRITRILED控制块数组的指针，用于存储多个TRILED的配置信息
 * @param trilednumber 需要初始化的TRILED数量
 * @return int8_t 返回状态码，0表示成功，非0表示失败
 */
QYQ_TRILED_DRIVE_EXT int8_t qyq_triled_drive_init(qyq_triled_control_block_t* triledlist, uint8_t trilednumber);

/**
 * @brief 打开指定的TRILED
 * @param triled_id 需要打开的TRILED的ID
 * @return int8_t 返回状态码，0表示成功，非0表示失败
 */
QYQ_TRILED_DRIVE_EXT int8_t qyq_triled_drive_turn_on(uint8_t triled_id);

/**
 * @brief 打开所有的TRILED
 * @return int8_t 返回状态码，0表示成功，非0表示失败
 */
QYQ_TRILED_DRIVE_EXT int8_t qyq_triled_drive_turn_onall(void);

/**
 * @brief 关闭指定的TRILED
 * @param triled_id 需要关闭的TRILED的ID
 * @return int8_t 返回状态码，0表示成功，非0表示失败
 */
QYQ_TRILED_DRIVE_EXT int8_t qyq_triled_drive_turn_off(uint8_t triled_id);

/**
 * @brief 关闭所有的TRILED
 * @return int8_t 返回状态码，0表示成功，非0表示失败
 */
QYQ_TRILED_DRIVE_EXT int8_t qyq_triled_drive_turn_offall(void);

/**
 * @brief 切换指定的TRILED状态（打开->关闭或关闭->打开）
 * @param triled_id 需要切换状态的TRILED的ID
 * @return int8_t 返回状态码，0表示成功，非0表示失败
 */
QYQ_TRILED_DRIVE_EXT int8_t qyq_triled_drive_toggle(uint8_t triled_id);

/**
 * @brief 获取指定TRILED的当前状态
 * @param triled_id 需要查询的TRILED的ID
 * @return int8_t 返回TRILED的当前状态，1表示打开，0表示关闭，负数表示错误
 */
QYQ_TRILED_DRIVE_EXT int8_t qyq_triled_drive_get_state(uint8_t triled_id, qyq_triled_control_block_t* triledstatus);

/**
 * @brief 设置指定TRILED的颜色
 * @param triled_id 需要设置颜色的TRILED的ID
 * @param red 红色分量值（0-255）
 * @param green 绿色分量值（0-255）
 * @param blue 蓝色分量值（0-255）
 * @return int8_t 返回状态码，0表示成功，非0表示失败
 */
QYQ_TRILED_DRIVE_EXT int8_t qyq_triled_drive_set_color(uint8_t triled_id, uint8_t red, uint8_t green, uint8_t blue);

/**
 * @brief 设置所有TRILED的颜色
 * @param triled_id 需要设置颜色的TRILED的ID
 * @param red 红色分量值（0-255）
 * @param green 绿色分量值（0-255）
 * @param blue 蓝色分量值（0-255）
 * @return int8_t 返回状态码，0表示成功，非0表示失败
 */

QYQ_TRILED_DRIVE_EXT int8_t qyq_triled_drive_set_allcolor(uint8_t red, uint8_t green, uint8_t blue);

/**
 * @brief 熄灭TRILED之后剩余灯珠
 * @param triled_id 需要设置颜色的TRILED的ID
 * @return int8_t 返回状态码，0表示成功，非0表示失败
 */

QYQ_TRILED_DRIVE_EXT int8_t qyq_triled_drive_set_othercolor(uint8_t triled_id);

QYQ_TRILED_DRIVE_EXT int8_t triled_drive_set_color_arry(uint8_t* triled_ids, uint8_t leds_numble, uint8_t red, uint8_t green, uint8_t blue);

QYQ_TRILED_DRIVE_EXT int8_t triled_drive_set_color_arry_no_start(uint8_t* triled_ids, uint8_t leds_numble, uint8_t red, uint8_t green, uint8_t blue);

QYQ_TRILED_DRIVE_EXT void  triled_drive_set_color_start(void);

QYQ_TRILED_DRIVE_EXT uint8_t triled_drive_get_status_all(void);
#endif // __QYQ_TRILED_DRIVE_H_
