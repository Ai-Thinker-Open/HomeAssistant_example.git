/**
 * @file main.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-01-23
 *
 * @copyright Copyright (c) 2024
 *
*/
#include <FreeRTOS.h>
#include "task.h"
#include "queue.h"
#include "board.h"
#include "bluetooth.h"
#include "btble_lib_api.h"
#include "bl616_glb.h"
#include "rfparam_adapter.h"
#include "bflb_mtd.h"
#include "easyflash.h"
#include "wifi_event.h"
#include "log.h"
#include "aiio_os_port.h"
#include "homeAssistantPort.h"

#define DBG_TAG "MAIN"
struct bflb_device_s* gpio;//全局变量

void ha_event_cb(ha_event_t event, homeAssisatnt_device_t* ha_dev)
{
    switch (event)
    {
        case HA_EVENT_MQTT_CONNECED:
            LOG_I("<<<<<<<<<<  HA_EVENT_MQTT_CONNECED");
            //一定要加static
            static ha_sw_entity_t entity_sw1 = {
                 .name = "开关1",
                 .icon = "mdi:power",
                 .unique_id = "sw1"
            };
            static ha_sw_entity_t entity_sw2 = {
                 .name = "开关2",
                 .icon = "mdi:power",
                .unique_id = "sw2",
            };
            static ha_sw_entity_t entity_sw3 = {
                 .name = "开关3",
                 .icon = "mdi:power",
                .unique_id = "sw3",
            };
            static ha_text_entity_t text1 = {
                .name = "传感器定时",
                .unique_id = "time_cont",
                .max = 255,
                .min = 1,
                .text_value = "0",
            };
            //创建二进制实体
            // static ha_Bsensor_entity_t entity_binary_sensor = {
            //     .name = "二进制传感器",
            //     .unique_id = "binary_sensor1",
            // };
            // //向HomeAssistant 添加二进制实体
            // homeAssistant_device_add_entity(CONFIG_HA_ENTITY_BINARY_SENSOR, &entity_binary_sensor);

            homeAssistant_device_add_entity(CONFIG_HA_ENTITY_SWITCH, &entity_sw1);
            homeAssistant_device_add_entity(CONFIG_HA_ENTITY_SWITCH, &entity_sw2);
            homeAssistant_device_add_entity(CONFIG_HA_ENTITY_SWITCH, &entity_sw3);
            homeAssistant_device_add_entity(CONFIG_HA_ENTITY_TEXT, &text1);
            homeAssistant_device_send_status(HOMEASSISTANT_STATUS_ONLINE);

            homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, &entity_sw1, 0);
            homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, &entity_sw2, 0);
            homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, &entity_sw3, 0);
            homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_TEXT, &text1, 0);
            // homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_BINARY_SENSOR, &entity_binary_sensor, 1);
            // update_all_entity_to_homeassistant();
            break;
        case HA_EVENT_MQTT_DISCONNECT:
            LOG_I("<<<<<<<<<<  HA_EVENT_MQTT_DISCONNECT");

            break;
        case HA_EVENT_MQTT_COMMAND_SWITCH:
            LOG_I("<<<<<<<<<<  HA_EVENT_MQTT_COMMAND_SWITCH");
            // LOG_I("switch addr =%p", ha_dev->entity_switch->command_switch);
            LOG_I(" switch %s is %s", ha_dev->entity_switch->command_switch->name, ha_dev->entity_switch->command_switch->switch_state?"true":"flase");
            int ret = homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, ha_dev->entity_switch->command_switch, ha_dev->entity_switch->command_switch->switch_state);
            if (ret!=-1)LOG_I("%s send entity suceess,state=%s", ha_dev->entity_switch->command_switch->name, ha_dev->entity_switch->command_switch->switch_state?"true":"flase");

            ha_sw_entity_t* sw1_s = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "sw1");

            if (ha_dev->entity_switch->command_switch->unique_id==sw1_s->unique_id&&ha_dev->entity_switch->command_switch->switch_state) {
                bflb_gpio_set(gpio, GPIO_PIN_15);
                // homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_BINARY_SENSOR, homeAssistant_fine_entity(CONFIG_HA_ENTITY_BINARY_SENSOR, "binary_sensor1"), 1);
                // ha_text_entity_t* text = homeAssistant_fine_entity(CONFIG_HA_ENTITY_TEXT, "time_cont");
                // memset(text->text_value, 0, 256);
                // sprintf(text->text_value, "80");
                // homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_TEXT, text, 0);
            }
            else if (ha_dev->entity_switch->command_switch->unique_id==sw1_s->unique_id&&!ha_dev->entity_switch->command_switch->switch_state) {
                bflb_gpio_reset(gpio, GPIO_PIN_15);
                // ha_text_entity_t* text = homeAssistant_fine_entity(CONFIG_HA_ENTITY_TEXT, "time_cont");
                // memset(text->text_value, 0, 256);
                // sprintf(text->text_value, "90");
                // // homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_BINARY_SENSOR, homeAssistant_fine_entity(CONFIG_HA_ENTITY_BINARY_SENSOR, "binary_sensor1"), 0);
                // homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_TEXT, text, 0);
            }
            HA_LOG_F("HomeAssistant free heap size=%d\r\n", aiio_os_get_free_heap_size());
            break;
        case HA_EVENT_MQTT_COMMAND_TEXT_VALUE:
            LOG_I("<<<<<<<<<<  HA_EVENT_MQTT_COMMAND_TEXT_VALUE");
            // HA_LOG_W("%s=%s\r\n", ha_dev->entity_text->command_text->name, ha_dev->entity_text->command_text->text_value);
            // homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_TEXT, ha_dev->entity_text->command_text, 0);
            // HA_LOG_F("HomeAssistant free heap size=%d\r\n", aiio_os_get_free_heap_size());
            // vPortFree();
            break;
        default:
            break;
    }
}

int main(void)
{

    board_init();
    bflb_mtd_init();
    easyflash_init();
    // aiio_log_init();

    if (0 != rfparam_init(0, NULL, 0)) {
        LOG_E("PHY RF init failed!\r\n");
        return 0;
    }
    gpio = bflb_device_get_by_name("gpio");
    bflb_gpio_init(gpio, GPIO_PIN_15, GPIO_OUTPUT | GPIO_PULLUP | GPIO_SMT_EN | GPIO_DRV_0);
    bflb_gpio_reset(gpio, GPIO_PIN_15);
    staWiFiInit();
    // homeassistant_blufi_init();
    homeAssisatnt_device_t ha_device = {
      .mqtt_info.mqtt_host = "你的服务器地址",
      .mqtt_info.port = 1883,
      .mqtt_info.mqtt_username = "myHomeAssistant",
      .mqtt_info.mqtt_password = "mypassword",
      .name = "myDevice",
      .model = "Ai-M6x-12F",
      .manufacturer = "Ai-Thinker",
      .hw_version = "v1.0",
      .sw_version = "v1.0.0",
    };
    unsigned char mac[6];
    homeAssistant_get_sta_mac(mac);
    ha_device.mqtt_info.mqtt_clientID = pvPortMalloc(12);
    sprintf(ha_device.mqtt_info.mqtt_clientID, "%02x%02x%02x%02x%02x%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    homeAssistant_device_init(&ha_device, ha_event_cb);
    vTaskStartScheduler();

    while (1) {
        LOG_F("HomeAssistant free heap size=%d", aiio_os_get_free_heap_size());
        vTaskDelay(pdMS_TO_TICKS(3000));
    }
}
