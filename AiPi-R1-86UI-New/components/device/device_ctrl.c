/**
 * @file device_ctrl.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-12-15
 *
 * @copyright Copyright (c) 2023
 *
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "device_ctrl.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "gui_guider.h"
#include "user_esflash.h"
#include "https_client.h"
#include "cJSON.h"
#include <lwip/tcpip.h>
#include "wifi_event.h"
#include "bl_fw_api.h"
#include <lwip/sockets.h>
#include <lwip/inet.h>
#include "wifi_mgmr_ext.h"
#include "wifi_mgmr.h"
#include "lwip/dns.h"
#include "log.h"
#include "https_client.h"
#include "aiio_system.h"
#include "aiio_wifi.h"
#include "ha_mqtt.h"
#include "events_init.h"
#define DBG_TAG "device_ctrl"

#define TIMER_STEP 3 //定时3 小时更新一次时间

void* device_task;
char wea[16] = { 0 };
char city[32] = { 0 };
char tem_day[16] = { 0 };
char tem_tem_night[16] = { 0 };
char win[16] = { 0 };
char air[16] = { 0 };
char humidity[16] = { 0 };
char pressure[16] = { 0 };

static xTimerHandle http_timer;

static void http_timer_cb(TimerHandle_t xTimer)
{
    uint32_t ulCount;
    BaseType_t xHigherPriorityTaskWoken;
    ulCount = (uint32_t)pvTimerGetTimerID(xTimer);
    ulCount++;
    if (ulCount>=TIMER_STEP) {
        ulCount = 0;

        vTimerSetTimerID(xTimer, (void*)ulCount);
        vTaskResume(https_Handle);
        xTimerStop(xTimer, 0);
    }
    else {
        vTimerSetTimerID(xTimer, (void*)ulCount);

    }

}

int event_get_mode(void);
void device_state_task(void* arg)
{
    lv_ui* ui = (lv_ui*)arg;
    static  device_state_t device_state;
    static char* ssid;
    static char* password;
    static ip_addr_t dns_addr;
    esay_flash_init();
    dns_init();
    http_timer = xTimerCreate("http timer", pdMS_TO_TICKS(1000*60*60), pdTRUE, 0, http_timer_cb);

    xTaskCreate(https_get_weather_task, "http", 1024*3, &guider_ui, 4, (TaskHandle_t*)&https_Handle);
    while (1)
    {
        xTaskNotifyWait(0xffffffff, 0x0, &device_state, portMAX_DELAY);
        switch (device_state)
        {
            case DEVICE_STATE_SYSTEM_START:
            {
                LOG_I("DEVICE_STATE_SYSTEM_START");
                //读取Flash WiFi 节点
                if (flash_check_key(FLASH_SSID_KEY)) {
                    ssid = flash_get_data(FLASH_SSID_KEY, 64);
                    password = flash_get_data(FLASH_PASS_KEY, 32);
                    LOG_I("wifi connect ......");
                    staWiFiConnect(ssid, password);
                    vPortFree(ssid);
                    vPortFree(password);
                }
                else {
                    ssid = pvPortMalloc(64);
                    password = pvPortMalloc(32);
                    sprintf(ssid, "FAE@Seahi");
                    sprintf(password, "fae12345678");
                    staWiFiConnect(ssid, password);
                    vPortFree(ssid);
                    vPortFree(password);
                }
            }
            break;
            case DEVICE_STATE_WIFI_CONNECT:
            {
                LOG_I(" DEVICE_STATE_WIFI_CONNECT");
            }
            break;
            case DEVICE_STATE_WIFI_CONNECT_OK:
            {
                LOG_I("DEVICE_STATE_WIFI_CONNECT_OK");
            }
            break;
            case  DEVICE_STATE_WIFI_GO_IP:
            {
                uint32_t ipv4_addr = 0;
                wifi_sta_ip4_addr_get(&ipv4_addr, NULL, NULL, NULL);
                LOG_I("DEVICE_STATE_WIFI_GO_IP:%s", inet_ntoa(ipv4_addr));
                // wifi_mgmr_sta_connect_params();
                aiio_wifi_sta_connect_ind_stat_info_t sta_info;
                if (!lv_obj_has_flag(ui->screen_cont_10, LV_OBJ_FLAG_HIDDEN))
                    lv_obj_add_flag(ui->screen_cont_10, LV_OBJ_FLAG_HIDDEN);
                aiio_wifi_sta_connect_ind_stat_get(&sta_info);
                flash_erase_set(FLASH_PASS_KEY, sta_info.passphr);
                flash_erase_set(FLASH_SSID_KEY, sta_info.ssid);
                //设置DNS 服务器
                inet_aton("223.5.5.5", &dns_addr.addr);
                dns_setserver(0, &dns_addr);
                // vTaskResume(https_Handle);
                //连接MQTT 服务器
                homeAssistant_dev.wifi_ssid = sta_info.ssid;
                homeAssistant_dev.wifi_password = sta_info.passphr;
                memcpy(homeAssistant_dev.bssid, sta_info.bssid, 6);
                ha_dev_mqtt_connenct();
            }
            break;
            case DEVICE_STATE_WIFI_DISCONNECT:
            {
                LOG_I("DEVICE_STATE_WIFI_DISCONNECT");
                // ssid = flash_get_data(FLASH_SSID_KEY, 64);
                // password = flash_get_data(FLASH_PASS_KEY, 32);
                // LOG_I("wifi connect ......");
                // staWiFiConnect(ssid, password);
            }
            break;
            case DEVICE_STATE_SYSTEM_TIME_UPDATE:
            {
                LOG_I("DEVICE_STATE_SYSTEM_TIME_UPDATE");
            }
            break;
            case DEVICE_STATE_HTTP_GET_WEATHER:
            {
                //获取天气的功能在HTTP 源码中
                LOG_I("DEVICE_STATE_HTTP_GET_WEATHER");
                lv_label_set_text_fmt(ui->screen_label_7, "%s市：#ffa2c2 %s# ", city, wea);
                lv_label_set_text_fmt(ui->screen_label_temp_day, "白天温度：%s ℃", tem_day);
                lv_label_set_text_fmt(ui->screen_label_temp_night, "夜间温度：%s ℃", tem_tem_night);
                lv_label_set_text_fmt(ui->screen_label_win, "风向：%s", win);
                lv_label_set_text_fmt(ui->screen_label_pre, "气压：%s\n", pressure);
                lv_label_set_text_fmt(ui->screen_label_air, "空气质量：%s", air);
                lv_label_set_text_fmt(ui->screen_label_humi, "湿度：%s%", humidity);

                xTimerStart(http_timer, 0);
            }
            break;
            case DEVICE_STATE_BLUFI_CONFIG_START:
            {
                LOG_I("DEVICE_STATE_BLUFI_CONFIG_START");
                //断开MQTT
                ha_dev_mqtt_deint();
                //配网
                homeassistant_blufi_start();
            }
            break;
            case DEVICE_STATE_RELAY1_ON:
            {
                LOG_I("DEVICE_STATE_RELAY1_ON");
                lv_event_send(ui->screen_imgbtn_sw1, LV_EVENT_CLICKED, NULL);
            }
            break;
            case DEVICE_STATE_RELAY1_OFF:
            {
                LOG_I("DEVICE_STATE_RELAY1_OFF");
                lv_event_send(ui->screen_imgbtn_sw1, LV_EVENT_CLICKED, NULL);
            }
            break;
            case DEVICE_STATE_RELAY2_ON:
            {
                LOG_I("DEVICE_STATE_RELAY2_ON");
                lv_event_send(ui->screen_imgbtn_sw2, LV_EVENT_CLICKED, NULL);
            }
            break;
            case DEVICE_STATE_RELAY2_OFF:
            {
                LOG_I("DEVICE_STATE_RELAY2_OFF");
                lv_event_send(ui->screen_imgbtn_sw2, LV_EVENT_CLICKED, NULL);
            }
            break;
            case DEVICE_STATE_RELAY3_ON:
            {
                LOG_I("DEVICE_STATE_RELAY3_ON");
                lv_event_send(ui->screen_imgbtn_sw3, LV_EVENT_CLICKED, NULL);
            }
            break;
            case DEVICE_STATE_RELAY3_OFF:
            {
                LOG_I("DEVICE_STATE_RELAY3_OFF");
                lv_event_send(ui->screen_imgbtn_sw3, LV_EVENT_CLICKED, NULL);
            }
            break;
            case DEVICE_STATE_XIAOMI_ON:
                LOG_I("DEVICE_STATE_XIAOMI_ON");
                lv_event_send(ui->screen_imgbtn_sw4, LV_EVENT_CLICKED, NULL);
                break;
            case DEVICE_STATE_XIAOMI_OFF:
                LOG_I("DEVICE_STATE_XIAOMI_OFF");
                lv_event_send(ui->screen_imgbtn_sw4, LV_EVENT_CLICKED, NULL);
                break;
            case DEVICE_STATE_HOMEKIT_ON:
                LOG_I("DEVICE_STATE_HOMEKIT_ON");
                lv_event_send(ui->screen_imgbtn_sw5, LV_EVENT_CLICKED, NULL);
                break;
            case DEVICE_STATE_RHOMEKIT_OFF:
                LOG_I("DEVICE_STATE_HOMEKIT_OFF");
                lv_event_send(ui->screen_imgbtn_sw5, LV_EVENT_CLICKED, NULL);
                break;
            case DEVICE_STATE_RELAY_ALL_ON:
            {
                LOG_I("DEVICE_STATE_RELAY_ALL_ON");
                lv_event_send(ui->screen_btn_sw_all, LV_EVENT_CLICKED, NULL);
            }
            break;
            case DEVICE_STATE_RELAY_ALL_OFF:
            {
                LOG_I("DEVICE_STATE_RELAY_ALL_OFF");
                lv_event_send(ui->screen_btn_sw_all, LV_EVENT_CLICKED, NULL);
            }
            break;
            case DEVICE_STATE_MODE_OUTHOME:
            {
                LOG_I("DEVICE_STATE_MODE_OUTHOME");
                lv_event_send(ui->screen_img_2, LV_EVENT_CLICKED, NULL);
            }
            break;
            case DEVICE_STATE_MODE_WORK:
            {
                LOG_I(" DEVICE_STATE_MODE_WORK");
                lv_event_send(ui->screen_img_1, LV_EVENT_CLICKED, NULL);
            }
            break;
            case DEVICE_STATE_MODE_SLEEP:
            {
                LOG_I("DEVICE_STATE_MODE_SLEEP");
                lv_event_send(ui->screen_img_3, LV_EVENT_CLICKED, NULL);
            }
            break;
            case DEVICE_STATE_MODE_VIDIO:
            {
                LOG_I("DEVICE_STATE_MODE_VIDIO");
                lv_event_send(ui->screen_img_4, LV_EVENT_CLICKED, NULL);
            }
            break;
            case DEVICE_STATE_MODE_NONE:
            {
                LOG_I("DEVICE_STATE_MODE_NONE mode=%d", event_get_mode());
                switch (event_get_mode())
                {
                    case DEVICE_MODE_OUT_HOME:

                        lv_event_send(ui->screen_img_2, LV_EVENT_CLICKED, NULL);
                        break;
                    case DEVICE_MODE_WORK:
                        lv_event_send(ui->screen_img_1, LV_EVENT_CLICKED, NULL);
                        break;
                    case DEVICE_MODE_SLEEP:
                        lv_event_send(ui->screen_img_3, LV_EVENT_CLICKED, NULL);
                        break;
                    case DEVICE_MODE_VIDEO:
                        lv_event_send(ui->screen_img_4, LV_EVENT_CLICKED, NULL);
                        break;
                    default:
                        break;
                }
            }
            break;
            default:
                break;
        }
    }

}

void device_send_state_notify(device_state_t device_state, int is_irq)
{
    if (device_task==NULL) {
        LOG_E("device_task is no init");
        return;
    }
    BaseType_t xHigherPriorityTaskWoken;
    xHigherPriorityTaskWoken = pdFALSE;
    if (is_irq)xTaskNotifyFromISR((TaskHandle_t)device_task, device_state, eSetValueWithoutOverwrite, &xHigherPriorityTaskWoken);
    else xTaskNotify((TaskHandle_t)device_task, device_state, eSetValueWithoutOverwrite);
}