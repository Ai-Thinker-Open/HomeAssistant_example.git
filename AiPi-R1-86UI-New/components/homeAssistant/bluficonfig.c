/**
 * @file bluficonfig.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-01-23
 *
 * @copyright Copyright (c) 2024
 *
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bluficonfig.h"
#include "log.h"
#include "aiio_ble.h"
#define DBG_TAG "BLUFI"


// static aiio_blufi_cb_t blufi_cb = { 0 };
static bool blufi_state = 0;
static unsigned char _blufi_name[31] = { 0 };

void homeassistant_blufi_init(char* blufi_name)
{
    uint8_t ble_mac[6] = { 0 };
    aiio_wifi_sta_mac_get(ble_mac);
    memset(_blufi_name, 0, 31);
    if (blufi_name==NULL) {
        sprintf(_blufi_name, "%s-%02x%02x", BLUFI_NAME, ble_mac[4], ble_mac[5]);
    }
    else {
        sprintf(_blufi_name, "%s-%02x%02x", blufi_name, ble_mac[4], ble_mac[5]);
    }

    blufi_set_name(_blufi_name);
}
/**
 * @brief 启动配网
 *
*/
void homeassistant_blufi_start(void)
{
    blufi_start();
    blufi_state = true;
}

int homeassistant_get_blufi_state(void)
{
    return blufi_state;
}

char* homeassistant_get_blufi_name(void)
{
    return _blufi_name;
}
/**
 * @brief 更新配网状态
 *
 * @param blufi_event
*/
void update_homeassistant_blufi_event(int blufi_event)
{
    if (!blufi_state) return;
    // aiio_blufi_wifi_event(blufi_event, NULL);
    blufi_wifi_evt_export(blufi_event);

}