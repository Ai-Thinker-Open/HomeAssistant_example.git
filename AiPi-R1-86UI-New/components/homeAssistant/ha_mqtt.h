/**
 * @file ha_mqtt.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-01-24
 *
 * @copyright Copyright (c) 2024
 *
*/
#ifndef HA_MQTT_H
#define HA_MQTT_H
#include "aiio_mqtt_client.h"

#define CONFIG_HA_AUTOMATIC_DISCOVERY "homeassistant"
#define CONFIG_HOMEASSISTANT_MQTT_DISCOVERY "switch"
#define HA_AUTOMATIC_DISCOVERY CONFIG_HA_AUTOMATIC_DISCOVERY //HomeAssistant 自动发现的前缀
#define HA_DISCOVERY_SWITCH CONFIG_HOMEASSISTANT_MQTT_DISCOVERY //HomeAssistant 实体类型 

#define CONFIG_MQTT_PORT 1883
#define CONFIG_MQTT_HOST "wx.ai-thinker.com"
#define CONFIG_DEVICE_TYPE "AiPi-86box"

typedef struct {
    char* wifi_ssid;
    unsigned char bssid[6];
    char* wifi_password;
    char* mqtt_host;
    char* mqtt_username;
    char* mqtt_password;
    char* dev_mac;
    char* dev_name;
    unsigned int port;
}ha_devMsg_t;

typedef struct {
    char* name;
    char* identifiers[32];
    int identifiers_len;
    char* manufacturer;
    char* model;
    char* sw_version;
}ha_msg_dev_t;

typedef struct discovery_payload {
    char* name;
    char* sw_version;
    char* command_topic;
    char* state_topic;
    char* unique_id;
    ha_msg_dev_t device;
    char* availability_topic;
    struct discovery_payload* next;
}discovery_payload_t;

typedef enum {
    HOMEASSISTANT_EVENT_NONE = 0,
    HOMEASSISTANT_EVENT_ONLINE,
    HOMEASSISTANT_EVENT_OFFLINE,
    HOMEASSISTANT_EVENT_RELY1_ON,
    HOMEASSISTANT_EVENT_RELY1_OFF,
    HOMEASSISTANT_EVENT_RELY2_ON,
    HOMEASSISTANT_EVENT_RELY2_OFF,
    HOMEASSISTANT_EVENT_RELY3_ON,
    HOMEASSISTANT_EVENT_RELY3_OFF,
    HOMEASSISTANT_EVENT_RELY_ALL_ON,
    HOMEASSISTANT_EVENT_RELY_ALL_OFF,
    HOMEASSISTANT_EVENT_MOD_WORK,
    HOMEASSISTANT_EVENT_MOD_OUTHOME,
    HOMEASSISTANT_EVENT_MOD_SLEEP,
    HOMEASSISTANT_EVENT_MOD_VIDIO,
    HOMEASSISTANT_EVENT_MOD_OFF,
    HOMEASSISTANT_EVENT_MOD_XIAOMISW_ON,
    HOMEASSISTANT_EVENT_MOD_XIAOMISW_OFF,
    HOMEASSISTANT_EVENT_MOD_HOMEKITSW_ON,
    HOMEASSISTANT_EVENT_MOD_HOMEKITSW_OFF,
    HOMEASSISTANT_EVENT_ERR,
}ha_event_t;

extern ha_devMsg_t homeAssistant_dev;
extern aiio_mqtt_client_handle_t fd_client;
extern discovery_payload_t discovery_payload[10];
void ha_dev_mqtt_init(ha_devMsg_t* devMsg);
void ha_dev_mqtt_deint(void);
void ha_dev_mqtt_connenct(void);
void homeAssistant_create_device(aiio_mqtt_client_handle_t client, discovery_payload_t* discovery_payload, const char* entity_type, const char* s_name);
// void homeAssistant_create_device(aiio_mqtt_client_handle_t client, discovery_payload_t* discovery_payload);
void homeAssistant_send_state(aiio_mqtt_client_handle_t client, discovery_payload_t* discovery_payload, int state);
ha_event_t homeassistant_get_event(discovery_payload_t* discovery_payload, const char* topic, int topic_len, const char* payload, int payload_len);
void homeassistant_send_status(char* status);
void homeassistant_send_mode(ha_event_t ha_event);
#endif