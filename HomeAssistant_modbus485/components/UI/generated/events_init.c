/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/

#include "events_init.h"
#include <stdio.h>
#include "lvgl.h"


static void main_event_handler (lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);

	switch (code) {
	case LV_EVENT_GESTURE:
	{
		lv_dir_t dir = lv_indev_get_gesture_dir(lv_indev_get_act());
		switch(dir) {
			case LV_DIR_LEFT:
			{
				lv_indev_wait_release(lv_indev_get_act());
				//Write the load screen code.
			    lv_obj_t * act_scr = lv_scr_act();
			    lv_disp_t * d = lv_obj_get_disp(act_scr);
			    if (d->prev_scr == NULL && (d->scr_to_load == NULL || d->scr_to_load == act_scr)) {
			        if (guider_ui.comfig_del == true) {
			          setup_scr_comfig(&guider_ui);
			        }
			        lv_scr_load_anim(guider_ui.comfig, LV_SCR_LOAD_ANIM_OVER_LEFT, 200, 50, true);
			        guider_ui.main_del = true;
			    }
				break;
			}
		}
		break;
	}
	default:
		break;
	}
}
void events_init_main(lv_ui *ui)
{
	lv_obj_add_event_cb(ui->main, main_event_handler, LV_EVENT_ALL, NULL);
}
static void comfig_event_handler (lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);

	switch (code) {
	case LV_EVENT_GESTURE:
	{
		lv_dir_t dir = lv_indev_get_gesture_dir(lv_indev_get_act());
		switch(dir) {
			case LV_DIR_RIGHT:
			{
				lv_indev_wait_release(lv_indev_get_act());
				//Write the load screen code.
			    lv_obj_t * act_scr = lv_scr_act();
			    lv_disp_t * d = lv_obj_get_disp(act_scr);
			    if (d->prev_scr == NULL && (d->scr_to_load == NULL || d->scr_to_load == act_scr)) {
			        if (guider_ui.main_del == true) {
			          setup_scr_main(&guider_ui);
			        }
			        lv_scr_load_anim(guider_ui.main, LV_SCR_LOAD_ANIM_OVER_RIGHT, 200, 50, true);
			        guider_ui.comfig_del = true;
			    }
				break;
			}
		}
		break;
	}
	default:
		break;
	}
}
void events_init_comfig(lv_ui *ui)
{
	lv_obj_add_event_cb(ui->comfig, comfig_event_handler, LV_EVENT_ALL, NULL);
}

void events_init(lv_ui *ui)
{

}
