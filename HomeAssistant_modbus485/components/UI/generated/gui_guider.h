/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/

#ifndef GUI_GUIDER_H
#define GUI_GUIDER_H
#ifdef __cplusplus
extern "C" {
#endif

#include "lvgl.h"

typedef struct
{
  
	lv_obj_t *main;
	bool main_del;
	lv_obj_t *main_cont_pm25;
	lv_obj_t *main_label_PM25_lable;
	lv_obj_t *main_label_pm25_value;
	lv_obj_t *main_label_pm25;
	lv_obj_t *main_img_pm25;
	lv_obj_t *main_cont_lu;
	lv_obj_t *main_label_lu;
	lv_obj_t *main_label_lu_value;
	lv_obj_t *main_label_lu_d;
	lv_obj_t *main_img_lu;
	lv_obj_t *main_label_title;
	lv_obj_t *main_cont_co2;
	lv_obj_t *main_label_co2;
	lv_obj_t *main_label_2;
	lv_obj_t *main_label_1;
	lv_obj_t *main_img_1;
	lv_obj_t *main_cont_PM10;
	lv_obj_t *main_label_pm10;
	lv_obj_t *main_label_pm10_value;
	lv_obj_t *main_label_pm10_d;
	lv_obj_t *main_img_pm10;
	lv_obj_t *comfig;
	bool comfig_del;
	lv_obj_t *comfig_label_netMSG;
	lv_obj_t *comfig_label_ssid;
	lv_obj_t *comfig_label_PWD;
	lv_obj_t *comfig_label_mqtt_host;
	lv_obj_t *comfig_label_MQTT_msg;
	lv_obj_t *comfig_label_port;
	lv_obj_t *comfig_img_ha;
	lv_obj_t *comfig_img_wxconfig;
	lv_obj_t *comfig_btn_config;
	lv_obj_t *comfig_btn_config_label;
}lv_ui;

void ui_init_style(lv_style_t * style);
void init_scr_del_flag(lv_ui *ui);
void setup_ui(lv_ui *ui);
extern lv_ui guider_ui;

void setup_scr_main(lv_ui *ui);
void setup_scr_comfig(lv_ui *ui);
LV_IMG_DECLARE(_molecule_alpha_20x20);
LV_IMG_DECLARE(_brightness_alpha_20x20);
LV_IMG_DECLARE(_moleculeco2_alpha_20x20);
LV_IMG_DECLARE(_molecule_alpha_20x20);
LV_IMG_DECLARE(_homeassistant_alpha_20x20);
LV_IMG_DECLARE(_code_alpha_130x130);

LV_FONT_DECLARE(lv_font_MiSans_Medium_12)
LV_FONT_DECLARE(lv_font_montserratMedium_16)
LV_FONT_DECLARE(lv_font_MiSans_Medium_16)
LV_FONT_DECLARE(lv_font_arial_16)
LV_FONT_DECLARE(lv_font_MiSans_Medium_14)
LV_FONT_DECLARE(lv_font_montserratMedium_12)


#ifdef __cplusplus
}
#endif
#endif
