/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/

#include "lvgl.h"
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "widgets_init.h"
#include "custom.h"


void setup_scr_main(lv_ui *ui)
{
	//Write codes main
	ui->main = lv_obj_create(NULL);
	lv_obj_set_size(ui->main, 320, 170);

	//Write style for main, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->main, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_cont_pm25
	ui->main_cont_pm25 = lv_obj_create(ui->main);
	lv_obj_set_pos(ui->main_cont_pm25, 35, 35);
	lv_obj_set_size(ui->main_cont_pm25, 124, 57);
	lv_obj_set_scrollbar_mode(ui->main_cont_pm25, LV_SCROLLBAR_MODE_OFF);

	//Write style for main_cont_pm25, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_cont_pm25, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->main_cont_pm25, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->main_cont_pm25, lv_color_hex(0x343434), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->main_cont_pm25, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_cont_pm25, 6, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_cont_pm25, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_cont_pm25, lv_color_hex(0x080808), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_cont_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_cont_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_cont_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_cont_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_cont_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_PM25_lable
	ui->main_label_PM25_lable = lv_label_create(ui->main_cont_pm25);
	lv_label_set_text(ui->main_label_PM25_lable, "PM2.5");
	lv_label_set_long_mode(ui->main_label_PM25_lable, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_PM25_lable, 5, 7);
	lv_obj_set_size(ui->main_label_PM25_lable, 46, 12);

	//Write style for main_label_PM25_lable, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_PM25_lable, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_PM25_lable, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_PM25_lable, lv_color_hex(0x9b9b9b), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_PM25_lable, &lv_font_MiSans_Medium_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_PM25_lable, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_PM25_lable, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_PM25_lable, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_PM25_lable, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_PM25_lable, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_PM25_lable, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_PM25_lable, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_PM25_lable, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_PM25_lable, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_pm25_value
	ui->main_label_pm25_value = lv_label_create(ui->main_cont_pm25);
	lv_label_set_text(ui->main_label_pm25_value, "---");
	lv_label_set_long_mode(ui->main_label_pm25_value, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_pm25_value, 2, 27);
	lv_obj_set_size(ui->main_label_pm25_value, 40, 16);

	//Write style for main_label_pm25_value, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_pm25_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_pm25_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_pm25_value, lv_color_hex(0xe1e1e1), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_pm25_value, &lv_font_MiSans_Medium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_pm25_value, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_pm25_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_pm25_value, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_pm25_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_pm25_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_pm25_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_pm25_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_pm25_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_pm25_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_pm25
	ui->main_label_pm25 = lv_label_create(ui->main_cont_pm25);
	lv_label_set_text(ui->main_label_pm25, "µg/m³");
	lv_label_set_long_mode(ui->main_label_pm25, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_pm25, 44, 30);
	lv_obj_set_size(ui->main_label_pm25, 50, 20);

	//Write style for main_label_pm25, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_pm25, lv_color_hex(0x9b9b9b), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_pm25, &lv_font_MiSans_Medium_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_pm25, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_pm25, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_img_pm25
	ui->main_img_pm25 = lv_img_create(ui->main_cont_pm25);
	lv_obj_add_flag(ui->main_img_pm25, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->main_img_pm25, &_molecule_alpha_20x20);
	lv_img_set_pivot(ui->main_img_pm25, 50,50);
	lv_img_set_angle(ui->main_img_pm25, 0);
	lv_obj_set_pos(ui->main_img_pm25, 89, 7);
	lv_obj_set_size(ui->main_img_pm25, 20, 20);

	//Write style for main_img_pm25, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_recolor_opa(ui->main_img_pm25, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_recolor(ui->main_img_pm25, lv_color_hex(0x44739e), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->main_img_pm25, 255, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_cont_lu
	ui->main_cont_lu = lv_obj_create(ui->main);
	lv_obj_set_pos(ui->main_cont_lu, 35, 100);
	lv_obj_set_size(ui->main_cont_lu, 124, 57);
	lv_obj_set_scrollbar_mode(ui->main_cont_lu, LV_SCROLLBAR_MODE_OFF);

	//Write style for main_cont_lu, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_cont_lu, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->main_cont_lu, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->main_cont_lu, lv_color_hex(0x343434), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->main_cont_lu, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_cont_lu, 6, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_cont_lu, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_cont_lu, lv_color_hex(0x080808), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_cont_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_cont_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_cont_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_cont_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_cont_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_lu
	ui->main_label_lu = lv_label_create(ui->main_cont_lu);
	lv_label_set_text(ui->main_label_lu, "光照度传感器");
	lv_label_set_long_mode(ui->main_label_lu, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_lu, 5, 7);
	lv_obj_set_size(ui->main_label_lu, 80, 16);

	//Write style for main_label_lu, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_lu, lv_color_hex(0x9b9b9b), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_lu, &lv_font_MiSans_Medium_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_lu, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_lu, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_lu_value
	ui->main_label_lu_value = lv_label_create(ui->main_cont_lu);
	lv_label_set_text(ui->main_label_lu_value, "---\n");
	lv_label_set_long_mode(ui->main_label_lu_value, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_lu_value, 2, 27);
	lv_obj_set_size(ui->main_label_lu_value, 53, 20);

	//Write style for main_label_lu_value, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_lu_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_lu_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_lu_value, lv_color_hex(0xe1e1e1), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_lu_value, &lv_font_MiSans_Medium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_lu_value, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_lu_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_lu_value, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_lu_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_lu_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_lu_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_lu_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_lu_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_lu_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_lu_d
	ui->main_label_lu_d = lv_label_create(ui->main_cont_lu);
	lv_label_set_text(ui->main_label_lu_d, "lux³");
	lv_label_set_long_mode(ui->main_label_lu_d, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_lu_d, 53, 30);
	lv_obj_set_size(ui->main_label_lu_d, 50, 20);

	//Write style for main_label_lu_d, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_lu_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_lu_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_lu_d, lv_color_hex(0x9b9b9b), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_lu_d, &lv_font_MiSans_Medium_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_lu_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_lu_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_lu_d, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_lu_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_lu_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_lu_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_lu_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_lu_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_lu_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_img_lu
	ui->main_img_lu = lv_img_create(ui->main_cont_lu);
	lv_obj_add_flag(ui->main_img_lu, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->main_img_lu, &_brightness_alpha_20x20);
	lv_img_set_pivot(ui->main_img_lu, 50,50);
	lv_img_set_angle(ui->main_img_lu, 0);
	lv_obj_set_pos(ui->main_img_lu, 90, 8);
	lv_obj_set_size(ui->main_img_lu, 20, 20);

	//Write style for main_img_lu, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_recolor_opa(ui->main_img_lu, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_recolor(ui->main_img_lu, lv_color_hex(0x44739e), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->main_img_lu, 255, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_title
	ui->main_label_title = lv_label_create(ui->main);
	lv_label_set_text(ui->main_label_title, "AiPi-Modbus485");
	lv_label_set_long_mode(ui->main_label_title, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_title, 72, 9);
	lv_obj_set_size(ui->main_label_title, 177, 14);

	//Write style for main_label_title, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_title, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_title, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_title, lv_color_hex(0xe1e1e1), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_title, &lv_font_arial_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_title, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_title, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_title, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_title, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_title, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_title, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_title, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_title, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_title, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_cont_co2
	ui->main_cont_co2 = lv_obj_create(ui->main);
	lv_obj_set_pos(ui->main_cont_co2, 165, 100);
	lv_obj_set_size(ui->main_cont_co2, 124, 57);
	lv_obj_set_scrollbar_mode(ui->main_cont_co2, LV_SCROLLBAR_MODE_OFF);

	//Write style for main_cont_co2, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_cont_co2, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->main_cont_co2, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->main_cont_co2, lv_color_hex(0x343434), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->main_cont_co2, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_cont_co2, 6, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_cont_co2, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_cont_co2, lv_color_hex(0x080808), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_cont_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_cont_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_cont_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_cont_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_cont_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_co2
	ui->main_label_co2 = lv_label_create(ui->main_cont_co2);
	lv_label_set_text(ui->main_label_co2, "二氧化碳浓度");
	lv_label_set_long_mode(ui->main_label_co2, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_co2, 5, 7);
	lv_obj_set_size(ui->main_label_co2, 80, 16);

	//Write style for main_label_co2, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_co2, lv_color_hex(0x9b9b9b), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_co2, &lv_font_MiSans_Medium_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_co2, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_co2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_2
	ui->main_label_2 = lv_label_create(ui->main_cont_co2);
	lv_label_set_text(ui->main_label_2, "---\n");
	lv_label_set_long_mode(ui->main_label_2, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_2, 2, 27);
	lv_obj_set_size(ui->main_label_2, 53, 20);

	//Write style for main_label_2, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_2, lv_color_hex(0xe1e1e1), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_2, &lv_font_MiSans_Medium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_2, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_2, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_1
	ui->main_label_1 = lv_label_create(ui->main_cont_co2);
	lv_label_set_text(ui->main_label_1, "ppm\n");
	lv_label_set_long_mode(ui->main_label_1, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_1, 53, 30);
	lv_obj_set_size(ui->main_label_1, 50, 20);

	//Write style for main_label_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_1, lv_color_hex(0x9b9b9b), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_1, &lv_font_MiSans_Medium_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_1, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_img_1
	ui->main_img_1 = lv_img_create(ui->main_cont_co2);
	lv_obj_add_flag(ui->main_img_1, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->main_img_1, &_moleculeco2_alpha_20x20);
	lv_img_set_pivot(ui->main_img_1, 50,50);
	lv_img_set_angle(ui->main_img_1, 0);
	lv_obj_set_pos(ui->main_img_1, 90, 8);
	lv_obj_set_size(ui->main_img_1, 20, 20);

	//Write style for main_img_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_recolor_opa(ui->main_img_1, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_recolor(ui->main_img_1, lv_color_hex(0x44739e), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->main_img_1, 255, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_cont_PM10
	ui->main_cont_PM10 = lv_obj_create(ui->main);
	lv_obj_set_pos(ui->main_cont_PM10, 165, 35);
	lv_obj_set_size(ui->main_cont_PM10, 124, 57);
	lv_obj_set_scrollbar_mode(ui->main_cont_PM10, LV_SCROLLBAR_MODE_OFF);

	//Write style for main_cont_PM10, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_cont_PM10, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->main_cont_PM10, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->main_cont_PM10, lv_color_hex(0x343434), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->main_cont_PM10, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_cont_PM10, 6, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_cont_PM10, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_cont_PM10, lv_color_hex(0x080808), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_cont_PM10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_cont_PM10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_cont_PM10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_cont_PM10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_cont_PM10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_pm10
	ui->main_label_pm10 = lv_label_create(ui->main_cont_PM10);
	lv_label_set_text(ui->main_label_pm10, "PM10\n");
	lv_label_set_long_mode(ui->main_label_pm10, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_pm10, 5, 7);
	lv_obj_set_size(ui->main_label_pm10, 46, 12);

	//Write style for main_label_pm10, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_pm10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_pm10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_pm10, lv_color_hex(0x9b9b9b), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_pm10, &lv_font_MiSans_Medium_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_pm10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_pm10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_pm10, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_pm10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_pm10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_pm10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_pm10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_pm10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_pm10, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_pm10_value
	ui->main_label_pm10_value = lv_label_create(ui->main_cont_PM10);
	lv_label_set_text(ui->main_label_pm10_value, "---");
	lv_label_set_long_mode(ui->main_label_pm10_value, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_pm10_value, 2, 27);
	lv_obj_set_size(ui->main_label_pm10_value, 40, 16);

	//Write style for main_label_pm10_value, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_pm10_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_pm10_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_pm10_value, lv_color_hex(0xe1e1e1), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_pm10_value, &lv_font_MiSans_Medium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_pm10_value, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_pm10_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_pm10_value, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_pm10_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_pm10_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_pm10_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_pm10_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_pm10_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_pm10_value, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_pm10_d
	ui->main_label_pm10_d = lv_label_create(ui->main_cont_PM10);
	lv_label_set_text(ui->main_label_pm10_d, "µg/m³");
	lv_label_set_long_mode(ui->main_label_pm10_d, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->main_label_pm10_d, 44, 30);
	lv_obj_set_size(ui->main_label_pm10_d, 50, 20);

	//Write style for main_label_pm10_d, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->main_label_pm10_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->main_label_pm10_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_pm10_d, lv_color_hex(0x9b9b9b), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_pm10_d, &lv_font_MiSans_Medium_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_pm10_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_pm10_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_pm10_d, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_pm10_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_pm10_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_pm10_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_pm10_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_pm10_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_pm10_d, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_img_pm10
	ui->main_img_pm10 = lv_img_create(ui->main_cont_PM10);
	lv_obj_add_flag(ui->main_img_pm10, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->main_img_pm10, &_molecule_alpha_20x20);
	lv_img_set_pivot(ui->main_img_pm10, 50,50);
	lv_img_set_angle(ui->main_img_pm10, 0);
	lv_obj_set_pos(ui->main_img_pm10, 89, 7);
	lv_obj_set_size(ui->main_img_pm10, 20, 20);

	//Write style for main_img_pm10, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_recolor_opa(ui->main_img_pm10, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_recolor(ui->main_img_pm10, lv_color_hex(0x44739e), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->main_img_pm10, 255, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Update current screen layout.
	lv_obj_update_layout(ui->main);

	
	//Init events for screen.
	events_init_main(ui);
}
