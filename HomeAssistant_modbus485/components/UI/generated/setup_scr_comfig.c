/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/

#include "lvgl.h"
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "widgets_init.h"
#include "custom.h"


void setup_scr_comfig(lv_ui *ui)
{
	//Write codes comfig
	ui->comfig = lv_obj_create(NULL);
	lv_obj_set_size(ui->comfig, 320, 170);

	//Write style for comfig, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->comfig, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->comfig, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes comfig_label_netMSG
	ui->comfig_label_netMSG = lv_label_create(ui->comfig);
	lv_label_set_text(ui->comfig_label_netMSG, "NetWork:");
	lv_label_set_long_mode(ui->comfig_label_netMSG, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->comfig_label_netMSG, 5, 10);
	lv_obj_set_size(ui->comfig_label_netMSG, 100, 15);

	//Write style for comfig_label_netMSG, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->comfig_label_netMSG, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->comfig_label_netMSG, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->comfig_label_netMSG, lv_color_hex(0x9b9b9b), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->comfig_label_netMSG, &lv_font_MiSans_Medium_14, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->comfig_label_netMSG, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->comfig_label_netMSG, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->comfig_label_netMSG, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->comfig_label_netMSG, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->comfig_label_netMSG, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->comfig_label_netMSG, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->comfig_label_netMSG, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->comfig_label_netMSG, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->comfig_label_netMSG, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes comfig_label_ssid
	ui->comfig_label_ssid = lv_label_create(ui->comfig);
	lv_label_set_text(ui->comfig_label_ssid, "ssid:fae23656");
	lv_label_set_long_mode(ui->comfig_label_ssid, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->comfig_label_ssid, 6, 31);
	lv_obj_set_size(ui->comfig_label_ssid, 210, 14);

	//Write style for comfig_label_ssid, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->comfig_label_ssid, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->comfig_label_ssid, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->comfig_label_ssid, lv_color_hex(0x068600), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->comfig_label_ssid, &lv_font_MiSans_Medium_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->comfig_label_ssid, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->comfig_label_ssid, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->comfig_label_ssid, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->comfig_label_ssid, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->comfig_label_ssid, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->comfig_label_ssid, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->comfig_label_ssid, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->comfig_label_ssid, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->comfig_label_ssid, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes comfig_label_PWD
	ui->comfig_label_PWD = lv_label_create(ui->comfig);
	lv_label_set_text(ui->comfig_label_PWD, "pwd:fae23656");
	lv_label_set_long_mode(ui->comfig_label_PWD, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->comfig_label_PWD, 6, 52);
	lv_obj_set_size(ui->comfig_label_PWD, 210, 14);

	//Write style for comfig_label_PWD, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->comfig_label_PWD, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->comfig_label_PWD, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->comfig_label_PWD, lv_color_hex(0x068600), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->comfig_label_PWD, &lv_font_MiSans_Medium_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->comfig_label_PWD, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->comfig_label_PWD, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->comfig_label_PWD, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->comfig_label_PWD, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->comfig_label_PWD, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->comfig_label_PWD, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->comfig_label_PWD, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->comfig_label_PWD, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->comfig_label_PWD, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes comfig_label_mqtt_host
	ui->comfig_label_mqtt_host = lv_label_create(ui->comfig);
	lv_label_set_text(ui->comfig_label_mqtt_host, "host:wx.ai-thinker.com");
	lv_label_set_long_mode(ui->comfig_label_mqtt_host, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->comfig_label_mqtt_host, 6, 96);
	lv_obj_set_size(ui->comfig_label_mqtt_host, 210, 13);

	//Write style for comfig_label_mqtt_host, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->comfig_label_mqtt_host, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->comfig_label_mqtt_host, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->comfig_label_mqtt_host, lv_color_hex(0x068600), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->comfig_label_mqtt_host, &lv_font_MiSans_Medium_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->comfig_label_mqtt_host, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->comfig_label_mqtt_host, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->comfig_label_mqtt_host, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->comfig_label_mqtt_host, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->comfig_label_mqtt_host, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->comfig_label_mqtt_host, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->comfig_label_mqtt_host, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->comfig_label_mqtt_host, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->comfig_label_mqtt_host, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes comfig_label_MQTT_msg
	ui->comfig_label_MQTT_msg = lv_label_create(ui->comfig);
	lv_label_set_text(ui->comfig_label_MQTT_msg, "MQTT:");
	lv_label_set_long_mode(ui->comfig_label_MQTT_msg, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->comfig_label_MQTT_msg, 0, 78);
	lv_obj_set_size(ui->comfig_label_MQTT_msg, 56, 18);

	//Write style for comfig_label_MQTT_msg, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->comfig_label_MQTT_msg, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->comfig_label_MQTT_msg, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->comfig_label_MQTT_msg, lv_color_hex(0x9b9b9b), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->comfig_label_MQTT_msg, &lv_font_MiSans_Medium_14, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->comfig_label_MQTT_msg, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->comfig_label_MQTT_msg, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->comfig_label_MQTT_msg, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->comfig_label_MQTT_msg, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->comfig_label_MQTT_msg, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->comfig_label_MQTT_msg, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->comfig_label_MQTT_msg, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->comfig_label_MQTT_msg, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->comfig_label_MQTT_msg, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes comfig_label_port
	ui->comfig_label_port = lv_label_create(ui->comfig);
	lv_label_set_text(ui->comfig_label_port, "port:1883");
	lv_label_set_long_mode(ui->comfig_label_port, LV_LABEL_LONG_WRAP);
	lv_obj_set_pos(ui->comfig_label_port, 6, 115);
	lv_obj_set_size(ui->comfig_label_port, 210, 13);

	//Write style for comfig_label_port, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->comfig_label_port, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->comfig_label_port, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->comfig_label_port, lv_color_hex(0x068600), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->comfig_label_port, &lv_font_MiSans_Medium_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->comfig_label_port, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->comfig_label_port, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->comfig_label_port, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->comfig_label_port, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->comfig_label_port, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->comfig_label_port, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->comfig_label_port, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->comfig_label_port, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->comfig_label_port, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes comfig_img_ha
	ui->comfig_img_ha = lv_img_create(ui->comfig);
	lv_obj_add_flag(ui->comfig_img_ha, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->comfig_img_ha, &_homeassistant_alpha_20x20);
	lv_img_set_pivot(ui->comfig_img_ha, 50,50);
	lv_img_set_angle(ui->comfig_img_ha, 0);
	lv_obj_set_pos(ui->comfig_img_ha, 56, 72);
	lv_obj_set_size(ui->comfig_img_ha, 20, 20);

	//Write style for comfig_img_ha, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_opa(ui->comfig_img_ha, 255, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes comfig_img_wxconfig
	ui->comfig_img_wxconfig = lv_img_create(ui->comfig);
	lv_obj_add_flag(ui->comfig_img_wxconfig, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->comfig_img_wxconfig, &_code_alpha_130x130);
	lv_img_set_pivot(ui->comfig_img_wxconfig, 50,50);
	lv_img_set_angle(ui->comfig_img_wxconfig, 0);
	lv_obj_set_pos(ui->comfig_img_wxconfig, 179, 10);
	lv_obj_set_size(ui->comfig_img_wxconfig, 130, 130);

	//Write style for comfig_img_wxconfig, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_img_opa(ui->comfig_img_wxconfig, 255, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes comfig_btn_config
	ui->comfig_btn_config = lv_btn_create(ui->comfig);
	ui->comfig_btn_config_label = lv_label_create(ui->comfig_btn_config);
	lv_label_set_text(ui->comfig_btn_config_label, "Click here start BluFi config network");
	lv_label_set_long_mode(ui->comfig_btn_config_label, LV_LABEL_LONG_WRAP);
	lv_obj_align(ui->comfig_btn_config_label, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_style_pad_all(ui->comfig_btn_config, 0, LV_STATE_DEFAULT);
	lv_obj_set_pos(ui->comfig_btn_config, 7, 145);
	lv_obj_set_size(ui->comfig_btn_config, 306, 19);

	//Write style for comfig_btn_config, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->comfig_btn_config, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->comfig_btn_config, lv_color_hex(0x080808), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->comfig_btn_config, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->comfig_btn_config, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->comfig_btn_config, lv_color_hex(0x1c1c1c), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->comfig_btn_config, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->comfig_btn_config, 5, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->comfig_btn_config, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->comfig_btn_config, lv_color_hex(0x9b9b9b), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->comfig_btn_config, &lv_font_MiSans_Medium_12, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->comfig_btn_config, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Update current screen layout.
	lv_obj_update_layout(ui->comfig);

	
	//Init events for screen.
	events_init_comfig(ui);
}
