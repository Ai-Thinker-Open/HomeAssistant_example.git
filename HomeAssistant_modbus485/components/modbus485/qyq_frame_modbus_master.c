#define __QYQ_FRAME_MODBUS_MASTER_C_
#include "qyq_frame_modbus_master.h"

uint16_t qyq_frame_modbus_master_calculatecrc(uint8_t* data, uint16_t length)
{
    const uint16_t POLYNOMIAL = 0xA001;
    uint16_t crc = 0xFFFF;

    for (uint16_t i = 0; i < length; ++i) {
        crc ^= data[i];
        for (int j = 0; j < 8; ++j) {
            if (crc & 0x0001) {
                crc = (crc >> 1) ^ POLYNOMIAL;
            }
            else {
                crc >>= 1;
            }
        }
    }

    // 将高低字节互换
    crc = (crc << 8) | (crc >> 8);

    return crc;
}

// 主要为modbus提供一个基准的时钟
static int8_t qyq_frame_modbus_master_tick(struct qyq_frame_modbus_master_type* qyq_frame_modbus_master)
{
    if (qyq_frame_modbus_master == NULL) {
        return -1;
    }
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count++;
    return 0;
}

// 数据接收
static int8_t qyq_frame_modbus_master_recv(struct qyq_frame_modbus_master_type* qyq_frame_modbus_master, uint8_t dat) // modbus数据接收
{
    if (qyq_frame_modbus_master == NULL) {
        return -1;
    }

    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - 1;
    if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index >= qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf_size) {
        qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
        qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;
        // 表示数据满了，返回状态信息
        return -2;
    }
    // 更新时间和数据
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index++] = dat;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 1;

    return 0;
}

static int8_t qyq_frame_modbus_master_read_coils(struct qyq_frame_modbus_master_type* qyq_frame_modbus_master, uint8_t slave_addr, uint16_t start_addr, uint16_t coils_num, uint8_t* read_buf, uint32_t* length)
{
    uint16_t modbus_crc = 0;
    uint32_t rev_size = 0;
    uint32_t cnt = 0;
    if (qyq_frame_modbus_master == NULL) {
        return -1;
    }
    if (read_buf == NULL) {
        return -1;
    }
    if (length == NULL) {
        return -1;
    }

    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;

    while (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states != QYQ_FRAME_MODBUS_MASTER_UNKNOWN) {
        switch (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states) {
            // 等待应答
            case QYQ_FRAME_MODBUS_MASTER_WAIT_ACK:
                if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_RESPONSE_TIMEOUT_TIME) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                }
                else {
                    if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status == 1) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_READ_END_TIME) {
                        rev_size = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index;
                        modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf, rev_size - 2);
                        if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 2] == ((modbus_crc >> 8) & 0x00FF)) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 1] == (modbus_crc & 0x00FF))) {
                            if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[0] == slave_addr) {
                                // 返回成功
                                if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[1] == (QYQ_FRAME_MODBUS_MASTER_READ_COILS | 0x80)) {
                                    read_buf[0] = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[2];
                                    *length = 1;
                                    // 返回异常码
                                    return 1;
                                }
                                else {
                                    for (cnt = 0; cnt < rev_size - 5; cnt++) {
                                        read_buf[cnt] = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[cnt + 3];
                                    }
                                    *length = rev_size - 5;
                                    if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[2] != *length) {
                                        qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                                    }
                                    else {
                                        return 0;
                                    }
                                }
                            }
                            else {
                                // 返回失败
                                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                            }
                        }
                        else {
                            // 返回失败
                            qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                        }
                    }
                }
                break;

            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK:
                // 数据打包处理
                // 从机地址与功能码
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[0] = slave_addr;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[1] = QYQ_FRAME_MODBUS_MASTER_READ_COILS;

                // 线圈地址
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[2] = (start_addr >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[3] = start_addr & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[4] = (coils_num >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[5] = coils_num & 0x00FF;

                modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 6);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[6] = (modbus_crc >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[7] = modbus_crc & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WAIT_ACK;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_config_write(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 8);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
                break;
            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED:
                if (QYQ_FRAME_MODBUS_MASTER_WRITE_CNT_SIZE <= qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                    return -2;
                }
                else {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;

                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt++;
                }
                break;
            case QYQ_FRAME_MODBUS_MASTER_END:
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                break;
            default:
                break;
        }
    }

    return 0;
}

int8_t qyq_frame_modbus_master_read_discrete_inputs(struct qyq_frame_modbus_master_type* qyq_frame_modbus_master, uint8_t slave_addr, uint16_t start_addr, uint16_t coils_num, uint8_t* read_buf, uint32_t* length)
{
    uint16_t modbus_crc = 0;
    uint32_t rev_size = 0;
    uint32_t cnt = 0;
    if (qyq_frame_modbus_master == NULL) {
        return -1;
    }
    if (read_buf == NULL) {
        return -1;
    }
    if (length == NULL) {
        return -1;
    }

    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;

    while (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states != QYQ_FRAME_MODBUS_MASTER_UNKNOWN) {
        switch (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states) {
            // 等待应答
            case QYQ_FRAME_MODBUS_MASTER_WAIT_ACK:
                if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_RESPONSE_TIMEOUT_TIME) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                }
                else {
                    if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status == 1) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_READ_END_TIME) {
                        rev_size = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index;
                        modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf, rev_size - 2);
                        if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 2] == ((modbus_crc >> 8) & 0x00FF)) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 1] == (modbus_crc & 0x00FF))) {
                            if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[0] == slave_addr) {
                                // 返回成功
                                if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[1] == (QYQ_FRAME_MODBUS_MASTER_READ_DISCRETE_INPUTS | 0x80)) {
                                    read_buf[0] = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[2];
                                    *length = 1;
                                    // 返回异常码
                                    return 1;
                                }
                                else {
                                    for (cnt = 0; cnt < rev_size - 5; cnt++) {
                                        read_buf[cnt] = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[cnt + 3];
                                    }
                                    *length = rev_size - 5;
                                    if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[2] != *length) {
                                        qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                                    }
                                    else {
                                        return 0;
                                    }
                                }
                            }
                            else {
                                // 返回失败
                                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                            }
                        }
                        else {
                            // 返回失败
                            qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                        }
                    }
                }
                break;

            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK:
                // 数据打包处理
                // 从机地址与功能码
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[0] = slave_addr;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[1] = QYQ_FRAME_MODBUS_MASTER_READ_DISCRETE_INPUTS;

                // 线圈地址
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[2] = (start_addr >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[3] = start_addr & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[4] = (coils_num >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[5] = coils_num & 0x00FF;

                modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 6);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[6] = (modbus_crc >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[7] = modbus_crc & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WAIT_ACK;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_config_write(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 8);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
                break;
            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED:
                if (QYQ_FRAME_MODBUS_MASTER_WRITE_CNT_SIZE <= qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                    return -2;
                }
                else {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;

                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt++;
                }
                break;
            case QYQ_FRAME_MODBUS_MASTER_END:
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                break;
            default:
                break;
        }
    }

    return 0;
}

int8_t qyq_frame_modbus_master_read_holding_registers(struct qyq_frame_modbus_master_type* qyq_frame_modbus_master, uint8_t slave_addr, uint16_t start_addr, uint16_t coils_num, uint8_t* read_buf, uint32_t* length)
{
    uint16_t modbus_crc = 0;
    uint32_t rev_size = 0;
    uint32_t cnt = 0;
    if (qyq_frame_modbus_master == NULL) {
        return -1;
    }
    if (read_buf == NULL) {
        return -1;
    }
    if (length == NULL) {
        return -1;
    }

    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;

    while (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states != QYQ_FRAME_MODBUS_MASTER_UNKNOWN) {
        switch (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states) {
            // 等待应答
            case QYQ_FRAME_MODBUS_MASTER_WAIT_ACK:
                if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_RESPONSE_TIMEOUT_TIME) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                }
                else {
                    if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status == 1) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_READ_END_TIME) {
                        rev_size = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index;
                        modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf, rev_size - 2);
                        if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 2] == ((modbus_crc >> 8) & 0x00FF)) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 1] == (modbus_crc & 0x00FF))) {
                            if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[0] == slave_addr) {
                                // 返回成功
                                if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[1] == (QYQ_FRAME_MODBUS_MASTER_READ_HOLDING_REGISTERS | 0x80)) {
                                    read_buf[0] = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[2];
                                    *length = 1;
                                    // 返回异常码
                                    return 1;
                                }
                                else {
                                    for (cnt = 0; cnt < rev_size - 5; cnt++) {
                                        read_buf[cnt] = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[cnt + 3];
                                    }
                                    *length = rev_size - 5;
                                    if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[2] != *length) {
                                        qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                                    }
                                    else {
                                        return 0;
                                    }
                                }
                            }
                            else {
                                // 返回失败
                                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                            }
                        }
                        else {
                            // 返回失败
                            qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                        }
                    }
                }
                break;

            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK:
                // 数据打包处理
                // 从机地址与功能码
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[0] = slave_addr;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[1] = QYQ_FRAME_MODBUS_MASTER_READ_HOLDING_REGISTERS;

                // 线圈地址
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[2] = (start_addr >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[3] = start_addr & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[4] = (coils_num >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[5] = coils_num & 0x00FF;

                modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 6);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[6] = (modbus_crc >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[7] = modbus_crc & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WAIT_ACK;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_config_write(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 8);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
                break;
            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED:
                if (QYQ_FRAME_MODBUS_MASTER_WRITE_CNT_SIZE <= qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                    return -2;
                }
                else {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;

                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt++;
                }
                break;
            case QYQ_FRAME_MODBUS_MASTER_END:
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                break;
            default:
                break;
        }
    }

    return 0;
}

int8_t qyq_frame_modbus_master_read_input_registers(struct qyq_frame_modbus_master_type* qyq_frame_modbus_master, uint8_t slave_addr, uint16_t start_addr, uint16_t coils_num, uint8_t* read_buf, uint32_t* length)
{
    uint16_t modbus_crc = 0;
    uint32_t rev_size = 0;
    uint32_t cnt = 0;
    if (qyq_frame_modbus_master == NULL) {
        return -1;
    }
    if (read_buf == NULL) {
        return -1;
    }
    if (length == NULL) {
        return -1;
    }

    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;

    while (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states != QYQ_FRAME_MODBUS_MASTER_UNKNOWN) {
        switch (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states) {
            // 等待应答
            case QYQ_FRAME_MODBUS_MASTER_WAIT_ACK:
                if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_RESPONSE_TIMEOUT_TIME) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                }
                else {
                    if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status == 1) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_READ_END_TIME) {
                        rev_size = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index;
                        modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf, rev_size - 2);
                        if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 2] == ((modbus_crc >> 8) & 0x00FF)) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 1] == (modbus_crc & 0x00FF))) {
                            if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[0] == slave_addr) {
                                // 返回成功
                                if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[1] == (QYQ_FRAME_MODBUS_MASTER_READ_INPUT_REGISTERS | 0x80)) {
                                    read_buf[0] = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[2];
                                    *length = 1;
                                    // 返回异常码
                                    return 1;
                                }
                                else {
                                    for (cnt = 0; cnt < rev_size - 5; cnt++) {
                                        read_buf[cnt] = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[cnt + 3];
                                    }
                                    *length = rev_size - 5;
                                    if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[2] != *length) {
                                        qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                                    }
                                    else {
                                        return 0;
                                    }
                                }
                            }
                            else {
                                // 返回失败
                                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                            }
                        }
                        else {
                            // 返回失败
                            qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                        }
                    }
                }
                break;

            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK:
                // 数据打包处理
                // 从机地址与功能码
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[0] = slave_addr;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[1] = QYQ_FRAME_MODBUS_MASTER_READ_INPUT_REGISTERS;

                // 线圈地址
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[2] = (start_addr >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[3] = start_addr & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[4] = (coils_num >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[5] = coils_num & 0x00FF;

                modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 6);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[6] = (modbus_crc >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[7] = modbus_crc & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WAIT_ACK;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_config_write(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 8);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
                break;
            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED:
                if (QYQ_FRAME_MODBUS_MASTER_WRITE_CNT_SIZE <= qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                    return -2;
                }
                else {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;

                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt++;
                }
                break;
            case QYQ_FRAME_MODBUS_MASTER_END:
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                break;
            default:
                break;
        }
    }

    return 0;
}

static int8_t qyq_frame_modbus_master_write_single_coil(struct qyq_frame_modbus_master_type* qyq_frame_modbus_master, uint8_t slave_addr, uint16_t start_addr, uint16_t value)
{
    uint16_t modbus_crc = 0;
    uint32_t rev_size = 0;
    uint32_t cnt = 0;
    if (qyq_frame_modbus_master == NULL) {
        return -1;
    }

    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;

    while (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states != QYQ_FRAME_MODBUS_MASTER_UNKNOWN) {
        switch (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states) {
            // 等待应答
            case QYQ_FRAME_MODBUS_MASTER_WAIT_ACK:
                if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_RESPONSE_TIMEOUT_TIME) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                }
                else {
                    if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status == 1) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_READ_END_TIME) {
                        rev_size = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index;
                        modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf, rev_size - 2);
                        if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 2] == ((modbus_crc >> 8) & 0x00FF)) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 1] == (modbus_crc & 0x00FF))) {
                            if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[0] == slave_addr) {
                                // 返回成功
                                if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[1] == (QYQ_FRAME_MODBUS_MASTER_WRITE_SINGLE_COIL | 0x80)) {
                                    // 返回异常码
                                    return qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[2];
                                }
                                else {
                                    for (cnt = 0; cnt < 8; cnt++) {
                                        if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[cnt] != qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[cnt]) {
                                            return -3;
                                        }
                                    }
                                    return 0;
                                }
                            }
                            else {
                                // 返回失败
                                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                            }
                        }
                        else {
                            // 返回失败
                            qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                        }
                    }
                }
                break;

            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK:
                // 数据打包处理
                // 从机地址与功能码
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[0] = slave_addr;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[1] = QYQ_FRAME_MODBUS_MASTER_WRITE_SINGLE_COIL;

                // 线圈地址
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[2] = (start_addr >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[3] = start_addr & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[4] = (value >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[5] = value & 0x00FF;

                modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 6);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[6] = (modbus_crc >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[7] = modbus_crc & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WAIT_ACK;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_config_write(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 8);
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
                break;
            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED:
                if (QYQ_FRAME_MODBUS_MASTER_WRITE_CNT_SIZE <= qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                    return -2;
                }
                else {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;

                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt++;
                }
                break;
            case QYQ_FRAME_MODBUS_MASTER_END:
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                break;
            default:
                break;
        }
    }

    return 0;
}

static int8_t qyq_frame_modbus_master_write_single_register(struct qyq_frame_modbus_master_type* qyq_frame_modbus_master, uint8_t slave_addr, uint16_t start_addr, uint16_t value)
{
    uint16_t modbus_crc = 0;
    uint32_t rev_size = 0;
    uint32_t cnt = 0;
    if (qyq_frame_modbus_master == NULL) {
        return -1;
    }

    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;

    while (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states != QYQ_FRAME_MODBUS_MASTER_UNKNOWN) {
        switch (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states) {
            // 等待应答
            case QYQ_FRAME_MODBUS_MASTER_WAIT_ACK:
                if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_RESPONSE_TIMEOUT_TIME) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                }
                else {
                    if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status == 1) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_READ_END_TIME) {
                        rev_size = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index;
                        modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf, rev_size - 2);
                        if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 2] == ((modbus_crc >> 8) & 0x00FF)) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 1] == (modbus_crc & 0x00FF))) {
                            if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[0] == slave_addr) {
                                // 返回成功
                                if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[1] == (QYQ_FRAME_MODBUS_MASTER_WRITE_SINGLE_REGISTER | 0x80)) {
                                    // 返回异常码
                                    return qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[2];
                                }
                                else {
                                    for (cnt = 0; cnt < 8; cnt++) {
                                        if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[cnt] != qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[cnt]) {
                                            return -3;
                                        }
                                    }
                                    return 0;
                                }
                            }
                            else {
                                // 返回失败
                                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                            }
                        }
                        else {
                            // 返回失败
                            qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                        }
                    }
                }
                break;

            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK:
                // 数据打包处理
                // 从机地址与功能码
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[0] = slave_addr;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[1] = QYQ_FRAME_MODBUS_MASTER_WRITE_SINGLE_REGISTER;

                // 线圈地址
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[2] = (start_addr >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[3] = start_addr & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[4] = (value >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[5] = value & 0x00FF;

                modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 6);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[6] = (modbus_crc >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[7] = modbus_crc & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WAIT_ACK;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_config_write(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 8);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
                break;
            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED:
                if (QYQ_FRAME_MODBUS_MASTER_WRITE_CNT_SIZE <= qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                    return -2;
                }
                else {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;

                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt++;
                }
                break;
            case QYQ_FRAME_MODBUS_MASTER_END:
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                break;
            default:
                break;
        }
    }

    return 0;
}

static int8_t qyq_frame_modbus_master_write_multiple_coil(struct qyq_frame_modbus_master_type* qyq_frame_modbus_master, uint8_t slave_addr, uint16_t start_addr, uint16_t coils_num, uint8_t* buf, uint8_t length)
{
    uint16_t modbus_crc = 0;
    uint32_t rev_size = 0;
    uint32_t cnt = 0;
    if (qyq_frame_modbus_master == NULL) {
        return -1;
    }

    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;

    while (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states != QYQ_FRAME_MODBUS_MASTER_UNKNOWN) {
        switch (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states) {
            // 等待应答
            case QYQ_FRAME_MODBUS_MASTER_WAIT_ACK:
                if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_RESPONSE_TIMEOUT_TIME) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                }
                else {
                    if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status == 1) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_READ_END_TIME) {
                        rev_size = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index;
                        modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf, rev_size - 2);
                        if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 2] == ((modbus_crc >> 8) & 0x00FF)) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 1] == (modbus_crc & 0x00FF))) {
                            if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[0] == slave_addr) {
                                // 返回成功
                                if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[1] == (QYQ_FRAME_MODBUS_MASTER_WRITE_MULTIPLE_COILS | 0x80)) {
                                    // 返回异常码
                                    return qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[2];
                                }
                                else {
                                    for (cnt = 0; cnt < 6; cnt++) {
                                        if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[cnt] != qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[cnt]) {
                                            return -3;
                                        }
                                    }
                                    return 0;
                                }
                            }
                            else {
                                // 返回失败
                                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                            }
                        }
                        else {
                            // 返回失败
                            qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                        }
                    }
                }
                break;

            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK:
                // 数据打包处理
                // 从机地址与功能码
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[0] = slave_addr;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[1] = QYQ_FRAME_MODBUS_MASTER_WRITE_MULTIPLE_COILS;

                // 线圈地址
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[2] = (start_addr >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[3] = start_addr & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[4] = (coils_num >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[5] = coils_num & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[6] = length;

                for (cnt = 0; cnt < length; cnt++) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[7 + cnt] = buf[cnt];
                }

                modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 7 + length);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[7 + length] = (modbus_crc >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[8 + length] = modbus_crc & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WAIT_ACK;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_config_write(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 9 + length);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
                break;
            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED:
                if (QYQ_FRAME_MODBUS_MASTER_WRITE_CNT_SIZE <= qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                    return -2;
                }
                else {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;

                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt++;
                }
                break;
            case QYQ_FRAME_MODBUS_MASTER_END:
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                break;
            default:
                break;
        }
    }

    return 0;
}

static int8_t qyq_frame_modbus_master_write_multiple_registers(struct qyq_frame_modbus_master_type* qyq_frame_modbus_master, uint8_t slave_addr, uint16_t start_addr, uint16_t coils_num, uint8_t* buf, uint8_t length)
{
    uint16_t modbus_crc = 0;
    uint32_t rev_size = 0;
    uint32_t cnt = 0;
    if (qyq_frame_modbus_master == NULL) {
        return -1;
    }

    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;

    while (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states != QYQ_FRAME_MODBUS_MASTER_UNKNOWN) {
        switch (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states) {
            // 等待应答
            case QYQ_FRAME_MODBUS_MASTER_WAIT_ACK:
                if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_RESPONSE_TIMEOUT_TIME) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                }
                else {
                    if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status == 1) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count - qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer) > QYQ_FRAME_MODBUS_MASTER_READ_END_TIME) {
                        rev_size = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index;
                        modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf, rev_size - 2);
                        if ((qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 2] == ((modbus_crc >> 8) & 0x00FF)) && (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[rev_size - 1] == (modbus_crc & 0x00FF))) {
                            if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[0] == slave_addr) {
                                // 返回成功
                                if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[1] == (QYQ_FRAME_MODBUS_MASTER_WRITE_MULTIPLE_REGISTERS | 0x80)) {
                                    // 返回异常码
                                    return qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[2];
                                }
                                else {
                                    for (cnt = 0; cnt < 6; cnt++) {
                                        if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf[cnt] != qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[cnt]) {
                                            return -3;
                                        }
                                    }
                                    return 0;
                                }
                            }
                            else {
                                // 返回失败
                                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                            }
                        }
                        else {
                            // 返回失败
                            qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED;
                        }
                    }
                }
                break;

            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK:
                // 数据打包处理
                // 从机地址与功能码
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[0] = slave_addr;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[1] = QYQ_FRAME_MODBUS_MASTER_WRITE_MULTIPLE_REGISTERS;

                // 线圈地址
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[2] = (start_addr >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[3] = start_addr & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[4] = (coils_num >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[5] = coils_num & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[6] = length;

                for (cnt = 0; cnt < length; cnt++) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[7 + cnt] = buf[cnt];
                }

                modbus_crc = qyq_frame_modbus_master_calculatecrc(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 7 + length);

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[7 + length] = (modbus_crc >> 8) & 0x00FF;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf[8 + length] = modbus_crc & 0x00FF;

                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WAIT_ACK;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_index = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readready_status = 0;
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_config_write(qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf, 9 + length);
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_read_bench_timer = qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_tick_count;
                break;
            case QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK_FAILED:
                if (QYQ_FRAME_MODBUS_MASTER_WRITE_CNT_SIZE <= qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt) {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                    return -2;
                }
                else {
                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_WRITE_BLOCK;

                    qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_write_cnt++;
                }
                break;
            case QYQ_FRAME_MODBUS_MASTER_END:
                qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_states = QYQ_FRAME_MODBUS_MASTER_UNKNOWN;
                break;
            default:
                break;
        }
    }

    return 0;
}

static int8_t qyq_frame_modbus_master_init(struct qyq_frame_modbus_master_type* qyq_frame_modbus_master)
{
    if (qyq_frame_modbus_master == NULL) {
        return -1;
    }

    if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_readbuf == NULL) {
        return -1;
    }

    if (qyq_frame_modbus_master->qyq_frame_modbus_master_config->qyq_frame_modbus_master_writebuf == NULL) {
        return -1;
    }

    return 0;
}

int8_t qyq_frame_modbus_master_create(qyq_frame_modbus_master_type_t* qyq_frame_modbus_master, qyq_frame_modbus_master_config_t* qyq_frame_modbus_master_config)
{
    if (qyq_frame_modbus_master == NULL) {
        return -1;
    }

    if (qyq_frame_modbus_master_config == NULL) {
        return -1;
    }

    qyq_frame_modbus_master->qyq_frame_modbus_master_config = qyq_frame_modbus_master_config;

    qyq_frame_modbus_master->qyq_frame_modbus_master_init = qyq_frame_modbus_master_init;
    qyq_frame_modbus_master->qyq_frame_modbus_master_tick = qyq_frame_modbus_master_tick;
    qyq_frame_modbus_master->qyq_frame_modbus_master_recv = qyq_frame_modbus_master_recv;
    qyq_frame_modbus_master->qyq_frame_modbus_master_read_coils = qyq_frame_modbus_master_read_coils;
    qyq_frame_modbus_master->qyq_frame_modbus_master_read_discrete_inputs = qyq_frame_modbus_master_read_discrete_inputs;
    qyq_frame_modbus_master->qyq_frame_modbus_master_read_holding_registers = qyq_frame_modbus_master_read_holding_registers;
    qyq_frame_modbus_master->qyq_frame_modbus_master_read_input_registers = qyq_frame_modbus_master_read_input_registers;

    qyq_frame_modbus_master->qyq_frame_modbus_master_write_single_coil = qyq_frame_modbus_master_write_single_coil;
    qyq_frame_modbus_master->qyq_frame_modbus_master_write_single_register = qyq_frame_modbus_master_write_single_register;
    qyq_frame_modbus_master->qyq_frame_modbus_master_write_multiple_coil = qyq_frame_modbus_master_write_multiple_coil;
    qyq_frame_modbus_master->qyq_frame_modbus_master_write_multiple_registers = qyq_frame_modbus_master_write_multiple_registers;
    return 0;
}
