/**
 * @file main.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-08-22
 *
 * @copyright Copyright (c) 2023
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <FreeRTOS.h>
#include <task.h>
#include "board.h"
#include "log.h"

#include "lv_user_config.h"
#include "gui_guider.h"
#include "custom.h"
#include "events_init.h"
#include "lv_port_fs.h"
#include "device_ctrl.h"
#include "wifi_event.h"

#include "bluficonfig.h"

#include "bflb_gpio.h"
#include "demo_gui_guider.h"
#include "homeAssistantPort.h"

#define DBG_TAG "MAIN"
extern bool config_scr_del;
extern bool main_scr_del;
extern bool ws2812_scr_del;
static struct bflb_device_s* gpio;
static homeAssisatnt_device_t ha_device;
bool mqtt_connect_status = false;
lv_ui guider_ui;
lv_demo_ui demo_guider_ui;

/* HomeAssistant 事件回调 */
static  ha_event_cb(ha_event_t event, homeAssisatnt_device_t* ha_dev)
{
    switch (event)
    {
        /*  连接服务器事件  */
        case HA_EVENT_MQTT_CONNECED:
            HA_LOG_I("<<<<<<<<<<  HA_EVENT_MQTT_CONNECED\r\n");
            mqtt_connect_status = true;

            static ha_lh_entity_t rgb_light = {
                .name = "WS2812灯板",
                .unique_id = "all_rgb",
                .rgb.rgb_command_topic = "m61/rbg/set",
                .rgb.rgb_state_topic = "m61/rgb/state",
                .command_topic = "r286/set",
                .state_topic = "r286/state",
            };
            static ha_sw_entity_t sw_mode_1 = {
                .name = "模式1",
                .unique_id = "sw_mode_1",
                .icon = "mdi:auto-mode",
            };

            static ha_sw_entity_t sw_mode_2 = {
                .name = "模式2",
                .unique_id = "sw_mode_2",
                .icon = "mdi:auto-mode",
            };
            static ha_sw_entity_t sw_mode_3 = {
                .name = "模式3",
                .unique_id = "sw_mode_3",
                .icon = "mdi:auto-mode",
            };

            homeAssistant_device_add_entity(CONFIG_HA_ENTITY_LIGHT, &rgb_light);

            homeAssistant_device_add_entity(CONFIG_HA_ENTITY_SWITCH, &sw_mode_1);

            homeAssistant_device_add_entity(CONFIG_HA_ENTITY_SWITCH, &sw_mode_2);

            homeAssistant_device_add_entity(CONFIG_HA_ENTITY_SWITCH, &sw_mode_3);

            homeAssistant_device_send_status(HOMEASSISTANT_STATUS_ONLINE);

            homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, &sw_mode_1, 0);
            homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, &sw_mode_2, 0);
            homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, &sw_mode_3, 0);
            homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, &rgb_light, 0);
            if (!config_scr_del) {
                lv_indev_wait_release(lv_indev_get_act());
                ui_load_scr_animation(&guider_ui, &guider_ui.main, guider_ui.main_del, &guider_ui.config_del, setup_scr_main, LV_SCR_LOAD_ANIM_OVER_LEFT, 150, 150, false, true);
                config_scr_del = true;
                main_scr_del = false;
            }

            lv_label_set_text(guider_ui.main_label_4, "WiFi OK");
            lv_obj_clear_flag(guider_ui.main_img_wifi, LV_OBJ_FLAG_HIDDEN);
            lv_obj_add_flag(guider_ui.main_img_nowifi, LV_OBJ_FLAG_HIDDEN);

            homeAssistant_mqtt_port_subscribe("LoRa_Borad/b4c2e041a095/temp_lora-41a0/state", 0);
            homeAssistant_mqtt_port_subscribe("LoRa_Borad/b4c2e041a095/humi_lora-41a0/state", 0);
            break;
            /*  服务器断开事件  */
        case HA_EVENT_MQTT_DISCONNECT:
            HA_LOG_I("<<<<<<<<<<  HA_EVENT_MQTT_DISCONNECT\r\n");
            mqtt_connect_status = false;
            if (!config_scr_del) {
                lv_indev_wait_release(lv_indev_get_act());
                ui_load_scr_animation(&guider_ui, &guider_ui.main, guider_ui.main_del, &guider_ui.config_del, setup_scr_main, LV_SCR_LOAD_ANIM_OVER_LEFT, 150, 150, false, true);
                config_scr_del = true;
                main_scr_del = false;
            }

            lv_label_set_text(guider_ui.main_label_4, "No Net");
            lv_obj_clear_flag(guider_ui.main_img_nowifi, LV_OBJ_FLAG_HIDDEN);
            lv_obj_add_flag(guider_ui.main_img_wifi, LV_OBJ_FLAG_HIDDEN);
            break;
        case HA_EVENT_MQTT_COMMAND_SWITCH:
        {
            // ha_sw_entity_t* switch_type = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "sw_mode_1");
            ha_sw_entity_t* mode = NULL;
            if (ha_dev->entity_switch->command_switch->unique_id=="sw_mode_1")
                mode = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "sw_mode_1");
            else if (ha_dev->entity_switch->command_switch->unique_id=="sw_mode_2")
            {
                mode = NULL;
                mode = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "sw_mode_2");
            }
            else if (ha_dev->entity_switch->command_switch->unique_id=="sw_mode_3")
            {
                mode = NULL;
                mode = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "sw_mode_3");
            }
            LOG_I("%s state=%d", mode->unique_id, mode->switch_state);
            mode->switch_state = ha_dev->entity_switch->command_switch->switch_state;
            homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, ha_dev->entity_switch->command_switch, ha_dev->entity_switch->command_switch->switch_state);
        }
        break;
        case HA_EVENT_MQTT_ERROR:
            HA_LOG_E("<<<<<<<<<<  HA_EVENT_MQTT_ERROR\r\n");
            break;
        case HA_EVENT_MQTT_COMMAND_LIGHT_SWITCH:
        {
            homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, ha_dev->entity_light->command_light, ha_dev->entity_light->command_light->light_state);
        }
        break;
        case HA_EVENT_MQTT_COMMAND_LIGHT_RGB_UPDATE:
        {

        }
        break;
        case HA_EVENT_MQTT_CUSTOM_SUBSCRIBE_DATA:
            LOG_I("topic=%.*s,data=%.*s", ha_dev->custom_sub_data.custom_sub_topic_len, ha_dev->custom_sub_data.custom_sub_topic, ha_dev->custom_sub_data.custom_sub_data_len, ha_dev->custom_sub_data.custom_sub_data);
            if (strncmp(ha_dev->custom_sub_data.custom_sub_topic, "LoRa_Borad/b4c2e041a095/humi_lora-41a0/state", ha_dev->custom_sub_data.custom_sub_topic_len)==0)
            {
                char* humi_str = pvPortMalloc(2);
                memset(humi_str, 0, 2);
                strncpy(humi_str, ha_dev->custom_sub_data.custom_sub_data, ha_dev->custom_sub_data.custom_sub_data_len);
                __humi = atoi(humi_str);
                vPortFree(humi_str);
                device_send_state_notify(DEVICE_SATTE_SENSOR_DATA_TEMP, false);
            }

            if (strncmp(ha_dev->custom_sub_data.custom_sub_topic, "LoRa_Borad/b4c2e041a095/temp_lora-41a0/state", ha_dev->custom_sub_data.custom_sub_topic_len)==0)
            {
                char* temp_str = pvPortMalloc(2);
                memset(temp_str, 0, 2);
                strncpy(temp_str, ha_dev->custom_sub_data.custom_sub_data, ha_dev->custom_sub_data.custom_sub_data_len);
                temp = atoi(temp_str);
                vPortFree(temp_str);
                device_send_state_notify(DEVICE_SATTE_SENSOR_DATA_HUMI, false);
            }
            break;
        default:
            break;
    }
}

int main(void)
{
    board_init();
    if (0 != rfparam_init(0, NULL, 0)) {
        LOG_E("PHY RF init failed!\r\n");
        return 0;
    }
    gpio = bflb_device_get_by_name("gpio");
    bflb_gpio_init(gpio, 1, GPIO_INPUT | GPIO_PULLUP | GPIO_SMT_EN | GPIO_DRV_0);
    lv_init();
    lv_port_disp_init();
    lv_port_indev_init();
    xTaskCreate(lvgl_tick_task, (char*)"lvgl", 1024, NULL, 2, NULL);
    if (!bflb_gpio_read(gpio, 1)) {
        demo_setup_ui(&demo_guider_ui);
    }
    else {
        setup_ui(&guider_ui);
        staWiFiInit();
        homeassistant_blufi_init(NULL);
        xTaskCreate(device_state_task, "deivce", 1024*2, &guider_ui, 3, (TaskHandle_t*)&device_task);
        // ha_dev_mqtt_init(&homeAssistant_dev);
        /*   定义 device 变量     */
        static char mac[6];
        homeAssistant_get_sta_mac(mac);

        ha_device.name = "手提箱86盒";
        ha_device.model = "Ai-M61-32S";
        ha_device.manufacturer = "Ai-Thinker";
        ha_device.hw_version = "v1.0";
        ha_device.sw_version = "v1.0";
        if (ha_device.mqtt_info.mqtt_clientID==NULL)
            ha_device.mqtt_info.mqtt_clientID = pvPortMalloc(128);
        memset(ha_device.mqtt_info.mqtt_clientID, 0, 128);
        sprintf(ha_device.mqtt_info.mqtt_clientID, "Ai-M61_R286-%02X%02X%02X%02X%02X%2X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
        ha_device.mqtt_info.mqtt_username = "AiPi-Eyes-R2";
        ha_device.mqtt_info.mqtt_password = "12345678";
        ha_device.mqtt_info.mqtt_host = "192.168.32.2";
        ha_device.mqtt_info.port = 1883;
        ha_device.mqtt_info.mqtt_keeplive = 120;
        homeAssistant_device_init(&ha_device, ha_event_cb);
    }
    vTaskStartScheduler();
}
