/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/

#include "events_init.h"
#include "FreeRTOS.h"
#include "task.h"
#include <stdio.h>
#include "lvgl.h"
#include "log.h"
#include "device_ctrl.h"
#include "homeAssistantPort.h"
#if LV_USE_GUIDER_SIMULATOR && LV_USE_FREEMASTER
#include "freemaster_client.h"
#endif
extern bool config_scr_del;
extern bool main_scr_del;
extern bool ws2812_scr_del;
led_s_t leds[64];

static void main_event_handler(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    switch (code) {
        case LV_EVENT_GESTURE:
        {
            lv_dir_t dir = lv_indev_get_gesture_dir(lv_indev_get_act());
            switch (dir) {
                case LV_DIR_RIGHT:
                {
                    lv_indev_wait_release(lv_indev_get_act());
                    ui_load_scr_animation(&guider_ui, &guider_ui.config, guider_ui.config_del, &guider_ui.main_del, setup_scr_config, LV_SCR_LOAD_ANIM_OVER_RIGHT, 150, 150, false, true);
                    config_scr_del = false;
                    break;
                }
                case LV_DIR_LEFT:
                {
                    lv_indev_wait_release(lv_indev_get_act());
                    ui_load_scr_animation(&guider_ui, &guider_ui.ws2812, guider_ui.ws2812_del, &guider_ui.main_del, setup_scr_ws2812, LV_SCR_LOAD_ANIM_OVER_LEFT, 150, 150, false, true);
                    ws2812_scr_del = false;
                    break;
                }
                default:
                    break;
            }
            break;
            main_scr_del = true;
        }
        default:
            break;
    }
}

void events_init_main(lv_ui* ui)
{
    lv_obj_add_event_cb(ui->main, main_event_handler, LV_EVENT_ALL, ui);
}

static void config_event_handler(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    switch (code) {
        case LV_EVENT_GESTURE:
        {
            lv_dir_t dir = lv_indev_get_gesture_dir(lv_indev_get_act());
            switch (dir) {
                case LV_DIR_LEFT:
                {
                    lv_indev_wait_release(lv_indev_get_act());
                    ui_load_scr_animation(&guider_ui, &guider_ui.main, guider_ui.main_del, &guider_ui.config_del, setup_scr_main, LV_SCR_LOAD_ANIM_OVER_LEFT, 150, 150, false, true);
                    config_scr_del = true;
                    main_scr_del = false;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}
static void event_wifi_config_start(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_ui* ui = lv_event_get_user_data(e);
    switch (code) {
        case LV_EVENT_CLICKED:
        {
            LOG_I("WiFi config start");
            device_send_state_notify(DEVICE_STATE_BLUFI_CONFIG_START, false);
            lv_label_set_text(ui->config_btn_1_label, "WiFi Config ...");
        }
        default:
            break;
    }
}

void events_init_config(lv_ui* ui)
{
    lv_obj_add_event_cb(ui->config, config_event_handler, LV_EVENT_ALL, ui);
    lv_obj_add_event_cb(ui->config_btn_1, event_wifi_config_start, LV_EVENT_CLICKED, ui);
}

static void ws2812_event_handler(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    switch (code) {
        case LV_EVENT_GESTURE:
        {
            lv_dir_t dir = lv_indev_get_gesture_dir(lv_indev_get_act());
            switch (dir) {
                case LV_DIR_RIGHT:
                {
                    lv_indev_wait_release(lv_indev_get_act());
                    ui_load_scr_animation(&guider_ui, &guider_ui.main, guider_ui.main_del, &guider_ui.ws2812_del, setup_scr_main, LV_SCR_LOAD_ANIM_OVER_RIGHT, 150, 150, false, true);
                    ws2812_scr_del = true;
                    main_scr_del = false;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

static void ws2812_ws2812_btn_all_on_event_handler(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_ui* ui = lv_event_get_user_data(e);
    switch (code) {
        case LV_EVENT_CLICKED:
        {
            LOG_I("WiFi Light all ON");
            homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, homeAssistant_fine_entity(CONFIG_HA_ENTITY_LIGHT, "all_rgb"), 1);
        }
        default:
            break;
    }

}
static void ws2812_ws2812_btn_all_off_event_handler(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_ui* ui = lv_event_get_user_data(e);
    switch (code) {
        case LV_EVENT_CLICKED:
        {
            LOG_I("WiFi Light all OFF");
            homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, homeAssistant_fine_entity(CONFIG_HA_ENTITY_LIGHT, "all_rgb"), 0);
            for (size_t i = 0; i <sizeof(led_id_arry); i++)
            {
                ui->leds[i].id = led_id_arry[i];
                ui->leds[i].state = false;
                ui->leds[i].rgb_red = 0x00;
                ui->leds[i].rgb_green = 0x00;
                ui->leds[i].rgb_blue = 0xff;
                lv_btnmatrix_clear_btn_ctrl_all(ui->ws2812_btnm_1, LV_BTNMATRIX_CTRL_CHECKED);
                // lv_btnmatrix_set_btn_ctrl_all(ui->ws2812_btnm_1, LV_BTNMATRIX_CTRL_DISABLED);
            }
        }
        default:
            break;
    }

}
static void ws2812_ws2812_btn_mode1_event_handler(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_ui* ui = lv_event_get_user_data(e);
    switch (code) {
        case LV_EVENT_CLICKED:
        {
            LOG_I("WiFi Light mode1");
            ha_sw_entity_t* mode1 = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "sw_mode_1");
            if (mode1->switch_state)
                homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode1, 0);
            else
                homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode1, 1);
        }
        default:
            break;
    }

}
static void ws2812_ws2812_btn_mode2_event_handler(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_ui* ui = lv_event_get_user_data(e);
    switch (code) {
        case LV_EVENT_CLICKED:
        {
            LOG_I("WiFi Light mode2");
            ha_sw_entity_t* mode2 = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "sw_mode_2");
            if (mode2->switch_state)
                homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode2, 0);
            else
                homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode2, 1);
        }
        default:
            break;
    }

}
static void ws2812_ws2812_btn_mode3_event_handler(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_ui* ui = lv_event_get_user_data(e);
    switch (code) {
        case LV_EVENT_CLICKED:
        {
            LOG_I("WiFi Light mode3");
            ha_sw_entity_t* mode3 = homeAssistant_fine_entity(CONFIG_HA_ENTITY_SWITCH, "sw_mode_3");
            if (mode3->switch_state) {
                homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode3, 0);
            }

            else
                homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_SWITCH, mode3, 1);
        }
        default:
            break;
    }

}
extern bool mqtt_connect_status;
static void ws2812_btnm_event_handler(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* obj = lv_event_get_target(e);
    lv_ui* ui = lv_event_get_user_data(e);
    ha_lh_entity_t* light = homeAssistant_fine_entity(CONFIG_HA_ENTITY_LIGHT, "all_rgb");
    if (code == LV_EVENT_VALUE_CHANGED) {
        uint32_t id = lv_btnmatrix_get_selected_btn(obj);
        //判断是否已经连接
        if (mqtt_connect_status) {
            // if (!light->light_state)
            homeAssistant_device_send_entity_state(CONFIG_HA_ENTITY_LIGHT, light, 1);
            /* 判断ID 是否已经打开*/
            if (ui->leds[id].state) {
                ui->leds[id].state = false;
            }
            else {
                ui->leds[id].state = true;
            }
            char* led_str = pvPortMalloc(128);
            memset(led_str, 0, 128);
            sprintf(led_str, "{\"id\":%d,\"state\":%d,\"red\":0,\"greem\":0,\"blue\":%d}", ui->leds[id].id, ui->leds[id].state, ui->leds[id].rgb_blue);
            homeAssistant_mqtt_port_public("rgb/one/set", led_str, 0, 0);
            vPortFree(led_str);

        }
        LOG_I("%d was pressed\n", id);

    }
}
void events_init_ws2812(lv_ui* ui)
{
    lv_obj_add_event_cb(ui->ws2812, ws2812_event_handler, LV_EVENT_ALL, ui);
    lv_obj_add_event_cb(ui->ws2812_btn_1, ws2812_ws2812_btn_all_on_event_handler, LV_EVENT_CLICKED, ui);
    lv_obj_add_event_cb(ui->ws2812_btn_2, ws2812_ws2812_btn_all_off_event_handler, LV_EVENT_CLICKED, ui);
    lv_obj_add_event_cb(ui->ws2812_btn_3, ws2812_ws2812_btn_mode1_event_handler, LV_EVENT_CLICKED, ui);
    lv_obj_add_event_cb(ui->ws2812_btn_4, ws2812_ws2812_btn_mode2_event_handler, LV_EVENT_CLICKED, ui);
    lv_obj_add_event_cb(ui->ws2812_btn_5, ws2812_ws2812_btn_mode3_event_handler, LV_EVENT_CLICKED, ui);
    lv_obj_add_event_cb(ui->ws2812_btnm_1, ws2812_btnm_event_handler, LV_EVENT_ALL, ui);
    for (size_t i = 0; i <sizeof(led_id_arry); i++)
    {
        ui->leds[i].id = led_id_arry[i];
        ui->leds[i].state = false;
        ui->leds[i].rgb_red = 0x00;
        ui->leds[i].rgb_green = 0x00;
        ui->leds[i].rgb_blue = 0xff;
    }
}

void events_init(lv_ui* ui)
{

}
