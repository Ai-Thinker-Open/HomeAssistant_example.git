/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/

#include "lvgl.h"
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "widgets_init.h"
#include "custom.h"



void setup_scr_ws2812(lv_ui* ui)
{
	//Write codes ws2812
	ui->leds = leds;
	ui->ws2812 = lv_obj_create(NULL);
	lv_obj_set_size(ui->ws2812, 480, 480);
	lv_obj_set_scrollbar_mode(ui->ws2812, LV_SCROLLBAR_MODE_OFF);

	//Write style for ws2812, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->ws2812, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->ws2812, lv_color_hex(0x000000), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->ws2812, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes ws2812_btnm_1
	ui->ws2812_btnm_1 = lv_btnmatrix_create(ui->ws2812);
	static const char* ws2812_btnm_1_text_map[] = { " ", " ", " ", " ", " ", " ", " ", " ", "\n", " ", " ", " ", " ", " ", " ", " ", " ", "\n", " ", " ", " ", " ", " ", " ", " ", " ", "\n", " ", " ", " ", " ", " ", " ", " ", " ", "\n", " ", " ", " ", " ", " ", " ", " ", " ", "\n", " ", " ", " ", " ", " ", " ", " ", " ", "\n", " ", " ", " ", " ", " ", " ", " ", " ", "\n", "  ", " ", " ", " ", " ", " ", " ", " ", "", };
	lv_btnmatrix_set_map(ui->ws2812_btnm_1, ws2812_btnm_1_text_map);
	lv_btnmatrix_set_btn_ctrl_all(ui->ws2812_btnm_1, LV_BTNMATRIX_CTRL_CHECKABLE);
	lv_obj_set_pos(ui->ws2812_btnm_1, 11, 54);
	lv_obj_set_size(ui->ws2812_btnm_1, 359, 359);

	//Write style for ws2812_btnm_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->ws2812_btnm_1, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->ws2812_btnm_1, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->ws2812_btnm_1, lv_color_hex(0x6a6868), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->ws2812_btnm_1, LV_BORDER_SIDE_FULL, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->ws2812_btnm_1, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->ws2812_btnm_1, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->ws2812_btnm_1, 3, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->ws2812_btnm_1, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_row(ui->ws2812_btnm_1, 5, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_column(ui->ws2812_btnm_1, 5, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->ws2812_btnm_1, 15, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->ws2812_btnm_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style for ws2812_btnm_1, Part: LV_PART_ITEMS, State: LV_STATE_DEFAULT.
	lv_obj_set_style_border_width(ui->ws2812_btnm_1, 1, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->ws2812_btnm_1, 0, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->ws2812_btnm_1, lv_color_hex(0xc9c9c9), LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_border_side(ui->ws2812_btnm_1, LV_BORDER_SIDE_FULL, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->ws2812_btnm_1, lv_color_hex(0xffffff), LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->ws2812_btnm_1, &lv_font_MiSans_Medium_16, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_text_opa(ui->ws2812_btnm_1, 255, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->ws2812_btnm_1, 25, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->ws2812_btnm_1, 255, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->ws2812_btnm_1, lv_color_hex(0x353637), LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->ws2812_btnm_1, LV_GRAD_DIR_NONE, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->ws2812_btnm_1, 0, LV_PART_ITEMS|LV_STATE_DEFAULT);

	//Write codes ws2812_btn_1
	ui->ws2812_btn_1 = lv_btn_create(ui->ws2812);
	ui->ws2812_btn_1_label = lv_label_create(ui->ws2812_btn_1);
	lv_label_set_text(ui->ws2812_btn_1_label, "All ON");
	lv_label_set_long_mode(ui->ws2812_btn_1_label, LV_LABEL_LONG_WRAP);
	lv_obj_align(ui->ws2812_btn_1_label, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_style_pad_all(ui->ws2812_btn_1, 0, LV_STATE_DEFAULT);
	lv_obj_set_width(ui->ws2812_btn_1_label, LV_PCT(100));
	lv_obj_set_pos(ui->ws2812_btn_1, 381, 93);
	lv_obj_set_size(ui->ws2812_btn_1, 74, 40);

	//Write style for ws2812_btn_1, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->ws2812_btn_1, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->ws2812_btn_1, lv_color_hex(0x28292a), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->ws2812_btn_1, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->ws2812_btn_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->ws2812_btn_1, 10, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->ws2812_btn_1, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->ws2812_btn_1, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->ws2812_btn_1, &lv_font_MiSans_Medium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_opa(ui->ws2812_btn_1, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->ws2812_btn_1, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes ws2812_btn_2
	ui->ws2812_btn_2 = lv_btn_create(ui->ws2812);
	ui->ws2812_btn_2_label = lv_label_create(ui->ws2812_btn_2);
	lv_label_set_text(ui->ws2812_btn_2_label, "All OFF");
	lv_label_set_long_mode(ui->ws2812_btn_2_label, LV_LABEL_LONG_WRAP);
	lv_obj_align(ui->ws2812_btn_2_label, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_style_pad_all(ui->ws2812_btn_2, 0, LV_STATE_DEFAULT);
	lv_obj_set_width(ui->ws2812_btn_2_label, LV_PCT(100));
	lv_obj_set_pos(ui->ws2812_btn_2, 381, 154);
	lv_obj_set_size(ui->ws2812_btn_2, 74, 40);

	//Write style for ws2812_btn_2, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->ws2812_btn_2, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->ws2812_btn_2, lv_color_hex(0x28292a), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->ws2812_btn_2, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->ws2812_btn_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->ws2812_btn_2, 10, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->ws2812_btn_2, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->ws2812_btn_2, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->ws2812_btn_2, &lv_font_MiSans_Medium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_opa(ui->ws2812_btn_2, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->ws2812_btn_2, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes ws2812_btn_3
	ui->ws2812_btn_3 = lv_btn_create(ui->ws2812);
	ui->ws2812_btn_3_label = lv_label_create(ui->ws2812_btn_3);
	lv_label_set_text(ui->ws2812_btn_3_label, "Mode 1");
	lv_label_set_long_mode(ui->ws2812_btn_3_label, LV_LABEL_LONG_WRAP);
	lv_obj_align(ui->ws2812_btn_3_label, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_style_pad_all(ui->ws2812_btn_3, 0, LV_STATE_DEFAULT);
	lv_obj_set_width(ui->ws2812_btn_3_label, LV_PCT(100));
	lv_obj_set_pos(ui->ws2812_btn_3, 381, 215);
	lv_obj_set_size(ui->ws2812_btn_3, 74, 40);

	//Write style for ws2812_btn_3, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->ws2812_btn_3, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->ws2812_btn_3, lv_color_hex(0x28292a), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->ws2812_btn_3, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->ws2812_btn_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->ws2812_btn_3, 10, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->ws2812_btn_3, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->ws2812_btn_3, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->ws2812_btn_3, &lv_font_MiSans_Medium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_opa(ui->ws2812_btn_3, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->ws2812_btn_3, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes ws2812_btn_4
	ui->ws2812_btn_4 = lv_btn_create(ui->ws2812);
	ui->ws2812_btn_4_label = lv_label_create(ui->ws2812_btn_4);
	lv_label_set_text(ui->ws2812_btn_4_label, "Mode 2");
	lv_label_set_long_mode(ui->ws2812_btn_4_label, LV_LABEL_LONG_WRAP);
	lv_obj_align(ui->ws2812_btn_4_label, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_style_pad_all(ui->ws2812_btn_4, 0, LV_STATE_DEFAULT);
	lv_obj_set_width(ui->ws2812_btn_4_label, LV_PCT(100));
	lv_obj_set_pos(ui->ws2812_btn_4, 381, 276);
	lv_obj_set_size(ui->ws2812_btn_4, 74, 40);

	//Write style for ws2812_btn_4, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->ws2812_btn_4, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->ws2812_btn_4, lv_color_hex(0x28292a), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->ws2812_btn_4, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->ws2812_btn_4, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->ws2812_btn_4, 10, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->ws2812_btn_4, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->ws2812_btn_4, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->ws2812_btn_4, &lv_font_MiSans_Medium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_opa(ui->ws2812_btn_4, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->ws2812_btn_4, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes ws2812_btn_5
	ui->ws2812_btn_5 = lv_btn_create(ui->ws2812);
	ui->ws2812_btn_5_label = lv_label_create(ui->ws2812_btn_5);
	lv_label_set_text(ui->ws2812_btn_5_label, "Mode 3");
	lv_label_set_long_mode(ui->ws2812_btn_5_label, LV_LABEL_LONG_WRAP);
	lv_obj_align(ui->ws2812_btn_5_label, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_style_pad_all(ui->ws2812_btn_5, 0, LV_STATE_DEFAULT);
	lv_obj_set_width(ui->ws2812_btn_5_label, LV_PCT(100));
	lv_obj_set_pos(ui->ws2812_btn_5, 381, 337);
	lv_obj_set_size(ui->ws2812_btn_5, 74, 40);

	//Write style for ws2812_btn_5, Part: LV_PART_MAIN, State: LV_STATE_DEFAULT.
	lv_obj_set_style_bg_opa(ui->ws2812_btn_5, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->ws2812_btn_5, lv_color_hex(0x28292a), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->ws2812_btn_5, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->ws2812_btn_5, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_radius(ui->ws2812_btn_5, 10, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->ws2812_btn_5, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->ws2812_btn_5, lv_color_hex(0xffffff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->ws2812_btn_5, &lv_font_MiSans_Medium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_opa(ui->ws2812_btn_5, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->ws2812_btn_5, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);

	//The custom code of ws2812.


	//Update current screen layout.
	lv_obj_update_layout(ui->ws2812);

	//Init events for screen.
	events_init_ws2812(ui);
}
