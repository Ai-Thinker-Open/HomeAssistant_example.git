# Copyright 2024 NXP
# NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
# accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
# activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
# comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
# terms, then you may not retain, install, activate or otherwise use the software.

import SDL
import utime as time
import usys as sys
import lvgl as lv
import lodepng as png
import ustruct
import fs_driver

lv.init()
SDL.init(w=480,h=480)

# Register SDL display driver.
disp_buf1 = lv.disp_draw_buf_t()
buf1_1 = bytearray(480*10)
disp_buf1.init(buf1_1, None, len(buf1_1)//4)
disp_drv = lv.disp_drv_t()
disp_drv.init()
disp_drv.draw_buf = disp_buf1
disp_drv.flush_cb = SDL.monitor_flush
disp_drv.hor_res = 480
disp_drv.ver_res = 480
disp_drv.register()

# Regsiter SDL mouse driver
indev_drv = lv.indev_drv_t()
indev_drv.init()
indev_drv.type = lv.INDEV_TYPE.POINTER
indev_drv.read_cb = SDL.mouse_read
indev_drv.register()

fs_drv = lv.fs_drv_t()
fs_driver.fs_register(fs_drv, 'Z')

# Below: Taken from https://github.com/lvgl/lv_binding_micropython/blob/master/driver/js/imagetools.py#L22-L94

COLOR_SIZE = lv.color_t.__SIZE__
COLOR_IS_SWAPPED = hasattr(lv.color_t().ch,'green_h')

class lodepng_error(RuntimeError):
    def __init__(self, err):
        if type(err) is int:
            super().__init__(png.error_text(err))
        else:
            super().__init__(err)

# Parse PNG file header
# Taken from https://github.com/shibukawa/imagesize_py/blob/ffef30c1a4715c5acf90e8945ceb77f4a2ed2d45/imagesize.py#L63-L85

def get_png_info(decoder, src, header):
    # Only handle variable image types

    if lv.img.src_get_type(src) != lv.img.SRC.VARIABLE:
        return lv.RES.INV

    data = lv.img_dsc_t.__cast__(src).data
    if data == None:
        return lv.RES.INV

    png_header = bytes(data.__dereference__(24))

    if png_header.startswith(b'\211PNG\r\n\032\n'):
        if png_header[12:16] == b'IHDR':
            start = 16
        # Maybe this is for an older PNG version.
        else:
            start = 8
        try:
            width, height = ustruct.unpack(">LL", png_header[start:start+8])
        except ustruct.error:
            return lv.RES.INV
    else:
        return lv.RES.INV

    header.always_zero = 0
    header.w = width
    header.h = height
    header.cf = lv.img.CF.TRUE_COLOR_ALPHA

    return lv.RES.OK

def convert_rgba8888_to_bgra8888(img_view):
    for i in range(0, len(img_view), lv.color_t.__SIZE__):
        ch = lv.color_t.__cast__(img_view[i:i]).ch
        ch.red, ch.blue = ch.blue, ch.red

# Read and parse PNG file

def open_png(decoder, dsc):
    img_dsc = lv.img_dsc_t.__cast__(dsc.src)
    png_data = img_dsc.data
    png_size = img_dsc.data_size
    png_decoded = png.C_Pointer()
    png_width = png.C_Pointer()
    png_height = png.C_Pointer()
    error = png.decode32(png_decoded, png_width, png_height, png_data, png_size)
    if error:
        raise lodepng_error(error)
    img_size = png_width.int_val * png_height.int_val * 4
    img_data = png_decoded.ptr_val
    img_view = img_data.__dereference__(img_size)

    if COLOR_SIZE == 4:
        convert_rgba8888_to_bgra8888(img_view)
    else:
        raise lodepng_error("Error: Color mode not supported yet!")

    dsc.img_data = img_data
    return lv.RES.OK

# Above: Taken from https://github.com/lvgl/lv_binding_micropython/blob/master/driver/js/imagetools.py#L22-L94

decoder = lv.img.decoder_create()
decoder.info_cb = get_png_info
decoder.open_cb = open_png

def anim_x_cb(obj, v):
    obj.set_x(v)

def anim_y_cb(obj, v):
    obj.set_y(v)

def anim_width_cb(obj, v):
    obj.set_width(v)

def anim_height_cb(obj, v):
    obj.set_height(v)

def anim_img_zoom_cb(obj, v):
    obj.set_zoom(v)

def anim_img_rotate_cb(obj, v):
    obj.set_angle(v)

global_font_cache = {}
def test_font(font_family, font_size):
    global global_font_cache
    if font_family + str(font_size) in global_font_cache:
        return global_font_cache[font_family + str(font_size)]
    if font_size % 2:
        candidates = [
            (font_family, font_size),
            (font_family, font_size-font_size%2),
            (font_family, font_size+font_size%2),
            ("montserrat", font_size-font_size%2),
            ("montserrat", font_size+font_size%2),
            ("montserrat", 16)
        ]
    else:
        candidates = [
            (font_family, font_size),
            ("montserrat", font_size),
            ("montserrat", 16)
        ]
    for (family, size) in candidates:
        try:
            if eval(f'lv.font_{family}_{size}'):
                global_font_cache[font_family + str(font_size)] = eval(f'lv.font_{family}_{size}')
                if family != font_family or size != font_size:
                    print(f'WARNING: lv.font_{family}_{size} is used!')
                return eval(f'lv.font_{family}_{size}')
        except AttributeError:
            try:
                load_font = lv.font_load(f"Z:MicroPython/lv_font_{family}_{size}.fnt")
                global_font_cache[font_family + str(font_size)] = load_font
                return load_font
            except:
                if family == font_family and size == font_size:
                    print(f'WARNING: lv.font_{family}_{size} is NOT supported!')

global_image_cache = {}
def load_image(file):
    global global_image_cache
    if file in global_image_cache:
        return global_image_cache[file]
    try:
        with open(file,'rb') as f:
            data = f.read()
    except:
        print(f'Could not open {file}')
        sys.exit()

    img = lv.img_dsc_t({
        'data_size': len(data),
        'data': data
    })
    global_image_cache[file] = img
    return img

def calendar_event_handler(e,obj):
    code = e.get_code()

    if code == lv.EVENT.VALUE_CHANGED:
        source = e.get_current_target()
        date = lv.calendar_date_t()
        if source.get_pressed_date(date) == lv.RES.OK:
            source.set_highlighted_dates([date], 1)

def spinbox_increment_event_cb(e, obj):
    code = e.get_code()
    if code == lv.EVENT.SHORT_CLICKED or code == lv.EVENT.LONG_PRESSED_REPEAT:
        obj.increment()
def spinbox_decrement_event_cb(e, obj):
    code = e.get_code()
    if code == lv.EVENT.SHORT_CLICKED or code == lv.EVENT.LONG_PRESSED_REPEAT:
        obj.decrement()

def digital_clock_cb(timer, obj, current_time, show_second, use_ampm):
    hour = int(current_time[0])
    minute = int(current_time[1])
    second = int(current_time[2])
    ampm = current_time[3]
    second = second + 1
    if second == 60:
        second = 0
        minute = minute + 1
        if minute == 60:
            minute = 0
            hour = hour + 1
            if use_ampm:
                if hour == 12:
                    if ampm == 'AM':
                        ampm = 'PM'
                    elif ampm == 'PM':
                        ampm = 'AM'
                if hour > 12:
                    hour = hour % 12
    hour = hour % 24
    if use_ampm:
        if show_second:
            obj.set_text("%d:%02d:%02d %s" %(hour, minute, second, ampm))
        else:
            obj.set_text("%d:%02d %s" %(hour, minute, ampm))
    else:
        if show_second:
            obj.set_text("%d:%02d:%02d" %(hour, minute, second))
        else:
            obj.set_text("%d:%02d" %(hour, minute))
    current_time[0] = hour
    current_time[1] = minute
    current_time[2] = second
    current_time[3] = ampm

def analog_clock_cb(timer, obj):
    datetime = time.localtime()
    hour = datetime[3]
    if hour >= 12: hour = hour - 12
    obj.set_time(hour, datetime[4], datetime[5])

def datetext_event_handler(e, obj):
    code = e.get_code()
    target = e.get_target()
    if code == lv.EVENT.FOCUSED:
        if obj is None:
            bg = lv.layer_top()
            bg.add_flag(lv.obj.FLAG.CLICKABLE)
            obj = lv.calendar(bg)
            scr = target.get_screen()
            scr_height = scr.get_height()
            scr_width = scr.get_width()
            obj.set_size(int(scr_width * 0.8), int(scr_height * 0.8))
            datestring = target.get_text()
            year = int(datestring.split('/')[0])
            month = int(datestring.split('/')[1])
            day = int(datestring.split('/')[2])
            obj.set_showed_date(year, month)
            highlighted_days=[lv.calendar_date_t({'year':year, 'month':month, 'day':day})]
            obj.set_highlighted_dates(highlighted_days, 1)
            obj.align(lv.ALIGN.CENTER, 0, 0)
            lv.calendar_header_arrow(obj)
            obj.add_event_cb(lambda e: datetext_calendar_event_handler(e, target), lv.EVENT.ALL, None)
            scr.update_layout()

def datetext_calendar_event_handler(e, obj):
    code = e.get_code()
    target = e.get_current_target()
    if code == lv.EVENT.VALUE_CHANGED:
        date = lv.calendar_date_t()
        if target.get_pressed_date(date) == lv.RES.OK:
            obj.set_text(f"{date.year}/{date.month}/{date.day}")
            bg = lv.layer_top()
            bg.clear_flag(lv.obj.FLAG.CLICKABLE)
            bg.set_style_bg_opa(lv.OPA.TRANSP, 0)
            target.delete()

# Create main
main = lv.obj()
main.set_size(480, 480)
main.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
# Set style for main, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
main.set_style_bg_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
main.set_style_bg_color(lv.color_hex(0x000000), lv.PART.MAIN|lv.STATE.DEFAULT)
main.set_style_bg_grad_dir(lv.GRAD_DIR.NONE, lv.PART.MAIN|lv.STATE.DEFAULT)

# Create main_wokong
main_wokong = lv.img(main)
main_wokong.set_src("B:MicroPython/_wukong_alpha_480x480.bin")
main_wokong.add_flag(lv.obj.FLAG.CLICKABLE)
main_wokong.set_pivot(50,50)
main_wokong.set_angle(0)
main_wokong.set_pos(0, 0)
main_wokong.set_size(480, 480)
# Set style for main_wokong, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
main_wokong.set_style_img_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
main_wokong.set_style_radius(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_wokong.set_style_clip_corner(True, lv.PART.MAIN|lv.STATE.DEFAULT)

# Create main_digital_clock_1
main_digital_clock_1_time = [int(11), int(25), int(50), ""]
main_digital_clock_1 = lv.dclock(main, "11:25")
main_digital_clock_1_timer = lv.timer_create_basic()
main_digital_clock_1_timer.set_period(1000)
main_digital_clock_1_timer.set_cb(lambda src: digital_clock_cb(main_digital_clock_1_timer, main_digital_clock_1, main_digital_clock_1_time, False, False ))
main_digital_clock_1.set_pos(234, 39)
main_digital_clock_1.set_size(243, 98)
# Set style for main_digital_clock_1, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
main_digital_clock_1.set_style_radius(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_digital_clock_1.set_style_text_color(lv.color_hex(0xffffff), lv.PART.MAIN|lv.STATE.DEFAULT)
main_digital_clock_1.set_style_text_font(test_font("MiSans_Semibold", 70), lv.PART.MAIN|lv.STATE.DEFAULT)
main_digital_clock_1.set_style_text_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
main_digital_clock_1.set_style_text_letter_space(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_digital_clock_1.set_style_text_align(lv.TEXT_ALIGN.CENTER, lv.PART.MAIN|lv.STATE.DEFAULT)
main_digital_clock_1.set_style_bg_opa(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_digital_clock_1.set_style_pad_top(7, lv.PART.MAIN|lv.STATE.DEFAULT)
main_digital_clock_1.set_style_pad_right(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_digital_clock_1.set_style_pad_bottom(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_digital_clock_1.set_style_pad_left(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_digital_clock_1.set_style_shadow_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)

# Create main_img_1
main_img_1 = lv.img(main)
main_img_1.set_src("B:MicroPython/_temp_alpha_32x32.bin")
main_img_1.add_flag(lv.obj.FLAG.CLICKABLE)
main_img_1.set_pivot(50,50)
main_img_1.set_angle(0)
main_img_1.set_pos(348, 139)
main_img_1.set_size(32, 32)
# Set style for main_img_1, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
main_img_1.set_style_img_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
main_img_1.set_style_radius(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_img_1.set_style_clip_corner(True, lv.PART.MAIN|lv.STATE.DEFAULT)

# Create main_img_2
main_img_2 = lv.img(main)
main_img_2.set_src("B:MicroPython/_huim_alpha_32x32.bin")
main_img_2.add_flag(lv.obj.FLAG.CLICKABLE)
main_img_2.set_pivot(50,50)
main_img_2.set_angle(0)
main_img_2.set_pos(348, 187)
main_img_2.set_size(32, 32)
# Set style for main_img_2, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
main_img_2.set_style_img_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
main_img_2.set_style_radius(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_img_2.set_style_clip_corner(True, lv.PART.MAIN|lv.STATE.DEFAULT)

# Create main_label_temp
main_label_temp = lv.label(main)
main_label_temp.set_text("25 F°")
main_label_temp.set_long_mode(lv.label.LONG.WRAP)
main_label_temp.set_width(lv.pct(100))
main_label_temp.set_pos(385, 146)
main_label_temp.set_size(84, 32)
# Set style for main_label_temp, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
main_label_temp.set_style_border_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_temp.set_style_radius(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_temp.set_style_text_color(lv.color_hex(0xe6e1e1), lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_temp.set_style_text_font(test_font("MiSans_Medium", 20), lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_temp.set_style_text_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_temp.set_style_text_letter_space(2, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_temp.set_style_text_line_space(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_temp.set_style_text_align(lv.TEXT_ALIGN.LEFT, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_temp.set_style_bg_opa(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_temp.set_style_pad_top(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_temp.set_style_pad_right(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_temp.set_style_pad_bottom(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_temp.set_style_pad_left(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_temp.set_style_shadow_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)

# Create main_label_1
main_label_1 = lv.label(main)
main_label_1.set_text("95 %")
main_label_1.set_long_mode(lv.label.LONG.WRAP)
main_label_1.set_width(lv.pct(100))
main_label_1.set_pos(385, 195)
main_label_1.set_size(82, 30)
# Set style for main_label_1, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
main_label_1.set_style_border_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_1.set_style_radius(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_1.set_style_text_color(lv.color_hex(0xe6e1e1), lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_1.set_style_text_font(test_font("MiSans_Medium", 20), lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_1.set_style_text_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_1.set_style_text_letter_space(2, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_1.set_style_text_line_space(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_1.set_style_text_align(lv.TEXT_ALIGN.LEFT, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_1.set_style_bg_opa(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_1.set_style_pad_top(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_1.set_style_pad_right(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_1.set_style_pad_bottom(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_1.set_style_pad_left(0, lv.PART.MAIN|lv.STATE.DEFAULT)
main_label_1.set_style_shadow_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)

main.update_layout()
# Create config
config = lv.obj()
config.set_size(480, 480)
config.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
# Set style for config, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
config.set_style_bg_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
config.set_style_bg_color(lv.color_hex(0x000000), lv.PART.MAIN|lv.STATE.DEFAULT)
config.set_style_bg_grad_dir(lv.GRAD_DIR.NONE, lv.PART.MAIN|lv.STATE.DEFAULT)

# Create config_img_1
config_img_1 = lv.img(config)
config_img_1.set_src("B:MicroPython/_gh_b486321d51a4_430_alpha_200x200.bin")
config_img_1.add_flag(lv.obj.FLAG.CLICKABLE)
config_img_1.set_pivot(50,50)
config_img_1.set_angle(0)
config_img_1.set_pos(138, 98)
config_img_1.set_size(200, 200)
# Set style for config_img_1, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
config_img_1.set_style_img_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
config_img_1.set_style_radius(0, lv.PART.MAIN|lv.STATE.DEFAULT)
config_img_1.set_style_clip_corner(True, lv.PART.MAIN|lv.STATE.DEFAULT)

# Create config_btn_1
config_btn_1 = lv.btn(config)
config_btn_1_label = lv.label(config_btn_1)
config_btn_1_label.set_text("Start WiFi Config")
config_btn_1_label.set_long_mode(lv.label.LONG.WRAP)
config_btn_1_label.set_width(lv.pct(100))
config_btn_1_label.align(lv.ALIGN.CENTER, 0, 0)
config_btn_1.set_style_pad_all(0, lv.STATE.DEFAULT)
config_btn_1.set_pos(143, 351)
config_btn_1.set_size(190, 37)
# Set style for config_btn_1, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
config_btn_1.set_style_bg_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
config_btn_1.set_style_bg_color(lv.color_hex(0x404040), lv.PART.MAIN|lv.STATE.DEFAULT)
config_btn_1.set_style_bg_grad_dir(lv.GRAD_DIR.NONE, lv.PART.MAIN|lv.STATE.DEFAULT)
config_btn_1.set_style_border_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
config_btn_1.set_style_radius(10, lv.PART.MAIN|lv.STATE.DEFAULT)
config_btn_1.set_style_shadow_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
config_btn_1.set_style_text_color(lv.color_hex(0xffffff), lv.PART.MAIN|lv.STATE.DEFAULT)
config_btn_1.set_style_text_font(test_font("MiSans_Medium", 16), lv.PART.MAIN|lv.STATE.DEFAULT)
config_btn_1.set_style_text_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
config_btn_1.set_style_text_align(lv.TEXT_ALIGN.CENTER, lv.PART.MAIN|lv.STATE.DEFAULT)

config.update_layout()
# Create ws2812
ws2812 = lv.obj()
ws2812.set_size(480, 480)
ws2812.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
# Set style for ws2812, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
ws2812.set_style_bg_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812.set_style_bg_color(lv.color_hex(0x000000), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812.set_style_bg_grad_dir(lv.GRAD_DIR.NONE, lv.PART.MAIN|lv.STATE.DEFAULT)

# Create ws2812_btnm_1
ws2812_btnm_1 = lv.btnmatrix(ws2812)
ws2812_btnm_1_text_map = [" ", " ", " ", " ", " ", " ", " ", " ", "\n", " ", " ", " ", " ", " ", " ", " ", " ", "\n", " ", " ", " ", " ", " ", " ", " ", " ", "\n", " ", " ", " ", " ", " ", " ", " ", " ", "\n", " ", " ", " ", " ", " ", " ", " ", " ", "\n", " ", " ", " ", " ", " ", " ", " ", " ", "\n", " ", " ", " ", " ", " ", " ", " ", " ", "\n", "  ", " ", " ", " ", " ", " ", " ", " ", "",]
ws2812_btnm_1.set_map(ws2812_btnm_1_text_map)
ws2812_btnm_1.set_pos(33, 86)
ws2812_btnm_1.set_size(300, 300)
# Set style for ws2812_btnm_1, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
ws2812_btnm_1.set_style_border_width(1, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_border_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_border_color(lv.color_hex(0x6a6868), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_border_side(lv.BORDER_SIDE.FULL, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_pad_top(16, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_pad_bottom(16, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_pad_left(16, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_pad_right(16, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_pad_row(8, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_pad_column(8, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_radius(15, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_bg_opa(0, lv.PART.MAIN|lv.STATE.DEFAULT)

# Set style for ws2812_btnm_1, Part: lv.PART.ITEMS, State: lv.STATE.DEFAULT.
ws2812_btnm_1.set_style_border_width(1, lv.PART.ITEMS|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_border_opa(0, lv.PART.ITEMS|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_border_color(lv.color_hex(0xc9c9c9), lv.PART.ITEMS|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_border_side(lv.BORDER_SIDE.FULL, lv.PART.ITEMS|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_text_color(lv.color_hex(0xffffff), lv.PART.ITEMS|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_text_font(test_font("montserratMedium", 16), lv.PART.ITEMS|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_text_opa(255, lv.PART.ITEMS|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_radius(15, lv.PART.ITEMS|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_bg_opa(255, lv.PART.ITEMS|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_bg_color(lv.color_hex(0x353637), lv.PART.ITEMS|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_bg_grad_dir(lv.GRAD_DIR.NONE, lv.PART.ITEMS|lv.STATE.DEFAULT)
ws2812_btnm_1.set_style_shadow_width(0, lv.PART.ITEMS|lv.STATE.DEFAULT)

# Create ws2812_btn_1
ws2812_btn_1 = lv.btn(ws2812)
ws2812_btn_1_label = lv.label(ws2812_btn_1)
ws2812_btn_1_label.set_text("All ON")
ws2812_btn_1_label.set_long_mode(lv.label.LONG.WRAP)
ws2812_btn_1_label.set_width(lv.pct(100))
ws2812_btn_1_label.align(lv.ALIGN.CENTER, 0, 0)
ws2812_btn_1.set_style_pad_all(0, lv.STATE.DEFAULT)
ws2812_btn_1.set_pos(355, 99)
ws2812_btn_1.set_size(109, 40)
# Set style for ws2812_btn_1, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
ws2812_btn_1.set_style_bg_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_1.set_style_bg_color(lv.color_hex(0x28292a), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_1.set_style_bg_grad_dir(lv.GRAD_DIR.NONE, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_1.set_style_border_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_1.set_style_radius(10, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_1.set_style_shadow_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_1.set_style_text_color(lv.color_hex(0xffffff), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_1.set_style_text_font(test_font("MiSans_Medium", 16), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_1.set_style_text_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_1.set_style_text_align(lv.TEXT_ALIGN.CENTER, lv.PART.MAIN|lv.STATE.DEFAULT)

# Create ws2812_btn_2
ws2812_btn_2 = lv.btn(ws2812)
ws2812_btn_2_label = lv.label(ws2812_btn_2)
ws2812_btn_2_label.set_text("All OFF")
ws2812_btn_2_label.set_long_mode(lv.label.LONG.WRAP)
ws2812_btn_2_label.set_width(lv.pct(100))
ws2812_btn_2_label.align(lv.ALIGN.CENTER, 0, 0)
ws2812_btn_2.set_style_pad_all(0, lv.STATE.DEFAULT)
ws2812_btn_2.set_pos(355, 155)
ws2812_btn_2.set_size(109, 40)
# Set style for ws2812_btn_2, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
ws2812_btn_2.set_style_bg_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_2.set_style_bg_color(lv.color_hex(0x28292a), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_2.set_style_bg_grad_dir(lv.GRAD_DIR.NONE, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_2.set_style_border_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_2.set_style_radius(10, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_2.set_style_shadow_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_2.set_style_text_color(lv.color_hex(0xffffff), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_2.set_style_text_font(test_font("MiSans_Medium", 16), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_2.set_style_text_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_2.set_style_text_align(lv.TEXT_ALIGN.CENTER, lv.PART.MAIN|lv.STATE.DEFAULT)

# Create ws2812_btn_3
ws2812_btn_3 = lv.btn(ws2812)
ws2812_btn_3_label = lv.label(ws2812_btn_3)
ws2812_btn_3_label.set_text("Mode 1")
ws2812_btn_3_label.set_long_mode(lv.label.LONG.WRAP)
ws2812_btn_3_label.set_width(lv.pct(100))
ws2812_btn_3_label.align(lv.ALIGN.CENTER, 0, 0)
ws2812_btn_3.set_style_pad_all(0, lv.STATE.DEFAULT)
ws2812_btn_3.set_pos(355, 211)
ws2812_btn_3.set_size(109, 40)
# Set style for ws2812_btn_3, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
ws2812_btn_3.set_style_bg_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_3.set_style_bg_color(lv.color_hex(0x28292a), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_3.set_style_bg_grad_dir(lv.GRAD_DIR.NONE, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_3.set_style_border_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_3.set_style_radius(10, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_3.set_style_shadow_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_3.set_style_text_color(lv.color_hex(0xffffff), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_3.set_style_text_font(test_font("MiSans_Medium", 16), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_3.set_style_text_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_3.set_style_text_align(lv.TEXT_ALIGN.CENTER, lv.PART.MAIN|lv.STATE.DEFAULT)

# Create ws2812_btn_4
ws2812_btn_4 = lv.btn(ws2812)
ws2812_btn_4_label = lv.label(ws2812_btn_4)
ws2812_btn_4_label.set_text("Mode 2")
ws2812_btn_4_label.set_long_mode(lv.label.LONG.WRAP)
ws2812_btn_4_label.set_width(lv.pct(100))
ws2812_btn_4_label.align(lv.ALIGN.CENTER, 0, 0)
ws2812_btn_4.set_style_pad_all(0, lv.STATE.DEFAULT)
ws2812_btn_4.set_pos(355, 267)
ws2812_btn_4.set_size(109, 40)
# Set style for ws2812_btn_4, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
ws2812_btn_4.set_style_bg_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_4.set_style_bg_color(lv.color_hex(0x28292a), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_4.set_style_bg_grad_dir(lv.GRAD_DIR.NONE, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_4.set_style_border_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_4.set_style_radius(10, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_4.set_style_shadow_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_4.set_style_text_color(lv.color_hex(0xffffff), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_4.set_style_text_font(test_font("MiSans_Medium", 16), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_4.set_style_text_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_4.set_style_text_align(lv.TEXT_ALIGN.CENTER, lv.PART.MAIN|lv.STATE.DEFAULT)

# Create ws2812_btn_5
ws2812_btn_5 = lv.btn(ws2812)
ws2812_btn_5_label = lv.label(ws2812_btn_5)
ws2812_btn_5_label.set_text("Mode 3")
ws2812_btn_5_label.set_long_mode(lv.label.LONG.WRAP)
ws2812_btn_5_label.set_width(lv.pct(100))
ws2812_btn_5_label.align(lv.ALIGN.CENTER, 0, 0)
ws2812_btn_5.set_style_pad_all(0, lv.STATE.DEFAULT)
ws2812_btn_5.set_pos(355, 323)
ws2812_btn_5.set_size(109, 40)
# Set style for ws2812_btn_5, Part: lv.PART.MAIN, State: lv.STATE.DEFAULT.
ws2812_btn_5.set_style_bg_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_5.set_style_bg_color(lv.color_hex(0x28292a), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_5.set_style_bg_grad_dir(lv.GRAD_DIR.NONE, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_5.set_style_border_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_5.set_style_radius(10, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_5.set_style_shadow_width(0, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_5.set_style_text_color(lv.color_hex(0xffffff), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_5.set_style_text_font(test_font("MiSans_Medium", 16), lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_5.set_style_text_opa(255, lv.PART.MAIN|lv.STATE.DEFAULT)
ws2812_btn_5.set_style_text_align(lv.TEXT_ALIGN.CENTER, lv.PART.MAIN|lv.STATE.DEFAULT)

ws2812.update_layout()

def main_event_handler(e):
    code = e.get_code()
    indev = lv.indev_get_act()
    gestureDir = lv.DIR.NONE
    if indev is not None: gestureDir = indev.get_gesture_dir()
    if (code == lv.EVENT.GESTURE and lv.DIR.RIGHT == gestureDir):
        if indev is not None: indev.wait_release()
        pass
        lv.scr_load_anim(config, lv.SCR_LOAD_ANIM.OVER_RIGHT, 150, 150, False)
    indev = lv.indev_get_act()
    gestureDir = lv.DIR.NONE
    if indev is not None: gestureDir = indev.get_gesture_dir()
    if (code == lv.EVENT.GESTURE and lv.DIR.LEFT == gestureDir):
        if indev is not None: indev.wait_release()
        pass
        lv.scr_load_anim(ws2812, lv.SCR_LOAD_ANIM.OVER_LEFT, 150, 150, False)
main.add_event_cb(lambda e: main_event_handler(e), lv.EVENT.ALL, None)

def config_event_handler(e):
    code = e.get_code()
    indev = lv.indev_get_act()
    gestureDir = lv.DIR.NONE
    if indev is not None: gestureDir = indev.get_gesture_dir()
    if (code == lv.EVENT.GESTURE and lv.DIR.LEFT == gestureDir):
        if indev is not None: indev.wait_release()
        pass
        lv.scr_load_anim(main, lv.SCR_LOAD_ANIM.OVER_LEFT, 150, 150, False)
config.add_event_cb(lambda e: config_event_handler(e), lv.EVENT.ALL, None)

def ws2812_event_handler(e):
    code = e.get_code()
    indev = lv.indev_get_act()
    gestureDir = lv.DIR.NONE
    if indev is not None: gestureDir = indev.get_gesture_dir()
    if (code == lv.EVENT.GESTURE and lv.DIR.RIGHT == gestureDir):
        if indev is not None: indev.wait_release()
        pass
        lv.scr_load_anim(main, lv.SCR_LOAD_ANIM.OVER_LEFT, 150, 150, False)
ws2812.add_event_cb(lambda e: ws2812_event_handler(e), lv.EVENT.ALL, None)

# content from custom.py

# Load the default screen
lv.scr_load(main)

while SDL.check():
    time.sleep_ms(5)

