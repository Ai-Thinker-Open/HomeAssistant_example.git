/*
* Copyright 2024 NXP
* NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
* accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
* activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
* comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
* terms, then you may not retain, install, activate or otherwise use the software.
*/


#ifndef DEMO_EVENTS_INIT_H_
#define DEMO_EVENTS_INIT_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "demo_gui_guider.h"

    void demo_events_init(lv_demo_ui* ui);

    void events_init_WidgetsDemo(lv_demo_ui* ui);

#ifdef __cplusplus
}
#endif
#endif /* EVENT_CB_H_ */
