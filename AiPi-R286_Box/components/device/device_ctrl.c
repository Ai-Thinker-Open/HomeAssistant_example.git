/**
 * @file device_ctrl.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-12-15
 *
 * @copyright Copyright (c) 2023
 *
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "device_ctrl.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "gui_guider.h"
#include "user_esflash.h"

#include "cJSON.h"
#include <lwip/tcpip.h>
#include "wifi_event.h"
#include "bl_fw_api.h"
#include <lwip/sockets.h>
#include <lwip/inet.h>
#include "wifi_mgmr_ext.h"
#include "wifi_mgmr.h"
#include "lwip/dns.h"
#include "log.h"

#include "aiio_system.h"
#include "aiio_wifi.h"

#include "events_init.h"
#include "homeAssistantPort.h"

#define DBG_TAG "device_ctrl"

#define TIMER_STEP 3 //定时3 小时更新一次时间

uint8_t led_id_arry[64] = { 56,55,40,39,24,23,8,7,
                            57,54,41,38,25,22,9,6,
                            58,53,42,37,26,21,10,5,
                            59,52,43,36,27,20,11,4,
                            60,51,44,35,28,19,12,3,
                            61,50,45,34,29,18,13,2,
                            62,49,46,33,30,17,14,1,
                            63,48,47,32,31,16,15,0
};

bool config_scr_del = true;
bool main_scr_del = false;
bool ws2812_scr_del = true;

int temp = 0;
int __humi = 0;
void* device_task;

char humidity[16] = { 0 };


static xTimerHandle http_timer;



int event_get_mode(void);

void device_state_task(void* arg)
{
    lv_ui* ui = (lv_ui*)arg;
    static  device_state_t device_state;
    static char* ssid;
    static char* password;
    static ip_addr_t dns_addr;
    esay_flash_init();
    // dns_init();

    while (1)
    {
        xTaskNotifyWait(0xffffffff, 0x0, &device_state, portMAX_DELAY);
        switch (device_state)
        {
            case DEVICE_STATE_SYSTEM_START:
            {
                LOG_I("DEVICE_STATE_SYSTEM_START");
                //读取Flash WiFi 节点
                if (flash_check_key(FLASH_SSID_KEY)) {
                    ssid = flash_get_data(FLASH_SSID_KEY, 64);
                    password = flash_get_data(FLASH_PASS_KEY, 32);
                    LOG_I("wifi connect ......");
                    staWiFiConnect(ssid, password);
                    vPortFree(ssid);
                    vPortFree(password);
                }
                else {
                    ssid = pvPortMalloc(64);
                    password = pvPortMalloc(32);
                    sprintf(ssid, "FAE@Seahi");
                    sprintf(password, "fae12345678");
                    staWiFiConnect(ssid, password);
                    vPortFree(ssid);
                    vPortFree(password);
                }
            }
            break;
            case DEVICE_STATE_WIFI_CONNECT:
            {
                LOG_I(" DEVICE_STATE_WIFI_CONNECT");
            }
            break;
            case DEVICE_STATE_WIFI_CONNECT_OK:
            {
                LOG_I("DEVICE_STATE_WIFI_CONNECT_OK");
            }
            break;
            case  DEVICE_STATE_WIFI_GO_IP:
            {
                uint32_t ipv4_addr = 0;
                wifi_sta_ip4_addr_get(&ipv4_addr, NULL, NULL, NULL);
                LOG_I("DEVICE_STATE_WIFI_GO_IP:%s", inet_ntoa(ipv4_addr));
                // wifi_mgmr_sta_connect_params();
                aiio_wifi_sta_connect_ind_stat_info_t sta_info;

                aiio_wifi_sta_connect_ind_stat_get(&sta_info);
                flash_erase_set(FLASH_PASS_KEY, sta_info.passphr);
                flash_erase_set(FLASH_SSID_KEY, sta_info.ssid);
                //设置DNS 服务器
                // inet_aton("223.5.5.5", &dns_addr.addr);
                // dns_setserver(0, &dns_addr);
                // vTaskResume(https_Handle);
                //连接MQTT 服务器
                // homeAssistant_dev.wifi_ssid = sta_info.ssid;
                // homeAssistant_dev.wifi_password = sta_info.passphr;
                // memcpy(homeAssistant_dev.bssid, sta_info.bssid, 6);

                homeAssistant_device_start();
                //如果配网界面没有回到主页面，则自动回到主页面 

            }
            break;
            case DEVICE_STATE_WIFI_DISCONNECT:
            {
                LOG_I("DEVICE_STATE_WIFI_DISCONNECT");
                // ssid = flash_get_data(FLASH_SSID_KEY, 64);
                // password = flash_get_data(FLASH_PASS_KEY, 32);
                // LOG_I("wifi connect ......");
                // staWiFiConnect(ssid, password);

            }
            break;

            case DEVICE_STATE_BLUFI_CONFIG_START:
            {
                LOG_I("DEVICE_STATE_BLUFI_CONFIG_START");
                //断开MQTT
                homeAssisatnt_device_stop();
                //配网
                homeassistant_blufi_start();
            }
            break;
            case DEVICE_SATTE_SENSOR_DATA_TEMP:

                if (!main_scr_del&&config_scr_del&&ws2812_scr_del) {
                    lv_label_set_text_fmt(guider_ui.main_label_temp, "%d°C", temp);
                }
                break;
            case DEVICE_SATTE_SENSOR_DATA_HUMI:
                LOG_I("DEVICE_SATTE_SENSOR_DATA_HUMI %d", __humi);

                if (!main_scr_del&&config_scr_del&&ws2812_scr_del) {
                    lv_label_set_text_fmt(guider_ui.main_label_huim, "%d%%", __humi);
                }
                break;
            default:
                break;
        }
    }

}

void device_send_state_notify(device_state_t device_state, int is_irq)
{
    if (device_task==NULL) {
        LOG_E("device_task is no init\r\n");
        return;
    }
    BaseType_t xHigherPriorityTaskWoken;
    xHigherPriorityTaskWoken = pdFALSE;
    if (is_irq)xTaskNotifyFromISR((TaskHandle_t)device_task, device_state, eSetValueWithoutOverwrite, &xHigherPriorityTaskWoken);
    else xTaskNotify((TaskHandle_t)device_task, device_state, eSetValueWithoutOverwrite);
}