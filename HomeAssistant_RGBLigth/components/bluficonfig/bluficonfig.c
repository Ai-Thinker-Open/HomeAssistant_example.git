/**
 * @file bluficonfig.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-01-23
 *
 * @copyright Copyright (c) 2024
 *
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bluficonfig.h"
#include "log.h"

#define DBG_TAG "BLUFI"
#define BLUFI_NAME "Ai-M62-Buld"

// static aiio_blufi_cb_t blufi_cb = { 0 };
static bool blufi_state = 0;

/**
 * @brief 启动配网
 *
*/
void homeassistant_blufi_start(void)
{
    blufi_set_name(BLUFI_NAME);
    blufi_start();
    blufi_state = true;
}
/**
 * @brief 更新配网状态
 *
 * @param blufi_event
*/
void update_homeassistant_blufi_event(int blufi_event)
{
    if (!blufi_state) return;
    // aiio_blufi_wifi_event(blufi_event, NULL);
    blufi_wifi_evt_export(blufi_event);

}