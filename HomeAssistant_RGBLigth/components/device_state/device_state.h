/**
 * @file device_state.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-02-27
 *
 * @copyright Copyright (c) 2024
 *
*/
#ifndef DEVICE_STATE_H
#define DEVICE_STATE_H

#include "user_esflash.h"
#include "homeAssistantMQTT.h"
#include "bluficonfig.h"

#define CONFIG_RESET_BOOT 1
#define CONFIG_BUTTON 2

#define CONFIG_RGB_RED_PIN 28
#define CONFIG_RGB_GREEN_PIN 29
#define CONFIG_RGB_BLUE_PIN 30

#define CONFIG_FLASH_KEY_SSID "SSID"
#define CONFIG_FLASH_KEY_PWD  "PASSWORD"
#define CONFIG_FLASH_KEY_BSSID "BSSID"

#define CONFIG_NETWORK_TYPE CONFIG_BUTTON 

#if (CONFIG_NETWORK_TYPE==CONFIG_RESET_BOOT)

#define CONFIG_RESET_NUBLE "RESET_N"
#define CONFIG_RESET_NUBLE_CONT 4   //重启4次启动配网

#endif

extern uint8_t red_buff;
extern uint8_t green_buff;
extern uint8_t blue_buff;

typedef enum {
    DEVICE_STATE_NONE = 0,
    DEVICE_STATE_INIT_OK,
    DEVICE_STATE_WIFI_INIT_OK,
    DEVICE_STATE_WIFI_CONNECT_OK,
    DEVICE_STATE_WIFI_DISCONNECT,
    DEVICE_STATE_MQTT_CONNECT,
    DEVICE_STATE_MQTT_DISCONNECT,
    DEVICE_STATE_BLUFI_STATR,
}dev_state_t;

void device_state_init(void);
void device_state_start(void);
void device_update_state(dev_state_t new_state, uint8_t is_irq);
void device_button_init(uint8_t button_pin);
void device_rgb_init(void);
void device_rgb_wifi_config_light_start(uint8_t light_pin);
void device_rgb_wifi_config_light_stop(void);
void device_rgb_update_color(uint8_t red, uint8_t green, uint8_t blue);
void device_rgb__inch_by_update_color(uint8_t red, uint8_t green, uint8_t blue, uint8_t inch_by_numble, uint16_t ms_ticks);
#endif