/**
 * @file device_buttom.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-02-27
 *
 * @copyright Copyright (c) 2024
 *
*/
#include <FreeRTOS.h>
#include <task.h>
#include "timers.h"
#include "log.h"
#include "bflb_gpio.h"
#include "device_state.h"

#define DBG_TAG "DEVICE_BUTTON"

#define BUTTON_TIMEOUT 2 //长按时间设定。单位 s

struct bflb_device_s* gpio;
static uint8_t s_button_pin;
static xTaskHandle config_task;
/**
 * @brief configButtomLongPressCb
 *    长按识别任务
 * @param arg
*/
static void configButtomLongPressCb(void* arg)
{
    uint8_t config_buttom_press_cout = 0;
    while (1)
    {
#if (CONFIG_NETWORK_TYPE==CONFIG_BUTTON)
        if (bflb_gpio_read(gpio, s_button_pin)) {
            vTaskDelay(pdMS_TO_TICKS(10));

            while (bflb_gpio_read(gpio, s_button_pin))
            {
                config_buttom_press_cout++;
                vTaskDelay(pdMS_TO_TICKS(100));
                if (config_buttom_press_cout>=(BUTTON_TIMEOUT*1000/100)) {
                    /**** 按键长按测试打印 ****/
                    LOG_I(">>> onfig buttom long press <<<");
                    config_buttom_press_cout = 0;
                    device_update_state(DEVICE_STATE_BLUFI_STATR, false);

                }
            }
        }
        config_buttom_press_cout = 0;
        vTaskDelay(pdMS_TO_TICKS(100));
#else
        xTaskNotifyWait(0xffffffff, 0x0, &config_buttom_press_cout, portMAX_DELAY);
        switch (config_buttom_press_cout)
        {
            case 1:
                device_rgb__inch_by_update_color(31, 43, 64, 20, 50);
                break;
            case 2:
                device_rgb__inch_by_update_color(64, 63, 62, 20, 50);
                break;
            case 3:
                device_rgb__inch_by_update_color(64, 34, 3, 20, 50);
                break;

            default:
                vTaskDelay(pdMS_TO_TICKS(200));
                break;
        }
        device_update_state(DEVICE_STATE_INIT_OK, false);
        vTaskDelete(config_task);
#endif
    }
}

void device_button_init(uint8_t button_pin)
{
    //如果是按键启动配网
#if (CONFIG_NETWORK_TYPE==CONFIG_BUTTON)
    gpio = bflb_device_get_by_name("gpio");
    s_button_pin = button_pin;
    bflb_gpio_init(gpio, s_button_pin, GPIO_INPUT | GPIO_PULLDOWN| GPIO_SMT_EN|GPIO_DRV_0);
    xTaskCreate(configButtomLongPressCb, "config button", 1024*2, NULL, 14, NULL);

#else //通过重启启动配网
    //读取Flash 启动值
    xTaskCreate(configButtomLongPressCb, "config button", 1024*2, NULL, 14, &config_task);

    char* reset_numble = flash_get_data(CONFIG_RESET_NUBLE, 1);
    if (!strlen(reset_numble)) {
        flash_erase_set(CONFIG_RESET_NUBLE, "0");
    }
    else {
        uint8_t numble = *reset_numble-'0';
        xTaskNotify(config_task, numble, eSetValueWithoutOverwrite);
        LOG_F(">>>>>>>>>>>>>>>>>>>>>>>>>reset numble= %d %d", numble, CONFIG_RESET_NUBLE_CONT);

        if (numble>=CONFIG_RESET_NUBLE_CONT) {
            flash_erase_data(CONFIG_FLASH_KEY_SSID);
            flash_erase_data(CONFIG_FLASH_KEY_PWD);
            flash_erase_data(CONFIG_FLASH_KEY_BSSID);
        }
        else {
            numble++;
            memset(reset_numble, 0, 1);
            sprintf(reset_numble, "%d", numble);
            flash_erase_set(CONFIG_RESET_NUBLE, reset_numble);
        }
    }
    vPortFree(reset_numble);

#endif
}

