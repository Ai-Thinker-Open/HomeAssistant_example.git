/**
 * @file device_rgb.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-02-27
 *
 * @copyright Copyright (c) 2024
 *
*/
#include "FreeRTOS.h"
#include "task.h"
#include "log.h"
#include "bflb_gpio.h"
#include "bflb_mtimer.h"
#include "bflb_pwm_v2.h"
#include "bflb_clock.h"
#include "device_state.h"
#define DBG_TAG "DEVICE_RGB"

#define RGB_RED_PWM_CH PWM_CH0
#define RGB_GREEN_PWM_CH PWM_CH1
#define RGB_BLUE_PWM_CH PWM_CH2

static uint8_t wifi_config_led;
static struct bflb_device_s* pwm;
static xTaskHandle configNet_task;
static struct bflb_device_s* config_gpio;

uint8_t red_buff;
uint8_t green_buff;
uint8_t blue_buff;

static void configNET_start(void* arg)
{
    while (1) {
        // bflb_gpio_set(config_gpio, wifi_config_led);
        // vTaskDelay(pdMS_TO_TICKS(200));
        // bflb_gpio_reset(config_gpio, wifi_config_led);
        // vTaskDelay(pdMS_TO_TICKS(200));
        device_rgb__inch_by_update_color(64, 48, 35, 20, 50);
        device_rgb__inch_by_update_color(0, 0, 0, 20, 50);
    }
}

void device_rgb_wifi_config_light_start(uint8_t light_pin)
{
    // config_gpio = bflb_device_get_by_name("gpio");
    // bflb_gpio_init(config_gpio, light_pin, GPIO_OUTPUT | GPIO_PULLUP | GPIO_SMT_EN | GPIO_DRV_0);
    // wifi_config_led = light_pin;
    xTaskCreate(configNET_start, "configNET_task", 1024, config_gpio, 13, &configNet_task);
}

void device_rgb_wifi_config_light_stop(void)
{

    vTaskSuspend(configNet_task);
    device_rgb__inch_by_update_color(64, 48, 35, 20, 50);
    vTaskDelete(configNet_task);
    // bflb_gpio_deinit(config_gpio, wifi_config_led);

}

void device_rgb_init(void)
{
    struct bflb_device_s* gpio;

    gpio = bflb_device_get_by_name("gpio");
    bflb_gpio_init(gpio, CONFIG_RGB_RED_PIN, GPIO_FUNC_PWM0 | GPIO_ALTERNATE | GPIO_PULLUP | GPIO_SMT_EN | GPIO_DRV_1);
    bflb_gpio_init(gpio, CONFIG_RGB_GREEN_PIN, GPIO_FUNC_PWM0 | GPIO_ALTERNATE | GPIO_PULLUP | GPIO_SMT_EN | GPIO_DRV_1);
    bflb_gpio_init(gpio, CONFIG_RGB_BLUE_PIN, GPIO_FUNC_PWM0 | GPIO_ALTERNATE | GPIO_PULLUP | GPIO_SMT_EN | GPIO_DRV_1);

    pwm = bflb_device_get_by_name("pwm_v2_0");
    //100Hz
    struct bflb_pwm_v2_config_s cfg = {
        .clk_source = BFLB_SYSTEM_XCLK ,
        .clk_div = 40,
        .period = 1000,
    };

    bflb_pwm_v2_init(pwm, &cfg);
    bflb_pwm_v2_channel_positive_start(pwm, RGB_RED_PWM_CH);
    bflb_pwm_v2_channel_positive_start(pwm, RGB_GREEN_PWM_CH);
    bflb_pwm_v2_channel_positive_start(pwm, RGB_BLUE_PWM_CH);
    bflb_pwm_v2_start(pwm);
}
/**
 * @brief
 *
 * @param red
 * @param green
 * @param blue
*/
void device_rgb_update_color(uint8_t red, uint8_t green, uint8_t blue)
{
    uint16_t color_red_period = 1000*red/255;
    uint16_t color_green_period = 1000*green/255;
    uint16_t color_blue_period = 1000*blue/255;
    bflb_pwm_v2_stop(pwm);
    bflb_pwm_v2_channel_set_threshold(pwm, RGB_RED_PWM_CH, 0, color_red_period);
    bflb_pwm_v2_channel_set_threshold(pwm, RGB_GREEN_PWM_CH, 0, color_green_period);
    bflb_pwm_v2_channel_set_threshold(pwm, RGB_BLUE_PWM_CH, 0, color_blue_period);
    bflb_pwm_v2_start(pwm);

}

/**
 * @brief 渐变调节颜色
 *
 * @param red  目标红色
 * @param green  目标绿色
 * @param blue 目标蓝色
 * @param inch_by_numble 调节步骤
 * @param ms_ticks 间隔时间，单位：ms, 时间越小，变化越快
*/
void device_rgb__inch_by_update_color(uint8_t red, uint8_t green, uint8_t blue, uint8_t inch_by_numble, uint16_t ms_ticks)
{
    uint8_t red_type = 0, green_type = 0, blue_type = 0;
    if (red>=235&&blue>=235&&green>=235) {
        red = 235;
        blue = 235;
        green = 235;
    }
    for (size_t i = 1; i < inch_by_numble; i++)
    {
        red_type = red_buff+(float)(i/(float)inch_by_numble)*(red-red_buff);
        green_type = green_buff+(float)(i/(float)inch_by_numble)*(green-green_buff);
        blue_type = blue_buff+(float)(i/(float)inch_by_numble)*(blue-blue_buff);
        device_rgb_update_color(red_type, green_type, blue_type);
        vTaskDelay(pdMS_TO_TICKS(ms_ticks));
    }
    red_buff = red;
    green_buff = green;
    blue_buff = blue;
}
